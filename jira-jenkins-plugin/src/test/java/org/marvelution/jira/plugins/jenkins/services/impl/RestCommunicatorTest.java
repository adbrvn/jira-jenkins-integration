/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.net.URI;
import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Artifact;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.ChangeSet;
import org.marvelution.jira.plugins.jenkins.model.Culprit;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;

import com.atlassian.modzdetector.IOUtils;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.testresources.net.MockRequest;
import com.atlassian.sal.testresources.net.MockRequestFactory;
import com.atlassian.sal.testresources.net.MockResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.marvelution.jira.plugins.jenkins.model.Result.FAILURE;
import static org.marvelution.jira.plugins.jenkins.model.Result.SUCCESS;

import static org.apache.commons.lang3.StringUtils.stripEnd;
import static org.apache.commons.lang3.StringUtils.stripStart;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * JUnit testcase for {@link RestCommunicator}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RestCommunicatorTest {

	@Mock
	private JenkinsPluginConfiguration pluginConfiguration;
	private Site site;
	private NonMarshallingMockRequestFactory requestFactory;
	private RestCommunicator communicator;

	/**
	 * Setup the test Mocks
	 *
	 * @throws Exception in case of errors
	 */
	@Before
	public void setup() throws Exception {
		site = new Site(1);
		site.setName("Jenkins CI");
		site.setRpcUrl(URI.create("http://ci.marvelution.org/"));
		site.setSupportsBackLink(true);
		requestFactory = new NonMarshallingMockRequestFactory();
		communicator = new RestCommunicator(pluginConfiguration, site, requestFactory);
	}

	/**
	 * Test {@link RestCommunicator#getJobs()}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetJobs() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "api/json"), "job-list.json");
		List<Job> jobs = communicator.getJobs();
		assertThat(jobs.isEmpty(), is(false));
		assertThat(jobs.size(), is(3));
		assertThat(jobs.get(0).getName(), is("jira-jenkins-integration"));
		assertThat(jobs.get(1).getName(), is("sonaqube-integration"));
		assertThat(jobs.get(2).getName(), is("atlassian-integration"));
		for (Job job : jobs) {
			assertThat(job.getSiteId(), is(1));
			assertThat(job.getId(), is(0));
			assertThat(job.getBuilds().isEmpty(), is(true));
			assertThat(job.getUrlName(), is(job.getName()));
			assertThat(job.getUrlNameOrNull(), is(nullValue()));
		}
	}

	/**
	 * Test {@link RestCommunicator#getDetailedJob(Job)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedJob() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "job/jira-jenkins-integration/api/json"), "job.json");
		Job result = communicator.getDetailedJob(new Job(0, 1, "jira-jenkins-integration"));
		assertThat(result.getId(), is(0));
		assertThat(result.getSiteId(), is(1));
		assertThat(result.getName(), is("jira-jenkins-integration"));
		assertThat(result.getUrlName(), is(result.getName()));
		assertThat(result.getUrlNameOrNull(), is(nullValue()));
		assertThat(result.getLastBuild(), is(0));
		assertThat(result.getDescription(), is(""));
		assertThat(result.isBuildable(), is(true));
		assertThat(result.isDeleted(), is(false));
		assertThat(result.isLinked(), is(false));
		assertThat(result.getOldestBuild(), is(1623));
		assertThat(result.getBuilds().isEmpty(), is(false));
		assertThat(result.getBuilds().size(), is(18));
		assertThat(result.getJobs().isEmpty(), is(true));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedJob(Job)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedJob_FolderJob() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "job/Cave/api/json"), "folder-job-list.json");
		Job result = communicator.getDetailedJob(new Job(0, 1, "Cave"));
		assertThat(result.getId(), is(0));
		assertThat(result.getSiteId(), is(1));
		assertThat(result.getName(), is("Cave"));
		assertThat(result.getUrlName(), is(result.getName()));
		assertThat(result.getUrlNameOrNull(), is(nullValue()));
		assertThat(result.getLastBuild(), is(0));
		assertThat(result.getDescription(), is(nullValue()));
		assertThat(result.isBuildable(), is(false));
		assertThat(result.isDeleted(), is(false));
		assertThat(result.isLinked(), is(false));
		assertThat(result.getBuilds().isEmpty(), is(true));
		assertThat(result.getJobs().size(), is(3));
		Job job = result.getJobs().get(0);
		assertThat(job.getSiteId(), is(1));
		assertThat(job.getId(), is(0));
		assertThat(job.getBuilds().isEmpty(), is(true));
		assertThat(job.getDescription(), is(nullValue()));
		assertThat(job.getName(), is("jira-jenkins-integration"));
		assertThat(job.getUrlName(), is("Cave/job/jira-jenkins-integration"));
		assertThat(job.getUrlNameOrNull(), is("Cave/job/jira-jenkins-integration"));
		job = result.getJobs().get(1);
		assertThat(job.getSiteId(), is(1));
		assertThat(job.getId(), is(0));
		assertThat(job.getBuilds().isEmpty(), is(true));
		assertThat(job.getDescription(), is(nullValue()));
		assertThat(job.getName(), is("sonaqube-integration"));
		assertThat(job.getUrlName(), is("Cave/job/sonaqube-integration"));
		assertThat(job.getUrlNameOrNull(), is("Cave/job/sonaqube-integration"));
		job = result.getJobs().get(2);
		assertThat(job.getSiteId(), is(1));
		assertThat(job.getId(), is(0));
		assertThat(job.getBuilds().isEmpty(), is(true));
		assertThat(job.getDescription(), is(nullValue()));
		assertThat(job.getName(), is("atlassian-integration"));
		assertThat(job.getUrlName(), is("Cave/job/atlassian-integration"));
		assertThat(job.getUrlNameOrNull(), is("Cave/job/atlassian-integration"));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedBuild(Job, int)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedBuild() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "job/jira-jenkins-integration/2144/api/json"), "build.json");
		Job job = new Job(1, 1, "jira-jenkins-integration");
		Build build = communicator.getDetailedBuild(job, 2144);
		assertThat(build.getDisplayName(), nullValue());
		assertBuild(build);
	}

	/**
	 * Test {@link RestCommunicator#getDetailedBuild(Job, int)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedBuildWithDisplayName() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "job/jira-jenkins-integration/2144/api/json"),
		                   "build-with-display-name.json");
		Job job = new Job(1, 1, "jira-jenkins-integration");
		Build build = communicator.getDetailedBuild(job, 2144);
		assertBuild(build);
		assertThat(build.getDisplayName(), is("#1.55.2144"));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedBuild(Job, int)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedBuildWithChangeSetAsArray() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "job/jira-jenkins-integration/2144/api/json"),
		                   "build-changeset-as-array.json");
		Job job = new Job(1, 1, "jira-jenkins-integration");
		Build build = communicator.getDetailedBuild(job, 2144);
		assertBuild(build);
		assertThat(build.getDisplayName(), nullValue());
	}

	private void assertBuild(Build build) {
		assertThat(build.getNumber(), is(2144));
		assertThat(build.getJobId(), is(1));
		assertThat(build.getDescription(), is("My build description"));
		assertThat(build.getDuration(), is(135329L));
		assertThat(build.getBuiltOn(), is("remote-slave-6"));
		assertThat(build.getTimestamp(), is(1355405446078L));
		assertThat(build.getResult(), is(FAILURE));
		assertThat(build.getCause(), is("Started by an SCM change"));
		assertThat(build.getArtifacts().size(), is(2));
		Artifact artifact = build.getArtifacts().get(0);
		assertThat(artifact.getFileName(), is("jenkins-jira-plugin-1.6.0-SNAPSHOT.hpi"));
		assertThat(artifact.getDisplayPath(), is("jenkins-jira-plugin-1.6.0-SNAPSHOT.hpi"));
		assertThat(artifact.getRelativePath(), is("jenkins-jira-plugin/target/jenkins-jira-plugin-1.6.0-SNAPSHOT.hpi"));
		artifact = build.getArtifacts().get(1);
		assertThat(artifact.getFileName(), is("jira-jenkins-plugin-1.6.0-SNAPSHOT.obr"));
		assertThat(artifact.getDisplayPath(), is("jira-jenkins-plugin-1.6.0-SNAPSHOT.obr"));
		assertThat(artifact.getRelativePath(), is("jira-jenkins-plugin/target/jira-jenkins-plugin-1.6.0-SNAPSHOT.obr"));
		assertThat(build.getCulprits().size(), is(2));
		Culprit culprit = build.getCulprits().get(0);
		assertThat(culprit.getUsername(), is("Mark Rekveld"));
		assertThat(culprit.getFullName(), is("Mark Rekveld"));
		culprit = build.getCulprits().get(1);
		assertThat(culprit.getUsername(), is("cave"));
		assertThat(culprit.getFullName(), is("Cave Groundskeeper"));
		assertThat(build.getChangeSet().size(), is(2));
		ChangeSet changeSet = build.getChangeSet().get(0);
		assertThat(changeSet.getCommitId(), is("9a0d506fa8982e41ed3340caeed4a605587b6aa2"));
		assertThat(changeSet.getMessage(), is("Fake commit message to test the RestCommunicator\n"));
		changeSet = build.getChangeSet().get(1);
		assertThat(changeSet.getCommitId(), is("9a0d506fa8982e41ed3340caeed4a605587b6aa2"));
		assertThat(changeSet.getMessage(), is("Fake commit message to test the RestCommunicator\n"));
		assertThat(build.getTestResults(), notNullValue());
		assertThat(build.getTestResults().getFailed(), is(0));
		assertThat(build.getTestResults().getSkipped(), is(0));
		assertThat(build.getTestResults().getTotal(), is(3121));
	}

	/**
	 * Test {@link RestCommunicator#getDetailedBuild(Job, int)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testGetDetailedBuildSvn() throws Exception {
		setupRequestAnswer(new MockRequest(Request.MethodType.GET, "job/testproject-1.0.0/92/api/json"), "build-svn.json");
		Job job = new Job(1, 1, "testproject-1.0.0");
		Build build = communicator.getDetailedBuild(job, 92);
		assertThat(build.getNumber(), is(92));
		assertThat(build.getJobId(), is(1));
		assertThat(build.getDuration(), is(81872L));
		assertThat(build.getDisplayName(), nullValue());
		assertThat(build.getDescription(), nullValue());
		assertThat(build.getBuiltOn(), is("master"));
		assertThat(build.getTimestamp(), is(1453305441713L));
		assertThat(build.getResult(), is(SUCCESS));
		assertThat(build.getCause(), is("Started by user Mark"));
		assertThat(build.getArtifacts().size(), is(0));
		Culprit culprit = build.getCulprits().get(0);
		assertThat(culprit.getFullName(), is("Mark"));
		assertThat(build.getChangeSet().size(), is(3));
		ChangeSet changeSet = build.getChangeSet().get(0);
		assertThat(changeSet.getCommitId(), is("438948"));
		assertThat(changeSet.getMessage(), is("TEST00000: Jira Jenkins Integration TEST-3"));
		changeSet = build.getChangeSet().get(1);
		assertThat(changeSet.getCommitId(), is("438707"));
		assertThat(changeSet.getMessage(), is("TEST00000: Jira Jenkins Integration TEST-3"));
		changeSet = build.getChangeSet().get(2);
		assertThat(changeSet.getCommitId(), is("436047"));
		assertThat(changeSet.getMessage(), is("TEST00000: Vault Configuration test +createjob"));
	}

	private void setupRequestAnswer(final MockRequest request, final String json) throws Exception {
		MockResponse response = new MockResponse();
		response.setStatusCode(200);
		response.setResponseBodyAsStream(getClass().getResourceAsStream(json));
		response.setResponseBodyAsString(IOUtils.toString(response.getResponseBodyAsStream()));
		request.setResponse(response);
		String url = stripEnd(site.getRpcUrl().toASCIIString(), "/") + "/" + stripStart(request.getUrl(), "/");
		request.setUrl(url);
		requestFactory.addRequest(url, request);
	}

	static class NonMarshallingMockRequestFactory implements NonMarshallingRequestFactory<Request<?, ?>> {

		final MockRequestFactory delegate = new MockRequestFactory();

		void addRequest(String url, Request<?, ?> request) {
			delegate.addRequest(url, request);
		}

		@Override
		public Request createRequest(Request.MethodType methodType, String url) {
			return delegate.createRequest(methodType, url);
		}

		@Override
		public boolean supportsHeader() {
			return delegate.supportsHeader();
		}

	}

}
