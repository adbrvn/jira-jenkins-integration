/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.net.URI;
import java.util.Map;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.marvelution.jira.plugins.jenkins.rest.Matchers.status;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class BuildResourceIT extends AbstractResourceTest {

	private static final GenericType<Map<String, String>> LINKS_TYPE = new GenericType<Map<String, String>>() {};
	private static Site site;
	private static Job job;

	@BeforeClass
	public static void setup() {
		site = backdoor.sites().addSite(SiteType.JENKINS, "Local Test", URI.create("http://localhost:8080"));
		job = backdoor.jobs().addJob("test-job");
	}

	@AfterClass
	public static void teardown() {
		backdoor.sites().clearSites();
	}

	@Before
	public void setUp() throws Exception {
		// Make sure all interaction are done as an anonymous user
		anonymous();
	}

	@Test
	public void testSyncJob() throws Exception {
		testSyncJob(job, ClientResponse.Status.NO_CONTENT);
	}

	@Test
	public void testSyncJob_UnknownJob() throws Exception {
		testSyncJob(new Job(-1, site.getId(), "unknown-job"), ClientResponse.Status.NOT_FOUND);
	}

	private void testSyncJob(Job job, ClientResponse.Status expectedResponseStatus) {
		ClientResponse response = buildResource(job).put(ClientResponse.class);
		assertThat(response, status(expectedResponseStatus));
	}

	@Test
	public void testGetBuildLinks() throws Exception {
		Project project = generateScrumProject();
		Build build = backdoor.builds().addBuild(job);
		Map<String, String> links = buildResource(job, build, "links").get(LINKS_TYPE);
		assertThat(links.isEmpty(), is(true));
		String issueKey = backdoor.issues().createIssue(project.key, "Build Link Test").key();
		backdoor.builds().linkBuildToIssue(build, issueKey);
		links = buildResource(job, build, "links").get(LINKS_TYPE);
		assertThat(links.isEmpty(), is(false));
		assertThat(links.size(), is(1));
		assertThat(links.keySet(), hasItem(issueKey));
		assertThat(links.get(issueKey), is(equalTo(UriBuilder.fromUri(backdoor.environmentData().getBaseUrl().toURI())
		                                                     .path("browse").path(issueKey).build().toASCIIString())));
		backdoor.issues().deleteIssue(issueKey, true);
	}

	@Test
	public void testGetBuildLinks_UnknownJob() throws Exception {
		ClientResponse response = buildResource(new Job(-1, site.getId(), "unknown-job"), 1, "links").get(ClientResponse.class);
		assertThat(response, status(ClientResponse.Status.NOT_FOUND));
	}

	@Test
	public void testGetBuildLinks_UnknownBuild() throws Exception {
		Map<String, String> links = buildResource(job, -1, "links").get(LINKS_TYPE);
		assertThat(links.isEmpty(), is(true));
	}

}
