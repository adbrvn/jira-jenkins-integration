/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import javax.annotation.Nullable;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link IssueKeyExtractor}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(Parameterized.class)
public class IssueKeyExtractorTest {

	private String[] messages;
	private String[] expected;

	public IssueKeyExtractorTest(Object messages, Object expected) {
		this.messages = toStringArray(messages);
		this.expected = toStringArray(expected);
	}

	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] {
				// single issue key
				{ "ABC-123", "ABC-123" },
				{ "ABC-123 text", "ABC-123" },
				{ "message ABC-123", "ABC-123" },
				{ "message ABC-123 text", "ABC-123" },
				{ "message\nABC-123\ntext", "ABC-123" },
				{ "message\rABC-123\rtext", "ABC-123" },
				{ "message.ABC-123.text", "ABC-123" },
				{ "message:ABC-123:text", "ABC-123" },
				{ "message,ABC-123,text", "ABC-123" },
				{ "message;ABC-123;text", "ABC-123" },
				{ "message&ABC-123&text", "ABC-123" },
				{ "message=ABC-123=text", "ABC-123" },
				{ "message?ABC-123?text", "ABC-123" },
				{ "message!ABC-123!text", "ABC-123" },
				{ "message/ABC-123/text", "ABC-123" },
				{ "message\\ABC-123\\text", "ABC-123" },
				{ "message~ABC-123~text", "ABC-123" },
				{ "message_ABC-123_text", "ABC-123" },
				{
						new String[] { "ABC-123", "message_ABC-987_text" },
						new String[] { "ABC-123", "ABC-987" }
				},
				//	single issue key with numbers
				{ "A1BC-123", "A1BC-123" },
				{ "AB8C-123 text", "AB8C-123" },
				{ "message A7BC-123", "A7BC-123" },
				{ "message AB9C-123 text", "AB9C-123" },
				{ "message\nA2BC-123\ntext", "A2BC-123" },
				{ "message\rABC0-123\rtext", "ABC0-123" },
				{ "message.ABC789-123.text", "ABC789-123" },
				{ "message:A1BC-123:text", "A1BC-123" },
				{ "message,A1BC-123,text", "A1BC-123" },
				{ "message;A1BC-123;text", "A1BC-123" },
				{ "message&AB1C-123&text", "AB1C-123" },
				{ "message=A1BC-123=text", "A1BC-123" },
				{ "message?A1BC-123?text", "A1BC-123" },
				{ "message!AB1C-123!text", "AB1C-123" },
				{ "message/AB1C-123/text", "AB1C-123" },
				{ "message\\A1BC-123\\text", "A1BC-123" },
				{ "message~A1BC-123~text", "A1BC-123" },
				{ "message_A1BC-123_text", "A1BC-123" },
				{
						new String[] { "A1BC-123", "message_A1BC-987_text" },
						new String[] { "A1BC-123", "A1BC-987" }
				},
				//	single issue key with underscore
				{ "A_BC-123", "A_BC-123" },
				{ "AB_C-123", "AB_C-123" },
				{ "ABC_-123", "ABC_-123" },
				{ "A_BC-123 text", "A_BC-123" },
				{ "message A_BC-123", "A_BC-123" },
				{ "message A_BC-123 text", "A_BC-123" },
				{ "message\nA_BC-123\ntext", "A_BC-123" },
				{ "message\rA_BC-123\rtext", "A_BC-123" },
				{ "message.A_BC-123.text", "A_BC-123" },
				{ "message:A_BC-123:text", "A_BC-123" },
				{ "message,A_BC-123,text", "A_BC-123" },
				{ "message;A_BC-123;text", "A_BC-123" },
				{ "message&A_BC-123&text", "A_BC-123" },
				{ "message=A_BC-123=text", "A_BC-123" },
				{ "message?A_BC-123?text", "A_BC-123" },
				{ "message!A_BC-123!text", "A_BC-123" },
				{ "message/A_BC-123/text", "A_BC-123" },
				{ "message\\A_BC-123\\text", "A_BC-123" },
				{ "message~A_BC-123~text", "A_BC-123" },
				{ "message_A_BC-123_text", "A_BC-123" },
				{
						new String[] { "ABC-123", "message_ABC-987" },
						new String[] { "ABC-123", "ABC-987" }
				},
				// multiple issue keys
				{ "ABC-123 DEF-456", new String[] { "ABC-123", "DEF-456" } },
				{ "message ABC-123 DEF-456 text", new String[] { "ABC-123", "DEF-456" } },
				{ "message\nABC-123\nDEF-456\ntext", new String[] { "ABC-123", "DEF-456" } },
				{ "message\rABC-123\rDEF-456\rtext", new String[] { "ABC-123", "DEF-456" } },
				{ "message.ABC-123.DEF-456.text", new String[] { "ABC-123", "DEF-456" } },
				{ "message:ABC-123:DEF-456:text", new String[] { "ABC-123", "DEF-456" } },
				{ "message,ABC-123,DEF-456,text", new String[] { "ABC-123", "DEF-456" } },
				{ "message;ABC-123;DEF-456;text", new String[] { "ABC-123", "DEF-456" } },
				{ "message&ABC-123&DEF-456&text", new String[] { "ABC-123", "DEF-456" } },
				{ "message=ABC-123=DEF-456=text", new String[] { "ABC-123", "DEF-456" } },
				{ "message?ABC-123?DEF-456?text", new String[] { "ABC-123", "DEF-456" } },
				{ "message!ABC-123!DEF-456!text", new String[] { "ABC-123", "DEF-456" } },
				{ "message/ABC-123/DEF-456/text", new String[] { "ABC-123", "DEF-456" } },
				{ "message\\ABC-123\\DEF-456\\text", new String[] { "ABC-123", "DEF-456" } },
				{ "message~ABC-123~DEF-456~text", new String[] { "ABC-123", "DEF-456" } },
				{ "message_ABC-123_DEF-456_text", new String[] { "ABC-123", "DEF-456" } },
				{
						new String[] { "ABC-123 DEF-456", "message_ABC-987_DEF-654_text" },
						new String[] { "ABC-123", "DEF-456", "ABC-987", "DEF-654" }
				},
				//	no issue keys
				{ "message without key", null },
				{ "message ABC-A text", null },
				{ "message M-123 invalid key", null },
				{ "message MES- invalid key", null },
				{ "message -123 invalid key", null },
				{ "message 1ABC-123 invalid key", null },
				{ "message 123-123 invalid key", null },
				{ "should not parse key0MES-123", null },
				{ "should not parse MES-123key", null },
				{ "MES-123k invalid char", null },
				{ "invalid char MES-123k", null },
				{ "", null },
				{ null, null },
				{ new String[] { "", "" }, null },
				{ new String[] { null, null }, null }
		});
	}

	@Nullable
	private static String[] toStringArray(Object object) {
		if (object == null) {
			return null;
		} else if (object instanceof String[]) {
			return (String[]) object;
		} else {
			return new String[] { (String) object };
		}
	}

	@Test
	public void testExtractIssueKeys() {
		Set<String> actual = IssueKeyExtractor.extractIssueKeys(messages);
		if (expected == null) {
			assertThat(actual.size(), is(0));
		} else {
			assertThat(actual, hasItems(expected));
		}
	}

}
