/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins;

import javax.inject.Inject;

import org.marvelution.jira.plugins.jenkins.testkit.backdoor.JenkinsBackdoor;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.setup.JiraWebDriverScreenshotRule;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.browser.Browser;
import com.atlassian.pageobjects.browser.BrowserAware;
import com.atlassian.pageobjects.inject.ConfigurableInjectionContext;
import com.atlassian.pageobjects.inject.InjectionConfiguration;
import com.atlassian.webdriver.testing.annotation.TestedProductClass;
import com.atlassian.webdriver.testing.rule.DefaultProductContextRules;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.runner.ProductContextRunner;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@TestedProductClass(JiraTestedProduct.class)
@RunWith(AbstractFunctionalTest.JiraTestedProductRunner.class)
public abstract class AbstractFunctionalTest extends AbstractIntegrationTest {

	@Inject
	@ClassRule
	public static DefaultProductContextRules.ForClass classRules;
	@Inject
	protected static JiraTestedProduct jira;
	@Inject
	@Rule
	public DefaultProductContextRules.ForMethod methodRules;

	/**
	 * Custom {@link ProductContextRunner} extending the default Injector
	 *
	 * @author Mark Rekveld
	 * @since 2.0.0
	 */
	public static class JiraTestedProductRunner extends ProductContextRunner {

		public JiraTestedProductRunner(Class<?> klass) throws InitializationError {
			super(klass);
		}

		@Override
		protected TestedProduct<?> createProduct(Class<? extends TestedProduct<?>> testedProductClass) {
			TestedProduct<?> product = super.createProduct(testedProductClass);
			InjectionConfiguration configuration = ((ConfigurableInjectionContext) product.getPageBinder())
					.configure().addSingleton(Logger.class, LoggerFactory.getLogger(JiraTestedProduct.class))
					.addImplementation(WebDriverScreenshotRule.class, JiraWebDriverScreenshotRule.class)
					.addImplementation(JenkinsBackdoor.class, JenkinsBackdoor.class);
			if (product.getTester() instanceof BrowserAware) {
				BrowserAware browserAware = (BrowserAware) product.getTester();
				configuration.addSingleton(BrowserAware.class, browserAware)
				             .addSingleton(Browser.class, browserAware.getBrowser());
			}
			configuration.finish();
			return product;
		}

	}


}
