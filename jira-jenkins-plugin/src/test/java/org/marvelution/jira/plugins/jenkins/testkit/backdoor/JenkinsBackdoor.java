/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import javax.inject.Inject;

import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.RestApiClient;

import static java.util.Collections.synchronizedMap;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class JenkinsBackdoor extends Backdoor {

	private final JIRAEnvironmentData environmentData;
	private final JenkinsSiteControl siteControl;
	private final JenkinsJobControl jobControl;
	private final JenkinsBuildControl buildControl;
	private final ExtendedLicenseControl extendedLicenseControl;
	private final RawControl rawControl;
	private final Map<Class<? extends RestApiClient>, RestApiClient> clients = synchronizedMap(new ConcurrentHashMap<>());
	private final Function<Class<? extends RestApiClient>, RestApiClient> clientCreator = type -> {
		try {
			return type.getConstructor(JIRAEnvironmentData.class).newInstance(environmentData());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	};

	@Inject
	public JenkinsBackdoor(JIRAEnvironmentData environmentData) {
		super(environmentData);
		this.environmentData = environmentData;
		siteControl = new JenkinsSiteControl(environmentData);
		jobControl = new JenkinsJobControl(environmentData, siteControl);
		buildControl = new JenkinsBuildControl(environmentData, jobControl);
		extendedLicenseControl = new ExtendedLicenseControl(environmentData);
		rawControl = new RawControl(environmentData);
	}

	public JIRAEnvironmentData environmentData() {
		return environmentData;
	}

	public JenkinsSiteControl sites() {
		return siteControl;
	}

	public JenkinsJobControl jobs() {
		return jobControl;
	}

	public JenkinsBuildControl builds() {
		return buildControl;
	}

	@Override
	public ExtendedLicenseControl license() {
		return extendedLicenseControl;
	}

	@Override
	public RawControl rawRestApiControl() {
		return rawControl;
	}

	public <RC extends RestApiClient> RC createRestClient(Class<RC> type) {
		return type.cast(clients.computeIfAbsent(type, clientCreator));
	}

}
