/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor;

import java.util.Set;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Result;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;

import static java.lang.String.valueOf;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class JenkinsBuildControl extends JenkinsBackdoorControl<JenkinsBuildControl> {

	private static final GenericType<Set<Build>> SET_GENERIC_TYPE = new GenericType<Set<Build>>() {};
	private final JenkinsJobControl jobControl;

	JenkinsBuildControl(JIRAEnvironmentData environmentData, JenkinsJobControl jobControl) {
		super(environmentData);
		this.jobControl = jobControl;
	}

	public Build getBuild(int jobId) {
		return createBuildResource().path(valueOf(jobId)).get(Build.class);
	}

	public Build addBuild(String jobName) {
		return addBuild(jobControl.addJob(jobName));
	}

	public Build addBuild(Job job) {
		return addBuild(job, Result.SUCCESS);
	}

	public Build addBuild(Job job, Result result) {
		return addBuild(job, "Generated by Backdoor", result);
	}

	public Build addBuild(Job job, String cause, Result result) {
		Build build = new Build(job.getId(), job.getLastBuild() + 1);
		build.setCause(cause);
		build.setResult(result);
		build.setTimestamp(System.currentTimeMillis());
		Build response = saveBuild(build);
		job.setLastBuild(response.getNumber());
		return response;
	}

	public Build saveBuild(Build build) {
		return createBuildResource().type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE).post(Build.class, build);
	}

	public void linkBuildToIssue(Build build, String issueKey) {
		createBuildResource().path(valueOf(build.getId())).path("link").type(APPLICATION_JSON_TYPE).post(issueKey);
	}

}
