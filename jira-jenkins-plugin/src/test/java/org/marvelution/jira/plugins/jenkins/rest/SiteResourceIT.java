/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.net.URI;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import org.marvelution.jira.plugins.jenkins.model.ErrorMessages;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;
import org.marvelution.jji.testkit.wiremock.WireMockRule;

import com.sun.jersey.api.client.ClientResponse;
import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static org.marvelution.jira.plugins.jenkins.rest.Matchers.badRequest;
import static org.marvelution.jira.plugins.jenkins.rest.Matchers.errorForField;
import static org.marvelution.jira.plugins.jenkins.rest.Matchers.status;
import static org.marvelution.jira.plugins.jenkins.rest.Matchers.successful;

import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.jayway.awaitility.Awaitility.await;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class SiteResourceIT extends AbstractResourceTest {

	@Rule
	public WireMockRule wireMock = new WireMockRule();

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	@Test
	public void testAddSite() throws Exception {
		Site site = new Site();
		// try adding a site without details
		ClientResponse response = siteResource().entity(site, APPLICATION_JSON_TYPE).post(ClientResponse.class);
		assertThat(response, badRequest());
		ErrorMessages errors = response.getEntity(ErrorMessages.class);
		assertThat(errors.getErrors(), hasItem(errorForField("type")));
		assertThat(errors.getErrors(), hasItem(errorForField("name")));
		assertThat(errors.getErrors(), hasItem(errorForField("rpcUrl")));
		// try adding a site with invalid rpcUrl
		site.setType(SiteType.JENKINS);
		site.setName("Added CI");
		site.setRpcUrl(URI.create("localhost"));
		site.setUser("bob");
		site.setNewtoken("bob");
		response = siteResource().entity(site, APPLICATION_JSON_TYPE).post(ClientResponse.class);
		assertThat(response, status(ClientResponse.Status.BAD_REQUEST));
		errors = response.getEntity(ErrorMessages.class);
		assertThat(errors.getErrors(), hasItem(errorForField("rpcUrl")));
		// try adding a site with valid rpcUrl
		site.setRpcUrl(wireMock.serverUri());
		response = siteResource().entity(site, APPLICATION_JSON_TYPE).post(ClientResponse.class);
		assertThat(response, successful());
		// verify returned site object
		final Site entity = response.getEntity(Site.class);
		assertThat(entity, is(notNullValue()));
		assertThat(entity.getId(), is(not(0)));
		assertThat(entity.getType(), is(SiteType.JENKINS));
		assertThat(entity.getName(), is("Added CI"));
		assertThat(entity.getRpcUrl(), is(wireMock.serverUri()));
		assertThat(entity.getUser(), is("bob"));
		assertThat(entity.getToken(), is(nullValue()));
		assertThat(entity.getNewtoken(), is(nullValue()));
		assertThat(entity.isSupportsBackLink(), is(false));
		assertThat(entity.isUseCrumbs(), is(true));
		// verify that jobs are synchronized after the entity is added
		assertThat(entity, is(siteEquals(backdoor.sites().getSite(entity.getId()))));
		await().atMost(20, TimeUnit.SECONDS).until(() -> !backdoor.sites().getSite(entity.getId()).getJobs().isEmpty());
		List<Job> jobs = backdoor.sites().getSite(entity.getId()).getJobs();
		assertThat(jobs, hasSize(3));
		assertThat(jobs.stream().filter(Job::isLinked).count(), is(0L));
		verify(1, getRequestedFor(urlEqualTo("/plugin/jenkins-jira-plugin/ping.html")));
		verify(3, getRequestedFor(urlEqualTo("/api/json")));
	}

	@Test
	public void testUpdateSite() throws Exception {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "Updated CI", URI.create("http://localhost:8080/"));
		site.setUser("bob");
		site.setChangeToken(true);
		site.setNewtoken("bob");
		site.setRpcUrl(wireMock.serverUri());
		site.setAutoLink(true);
		Site entity = siteResource(site).entity(site, APPLICATION_JSON_TYPE).post(Site.class);
		assertThat(entity.isSupportsBackLink(), is(false));
		assertThat(entity.isUseCrumbs(), is(false));
		assertThat(entity, is(siteEquals(backdoor.sites().getSite(site.getId()))));
		await().atMost(20, TimeUnit.SECONDS).until(() -> !backdoor.sites().getSite(entity.getId()).getJobs().isEmpty());
		Site updated = backdoor.sites().getSite(entity.getId());
		assertThat(updated.isSupportsBackLink(), is(false));
		assertThat(updated.isUseCrumbs(), is(false));
		assertThat(updated.getJobs(), hasSize(3));
		assertThat(updated.getJobs().stream().filter(Job::isLinked).count(), is(3L));
		verify(1, getRequestedFor(urlEqualTo("/plugin/jenkins-jira-plugin/ping.html")));
		verify(6, getRequestedFor(urlEqualTo("/api/json")));
		verify(3, getRequestedFor(urlMatching("/job/([a-z-\\-]+)/api/json")));
	}

	public static Matcher<Site> siteEquals(final Site site) {
		return new DiagnosingMatcher<Site>() {
			@Override
			public void describeTo(Description description) {
				description.appendText("site same as ").appendValue(site);
			}

			@Override
			protected boolean matches(Object item, Description mismatchDescription) {
				if (item == null) {
					mismatchDescription.appendText("null");
					return false;
				} else if (!(item instanceof Site)) {
					mismatchDescription.appendValue(item).appendValue(" is a " + item.getClass().getName());
					return false;
				}
				Site other = (Site) item;
				if (site.getId() != other.getId()) {
					mismatchDescription.appendText("has id ").appendValue(other.getId());
					return false;
				} else if (!Objects.equals(site.getType(), other.getType())) {
					mismatchDescription.appendValue("has type ").appendValue(other.getType());
					return false;
				} else if (!Objects.equals(site.getName(), other.getName())) {
					mismatchDescription.appendText("has name ").appendValue(site.getName());
					return false;
				} else if (!Objects.equals(site.getType(), other.getType())) {
					mismatchDescription.appendValue("has type ").appendValue(other.getType());
					return false;
				} else if (!Objects.equals(site.getRpcUrl(), other.getRpcUrl())) {
					mismatchDescription.appendValue("has RPC URL ").appendValue(other.getRpcUrl());
					return false;
				} else if (!Objects.equals(site.getDisplayUrl(), other.getDisplayUrl())) {
					mismatchDescription.appendValue("has display URL ").appendValue(other.getDisplayUrl());
					return false;
				} else if (!Objects.equals(site.getUser(), other.getUser())) {
					mismatchDescription.appendValue("has user ").appendValue(other.getUser());
					return false;
				} else if (site.isAutoLink() != other.isAutoLink()) {
					mismatchDescription.appendValue("has autoLink ").appendValue(other.isAutoLink());
					return false;
				} else if (site.isDeleted() != other.isDeleted()) {
					mismatchDescription.appendValue("has delete ").appendValue(other.isDeleted());
					return false;
				}
				return true;
			}
		};
	}

}
