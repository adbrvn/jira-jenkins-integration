/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.utils;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link org.marvelution.jira.plugins.jenkins.utils.URIUtils}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class URIUtilsTest {

	/**
	 * Test {@link org.marvelution.jira.plugins.jenkins.utils.URIUtils#encode(String, String)}
	 */
	@Test
	public void testEncode() throws Exception {
		assertThat(URIUtils.encode("peripheral apps deactivation prevention", "UTF-8"),
		           is("peripheral%20apps%20deactivation%20prevention"));
	}

}
