/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.junit.Before;
import org.junit.BeforeClass;

import static org.marvelution.jira.plugins.jenkins.rest.Matchers.status;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Base Integration Testcase for Authentication and Authorization REST resources testing
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public abstract class AbstractResourceAuthzTest extends AbstractResourceTest {

	@BeforeClass
	public static void setupUsers() throws Exception {
		if (!backdoor.usersAndGroups().userExists(USER)) {
			backdoor.usersAndGroups().addUser(USER);
			backdoor.usersAndGroups().addUserToGroup(USER, "jira-software-users");
		}
	}

	@Before
	public void setUpAnonymousDefaultAuth() throws Exception {
		anonymous();
	}

	protected ClientResponse testAuthzGet(WebResource resource, AuthzPair... pairs) {
		return testAuthz("GET", resource, pairs);
	}

	protected ClientResponse testAuthzGet(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair... pairs) {
		return testAuthz("GET", resource, resourceCustomizer, pairs);
	}

	protected ClientResponse testAuthzPost(WebResource resource, AuthzPair... pairs) {
		return testAuthz("POST", resource, pairs);
	}

	protected ClientResponse testAuthzPost(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair... pairs) {
		return testAuthz("POST", resource, resourceCustomizer, pairs);
	}

	protected ClientResponse testAuthzPut(WebResource resource, AuthzPair... pairs) {
		return testAuthz("PUT", resource, pairs);
	}

	protected ClientResponse testAuthzPut(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer, AuthzPair... pairs) {
		return testAuthz("PUT", resource, resourceCustomizer, pairs);
	}

	protected ClientResponse testAuthzDelete(WebResource resource, AuthzPair... pairs) {
		return testAuthz("DELETE", resource, pairs);
	}

	protected ClientResponse testAuthzDelete(WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer,
	                                         AuthzPair... pairs) {
		return testAuthz("DELETE", resource, resourceCustomizer, pairs);
	}

	protected ClientResponse testAuthz(String method, WebResource resource, AuthzPair... pairs) {
		return testAuthz(method, resource, AuthzWebResourceBuilderCustomizer.DEFAULT, pairs);
	}

	protected ClientResponse testAuthz(String method, WebResource resource, AuthzWebResourceBuilderCustomizer resourceCustomizer,
	                                   AuthzPair... pairs) {
		ClientResponse response = null;
		HTTPBasicAuthFilter authFilter = null;
		for (AuthzPair pair : pairs) {
			if (authFilter != null && resource.isFilterPresent(authFilter)) {
				resource.removeFilter(authFilter);
			}
			if (!ANONYMOUS.equals(pair.username)) {
				authFilter = new HTTPBasicAuthFilter(pair.username, pair.password);
				resource.addFilter(authFilter);
			}
			response = resourceCustomizer.customize(resource.type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE))
			                             .method(method, ClientResponse.class);
			if (pair.expectedResponseStatus != null) {
				assertThat("Response status mismatch for " + pair.username, response, status(pair.expectedResponseStatus));
			}
		}
		assertThat(resource, notNullValue());
		return response;
	}

	/**
	 * Builtin {@link AuthzPair} for anonymous access
	 */
	protected AuthzPair anonymous(ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(ANONYMOUS, expectedResponseStatus);
	}

	/**
	 * Builtin {@link AuthzPair} for authenticated access
	 */
	protected AuthzPair authenticatedUser(ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(USER, expectedResponseStatus);
	}

	/**
	 * Builtin {@link AuthzPair} for administrator access
	 */
	protected AuthzPair administrator(ClientResponse.Status expectedResponseStatus) {
		return new AuthzPair(ADMIN, expectedResponseStatus);
	}

	/**
	 * Customizer API for authz resource invocations
	 */
	protected interface AuthzWebResourceBuilderCustomizer {

		AuthzWebResourceBuilderCustomizer DEFAULT = original -> original;

		WebResource.Builder customize(WebResource.Builder original);

	}

	/**
	 * Authentication test pair
	 */
	protected class AuthzPair {

		public final String username;
		public final String password;
		public final ClientResponse.Status expectedResponseStatus;

		protected AuthzPair(String username, ClientResponse.Status expectedResponseStatus) {
			this(username, username, expectedResponseStatus);
		}

		protected AuthzPair(String username, String password, ClientResponse.Status expectedResponseStatus) {
			this.username = username;
			this.password = password;
			this.expectedResponseStatus = expectedResponseStatus;
		}
	}

}
