/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins;

import javax.annotation.Nullable;
import javax.inject.Inject;

import org.marvelution.jira.plugins.jenkins.testkit.backdoor.JenkinsBackdoor;

import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestName;

import static com.atlassian.jira.rest.v2.issue.project.ProjectInputBean.builder;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.capitalize;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
public abstract class AbstractIntegrationTest {

	protected static final String ANONYMOUS = "anonymous";
	protected static final String USER = "user";
	protected static final String ADMIN = "admin";

	private static final String SCRUM_SOFTWARE_PROJECT_TEMPLATE_KEY = "com.pyxis.greenhopper.jira:gh-scrum-template";
	private static final String KANBAN_SOFTWARE_PROJECT_TEMPLATE_KEY = "com.pyxis.greenhopper.jira:gh-kanban-template";
	private static final String BASIC_SOFTWARE_PROJECT_TEMPLATE_KEY = "com.pyxis.greenhopper.jira:basic-software-development-template";
	private static final String PROJECT_MANAGEMENT_PROJECT_TEMPLATE_KEY = "com.atlassian.jira-core-project-templates:jira-core-project-management";

	@Inject
	protected static JenkinsBackdoor backdoor;
	@Rule
	public TestName testName = new TestName();

	@BeforeClass
	public static void switchToTestingLicense() {
		backdoor.license().switchToTestingLicense();
	}

	protected Project generateScrumProject() {
		return generateScrumProject(null);
	}

	protected Project generateScrumProject(@Nullable String projectName) {
		return generateProject(projectName, SCRUM_SOFTWARE_PROJECT_TEMPLATE_KEY);
	}

	protected Project generateKanBanProject() {
		return generateKanBanProject(null);
	}

	protected Project generateKanBanProject(@Nullable String projectName) {
		return generateProject(projectName, KANBAN_SOFTWARE_PROJECT_TEMPLATE_KEY);
	}

	protected Project generateBasicProject() {
		return generateBasicProject(null);
	}

	protected Project generateBasicProject(@Nullable String projectName) {
		return generateProject(projectName, BASIC_SOFTWARE_PROJECT_TEMPLATE_KEY);
	}

	protected Project generateBusinessProject() {
		return generateBusinessProject(null);
	}

	protected Project generateBusinessProject(@Nullable String projectName) {
		return generateProject(projectName, PROJECT_MANAGEMENT_PROJECT_TEMPLATE_KEY);
	}

	private Project generateProject(@Nullable String name, String templateKey) {
		ProjectClient restClient = backdoor.createRestClient(ProjectClient.class);
		String projectName = capitalize(ofNullable(name).orElse(testName.getMethodName()));
		String projectKey = projectName.replaceAll("[^A-Z]", "");
		restClient.getProjects().stream()
		          .filter(p -> p.key.equals(projectKey))
		          .findFirst()
		          .ifPresent(project -> restClient.delete(project.key));
		ClientResponse response = restClient.create(builder().setKey(projectKey)
		                                                     .setName(projectName)
		                                                     .setLeadName(ADMIN)
		                                                     .setProjectTemplateKey(templateKey)
		                                                     .build());
		assertThat(response.getStatusInfo(), is(ClientResponse.Status.CREATED));
		return response.getEntity(Project.class).name(projectName).projectTypeKey(templateKey);
	}

}
