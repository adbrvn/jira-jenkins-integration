/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import org.marvelution.jira.plugins.jenkins.ao.BuildMapping;
import org.marvelution.jira.plugins.jenkins.model.Build;

import com.atlassian.activeobjects.test.TestActiveObjects;
import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.jdbc.NonTransactional;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.marvelution.jira.plugins.jenkins.model.Result.FAILURE;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Test case for the {@link DefaultBuildDAO}
 *
 * @author Mark Rekveld
 * @since 1.3.4
 */
@RunWith(ActiveObjectsJUnitRunner.class)
@Data(DefaultBuildDAOTest.DefaultBuildDaoTestDatabaseUpdater.class)
public class DefaultBuildDAOTest {

	private EntityManager entityManager;
	private DefaultBuildDAO buildDao;

	@Before
	public void setup() throws Exception {
		assertNotNull(entityManager);
		buildDao = new DefaultBuildDAO(new TestActiveObjects(entityManager), null);
	}

	/**
	 * Testcase to make sure that the build is stored even with a large cause string
	 */
	@Test
	@NonTransactional
	public void testAddWithLargeCause() {
		Build build = new Build(54, 187);
		build.setCause("Started by remote host 207.223.240.188 with note: Triggered by push of revision b034be258e2c: \"IN PROGRESS - " +
				"issue TFE-805: TFE: Pressing back in ActionMode of RemindersActivity does not unselect and can lead to a crash " +
				"https://XXXXXXXXXXXXXXXXXXXXXXXXX/browse/TFE-805 \" to https://bitbucket.org/XXXXXXXXXXX/XXXXXXXX/ by XXXXXX");
		build.setResult(FAILURE);
		build.setDeleted(false);
		Build saved = buildDao.save(build);
		assertThat(saved.getJobId(), is(54));
		assertThat(saved.getNumber(), is(187));
		assertThat(saved.isDeleted(), is(false));
		assertThat(saved.getResult(), is(FAILURE));
		assertThat(saved.getCause().length(), is(200));
	}

	public static class DefaultBuildDaoTestDatabaseUpdater implements DatabaseUpdater {

		@Override
		@SuppressWarnings("unchecked")
		public void update(EntityManager entityManager) throws Exception {
			entityManager.migrate(BuildMapping.class);
		}

	}

}
