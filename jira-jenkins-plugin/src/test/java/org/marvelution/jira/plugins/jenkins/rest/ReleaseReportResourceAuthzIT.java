/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.Test;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class ReleaseReportResourceAuthzIT extends AbstractResourceAuthzTest {

	@Test
	public void testBuildInformation() throws Exception {
		Project project = generateScrumProject();
		IssueCreateResponse issue = backdoor.issues().createIssue(project.key, "Authz Test: " + testName.getMethodName());
		testAuthzGet(releaseReportResource().path("build-info/" + issue.key),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.OK),
		             administrator(ClientResponse.Status.OK));
	}

}
