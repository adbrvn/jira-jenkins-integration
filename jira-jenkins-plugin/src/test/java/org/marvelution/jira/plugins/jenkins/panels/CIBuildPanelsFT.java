/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.panels;

import org.marvelution.jira.plugins.jenkins.AbstractFunctionalTest;
import org.marvelution.jira.plugins.jenkins.testkit.pageobjects.CIBuildsTab;
import org.marvelution.jira.plugins.jenkins.testkit.pageobjects.IssuePage;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.restclient.Project;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Functional test for the CI Builds issue tab panel
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class CIBuildPanelsFT extends AbstractFunctionalTest {

	@Before
	public void loginAsAdmin() throws Exception {
		jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
	}

	@After
	public void logout() throws Exception {
		jira.logout();
	}

	@Test
	public void testCIBuildTabVisibleForScrumSoftwareProjectIssues() throws Exception {
		testCIBuildTabVisibilityForSoftwareProject(generateScrumProject());
	}

	@Test
	public void testCIBuildTabVisibleForKanbanSoftwareProjectIssues() throws Exception {
		testCIBuildTabVisibilityForSoftwareProject(generateKanBanProject());
	}

	@Test
	public void testCIBuildTabVisibleForBasicSoftwareProjectIssues() throws Exception {
		testCIBuildTabVisibilityForSoftwareProject(generateBasicProject());
	}

	private void testCIBuildTabVisibilityForSoftwareProject(Project project) {
		IssueCreateResponse issue = backdoor.issues().createIssue(project.key, testName.getMethodName());
		IssuePage issuePage = IssuePage.gotoIssue(jira, issue.key);
		assertThat("CI Builds tab panel should be visible", issuePage.hasCIBuildsTab(), is(true));
		CIBuildsTab panel = issuePage.openCIBuildsTab();
		assertThat(panel.hasBuilds(), is(false));
	}

	@Test
	public void testCIBuildTabNotVisibleForOtherProjectIssues() throws Exception {
		Project project = generateBusinessProject();
		IssueCreateResponse issue = backdoor.issues().createIssue(project.key, testName.getMethodName());
		IssuePage issuePage = IssuePage.gotoIssue(jira, issue.key);
		assertThat("CI Builds tab panel should not be present", issuePage.hasCIBuildsTab(), is(false));
	}

}
