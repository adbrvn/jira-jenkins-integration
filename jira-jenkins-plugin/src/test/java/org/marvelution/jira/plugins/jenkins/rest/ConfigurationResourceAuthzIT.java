/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import org.marvelution.jira.plugins.jenkins.model.PluginConfiguration;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.Test;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class ConfigurationResourceAuthzIT extends AbstractResourceAuthzTest {

	@Test
	public void testGetPluginConfiguration() throws Exception {
		testAuthzGet(configurationResource(),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testSavePluginConfiguration() throws Exception {
		testAuthzPost(configurationResource(),
		              builder -> builder.entity(new PluginConfiguration(), APPLICATION_JSON_TYPE),
		              anonymous(ClientResponse.Status.UNAUTHORIZED),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN),
		              administrator(ClientResponse.Status.NO_CONTENT));
	}

}
