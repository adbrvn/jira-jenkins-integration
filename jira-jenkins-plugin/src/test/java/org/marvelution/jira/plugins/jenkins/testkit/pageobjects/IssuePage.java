/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.pageobjects;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.AbstractJiraTabPage;
import com.atlassian.jira.pageobjects.pages.Tab;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class IssuePage extends AbstractJiraTabPage<Tab> {

	private final ViewIssuePage mainPage;

	public IssuePage(ViewIssuePage mainPage) {
		super(By.id("activitymodule"));
		this.mainPage = mainPage;
	}

	public static IssuePage gotoIssue(JiraTestedProduct jira, String issueKey) {
		ViewIssuePage viewIssuePage = jira.goTo(ViewIssuePage.class, issueKey);
		return jira.getPageBinder().bind(IssuePage.class, viewIssuePage);
	}

	@Override
	public TimedCondition isAt() {
		return mainPage.isAt();
	}

	@Override
	public String getUrl() {
		return mainPage.getUrl();
	}

	public boolean hasCIBuildsTab() {
		return hasTab(CIBuildsTab.class);
	}

	public CIBuildsTab openCIBuildsTab() {
		return openTab(CIBuildsTab.class);
	}

}
