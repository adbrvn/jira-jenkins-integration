/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jira.plugins.jenkins.AbstractTechnicalTest;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.DeserializationConfig;

import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

/**
 * Base Integration Testcase for REST resource testing
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public abstract class AbstractResourceTest extends AbstractTechnicalTest {

	private static ThreadLocal<Client> client = new ThreadLocal<Client>() {
		@Override
		@SuppressWarnings("Duplicates")
		protected Client initialValue() {
			DefaultClientConfig config = new DefaultClientConfig();
			JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
			jacksonProvider.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			config.getSingletons().add(jacksonProvider);
			return Client.create(config);
		}
	};
	private String login = ADMIN;
	private String loginPassword = login;

	protected void anonymous() {
		login = null;
		loginPassword = null;
	}

	protected void asAdmin() {
		loginAs(ADMIN);
	}

	protected void loginAs(String username) {
		loginAs(username, username);
	}

	protected void loginAs(String username, String password) {
		login = username;
		loginPassword = password;
	}

	/**
	 * Returns the {@link WebResource site resource}
	 */
	protected WebResource siteResource(String... path) {
		return siteResource(null, path);
	}

	/**
	 * Returns the {@link WebResource site resource}
	 */
	protected WebResource siteResource(@Nullable Site site, String... path) {
		Map<String, Object> values = new HashMap<>();
		UriBuilder uriBuilder = uriBuilder().path("site");
		if (site != null) {
			uriBuilder.path("{siteId}");
			values.put("siteId", site.getId());
		}
		if (path != null && path.length > 0) {
			for (String p : path) {
				uriBuilder.path(p);
			}
		}
		return resource(uriBuilder.buildFromMap(values));
	}

	/**
	 * Returns the {@link WebResource job resource}
	 */
	protected WebResource jobResource(String... path) {
		return jobResource(null, path);
	}

	/**
	 * Returns the {@link WebResource job resource}
	 */
	protected WebResource jobResource(@Nullable Job job, String... path) {
		Map<String, Object> values = new HashMap<>();
		UriBuilder uriBuilder = uriBuilder().path("job");
		if (job != null) {
			uriBuilder.path("{jobId}");
			values.put("jobId", job.getId());
		}
		if (path != null && path.length > 0) {
			for (String p : path) {
				uriBuilder.path(p);
			}
		}
		return resource(uriBuilder.buildFromMap(values));
	}

	/**
	 * Returns the {@link WebResource build resource}
	 */
	protected WebResource buildResource(Job job, String... path) {
		return buildResource(job, null, path);
	}

	/**
	 * Returns the {@link WebResource build resource}
	 */
	protected WebResource buildResource(Job job, int buildNumber, String... path) {
		return buildResource(job, new Build(job.getId(), buildNumber), path);
	}

	/**
	 * Returns the {@link WebResource build resource}
	 */
	protected WebResource buildResource(Job job, @Nullable Build build, String... path) {
		Map<String, Object> values = new HashMap<>();
		UriBuilder uriBuilder = uriBuilder().path("build").path("{jobHash}");
		values.put("jobHash", sha1Hex(job.getUrlName()));
		if (build != null) {
			uriBuilder.path("{buildNumber}");
			values.put("buildNumber", build.getNumber());
		}
		if (path != null && path.length > 0) {
			for (String p : path) {
				uriBuilder.path(p);
			}
		}
		return resource(uriBuilder.buildFromMap(values));
	}

	/**
	 * Returns the {@link WebResource configuration resource}
	 */
	protected WebResource configurationResource() {
		return resource(uriBuilder().path("configuration").build());
	}

	/**
	 * Returns the {@link WebResource release-report resource}
	 */
	protected WebResource releaseReportResource() {
		return resource(uriBuilder().path("release-report").build());
	}

	/**
	 * Returns a new {@link UriBuilder} to build {@link URI}s targeted to the Jenkins API
	 */
	protected UriBuilder uriBuilder() {
		try {
			return UriBuilder.fromUri(backdoor.environmentData().getBaseUrl().toURI()).path("rest").path("jenkins").path("1.0");
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Malformed base url " + e.getMessage(), e);
		}
	}

	private WebResource resource(URI uri) {
		WebResource resource = client().resource(uri);
		if (login != null && loginPassword != null) {
			resource.addFilter(new HTTPBasicAuthFilter(login, loginPassword));
		}
		return resource;
	}

	private Client client() {
		return client.get();
	}

}
