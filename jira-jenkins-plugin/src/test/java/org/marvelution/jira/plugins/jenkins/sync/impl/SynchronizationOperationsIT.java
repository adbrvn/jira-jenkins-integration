/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.concurrent.TimeUnit;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;
import org.marvelution.jira.plugins.jenkins.rest.AbstractResourceTest;
import org.marvelution.jji.testkit.wiremock.WireMockRule;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;

import static com.jayway.awaitility.Awaitility.await;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class SynchronizationOperationsIT extends AbstractResourceTest {

	@Rule
	public WireMockRule wireMock = new WireMockRule();

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	private Site addSiteForTest() {
		Site site = new Site();
		site.setType(SiteType.JENKINS);
		site.setName(testName.getMethodName());
		site.setRpcUrl(wireMock.serverUri());
		site.setAutoLink(true);
		return siteResource().entity(site, APPLICATION_JSON_TYPE).post(Site.class);
	}

	@Test
	public void testFolderJobSynchronization() throws Exception {
		int expectedJobCount = 5;
		Site site = addSiteForTest();
		await().atMost(30, TimeUnit.SECONDS)
		       .pollInterval(1, TimeUnit.SECONDS)
		       .until(() -> backdoor.sites().getSite(site.getId()).getJobs().size() == expectedJobCount);
		Site updated = backdoor.sites().getSite(site.getId());
		assertThat(updated.isSupportsBackLink(), is(false));
		assertThat(updated.isUseCrumbs(), is(false));
		assertThat(updated.getJobs(), hasSize(expectedJobCount));
		assertThat(updated.getJobs().stream().filter(Job::isLinked).count(), is((long) expectedJobCount));
		updated.getJobs().stream()
		       .filter(job -> job.getName().equals("Cave"))
		       .forEach(job -> await(job.getUrlName()).atMost(30, TimeUnit.SECONDS)
		                                              .pollInterval(1, TimeUnit.SECONDS)
		                                              .until(() -> !backdoor.jobs().getJob(job.getId()).isDeleted()));
		updated.getJobs().parallelStream()
		       .filter(job -> !job.getName().equals("Cave"))
		       .forEach(job -> await(job.getUrlName()).atMost(30, TimeUnit.SECONDS)
		                                              .pollInterval(1, TimeUnit.SECONDS)
		                                              .until(() -> backdoor.jobs().getJob(job.getId()).isDeleted()));
	}

}
