/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.LicenseControl;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class ExtendedLicenseControl extends LicenseControl {

	public static final String LICENSE_FOR_DEVELOPMENT = "\n"
			+ "AAABdg0ODAoPeNqNkl1PwjAYhe/7K5p4vWWbfCjJLtA1CsGNMMDEu9K9QAXape1A/r0FISyOoRe9a\n"
			+ "Pp+nPOc3r1DhiNg2Auw3+r47U7Dw10SjXHg+U30yRV1cyWzghn3cHE0qC1nkIFeuZQZvoXQqALQQg\n"
			+ "GIpcxzUO7AFggNJOOGSxGSeExGw1EvJSguNjNQyXxix+jQ8a8sYFKBW6kbFootqYaIGggP0hw/cLw\n"
			+ "AnVaN9znEdANhRKZkkAzJ6PxCvnKu9se24WPrFaVWP6heFD6RNnEa0UfgtJJ+23m5f3y4ZlfOzY5a\n"
			+ "RWWvTG5catZUa06FWyZSL+c2yfq+tJhppnh+JFlHum5ffRA13MsubyuuJPRGuTAgqGAV5jewVsacJ\n"
			+ "NuAJmIl5E6glMShPU7T81CiFlRwTY92+r1RF2ewhbW0LDRe/7TiuVTYgDZcLNCzgmPx739zqp3avs\n"
			+ "OoAEVwwfyfweUUKtT+wHg2X58e2dJ1QS+ZfwM/kUp8MC0CFQCABGWOTOTftvnMbFMAyaAwSLqw0gI\n"
			+ "UDk5o2AIF68UZM4Z4jNMjHiNMSH8=X02ia";

	ExtendedLicenseControl(JIRAEnvironmentData environmentData) {
		super(environmentData);
	}

	public boolean switchToTestingLicense() {
		return createResource().path("license").path("set").post(Boolean.class, LICENSE_FOR_DEVELOPMENT);
	}

}
