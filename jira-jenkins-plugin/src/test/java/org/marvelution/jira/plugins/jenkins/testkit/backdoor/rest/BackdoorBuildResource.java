/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor.rest;

import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.services.BuildService;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@AnonymousAllowed
@Path("build")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorBuildResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(BackdoorBuildResource.class);
	private static final String LOGGING_PREFIX = "BACKDOOR NOT FOR PRODUCTION:";
	private final BuildService buildService;

	public BackdoorBuildResource(BuildService buildService) {
		this.buildService = buildService;
	}

	@POST
	public Build save(Build job) {
		LOGGER.warn("{} Saving job {}", LOGGING_PREFIX, job);
		return buildService.save(job);
	}

	@GET
	@Path("{buildId}")
	public Build get(@PathParam("buildId") Integer buildId) {
		LOGGER.warn("{} Returning build with id {}", LOGGING_PREFIX, buildId);
		return buildService.get(buildId);
	}

	@POST
	@Path("{buildId}/link")
	public void linkBuildToIssue(@PathParam("buildId") int buildId, String issueKey) {
		LOGGER.warn("{} Linking build with id {} to issue {}", LOGGING_PREFIX, buildId, issueKey);
		Optional.ofNullable(buildService.get(buildId)).ifPresent(build -> buildService.link(build, issueKey));
	}

	@DELETE
	@Path("{buildId}")
	public void delete(@PathParam("buildId") int buildId) {
		LOGGER.warn("{} Deleting build with id {}", LOGGING_PREFIX, buildId);
		Optional.ofNullable(buildService.get(buildId)).ifPresent(buildService::delete);
	}

}
