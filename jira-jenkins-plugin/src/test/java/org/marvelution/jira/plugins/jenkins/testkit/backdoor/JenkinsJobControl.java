/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor;

import java.util.List;
import java.util.NoSuchElementException;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;

import static java.lang.String.valueOf;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class JenkinsJobControl extends JenkinsBackdoorControl<JenkinsJobControl> {

	private static final GenericType<List<Job>> LIST_GENERIC_TYPE = new GenericType<List<Job>>() {};
	private final JenkinsSiteControl siteControl;

	JenkinsJobControl(JIRAEnvironmentData environmentData, JenkinsSiteControl siteControl) {
		super(environmentData);
		this.siteControl = siteControl;
	}

	public List<Job> getJobs() {
		return createJobResource().get(LIST_GENERIC_TYPE);
	}

	public Job getJob(int jobId) {
		return createJobResource().path(valueOf(jobId)).get(Job.class);
	}

	public Job addJob(String name) {
		int siteId = siteControl.getSites().stream().findFirst().map(Site::getId)
		                        .orElseThrow(NoSuchElementException::new);
		return addJob(siteId, name);
	}

	public Job addJob(int siteId, String name) {
		return saveJob(new Job(siteId, name));
	}

	public Job saveJob(Job job) {
		return createJobResource().type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE).post(Job.class, job);
	}

}
