/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.pageobjects;

import javax.inject.Inject;

import com.atlassian.jira.pageobjects.pages.Tab;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import org.openqa.selenium.By;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class CIBuildsTab implements Tab {

	private static final String JENKINS_ISSUE_PANEL = "jenkins-issue-panel";
	private PageElement linkItem;
	@ElementBy(id = "issue_actions_container")
	private PageElement issueActionsContainer;
	@Inject
	private PageElementFinder elementFinder;

	@Init
	public void init() {
		linkItem = elementFinder.find(By.id(JENKINS_ISSUE_PANEL));
	}

	private PageElement getBuildsList() {
		return issueActionsContainer.find(By.className("jenkins-build-list"));
	}

	@Override
	public String linkId() {
		return JENKINS_ISSUE_PANEL;
	}

	@Override
	public TimedCondition isOpen() {
		return linkItem.timed().hasClass("active");
	}

	public boolean hasBuilds() {
		return getBuildsList().isPresent();
	}

}
