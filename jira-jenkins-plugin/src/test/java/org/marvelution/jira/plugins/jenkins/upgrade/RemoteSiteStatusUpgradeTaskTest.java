/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.upgrade;

import java.util.ArrayList;
import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.services.SiteService;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link RemoteSiteStatusUpgradeTask}
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@RunWith(MockitoJUnitRunner.class)
public class RemoteSiteStatusUpgradeTaskTest {

	@Mock
	private JenkinsPluginUtil pluginUtil;
	@Mock
	private SiteService siteService;
	@Mock
	private CommunicatorFactory communicatorFactory;
	@Mock
	private Communicator communicator;
	private RemoteSiteStatusUpgradeTask upgradeTask;

	@Before
	public void setUp() throws Exception {
		upgradeTask = new RemoteSiteStatusUpgradeTask(pluginUtil, siteService, communicatorFactory);
		when(communicatorFactory.get(any(Site.class))).thenReturn(communicator);
	}

	@Test
	public void testDoUpgrade() throws Exception {
		when(communicator.isCrumbSecurityEnabled()).thenReturn(true).thenReturn(false);
		when(communicator.isJenkinsPluginInstalled()).thenReturn(true).thenReturn(false);
		List<Site> sites = new ArrayList<>(2);
		Site siteA = new Site(1);
		siteA.setName("Site A");
		sites.add(siteA);
		Site siteB = new Site(2);
		siteB.setName("Site B");
		sites.add(siteB);
		when(siteService.getAll(false)).thenReturn(sites);
		upgradeTask.doUpgrade();
		verify(siteService).getAll(false);
		verify(communicatorFactory).get(siteA);
		verify(communicatorFactory).get(siteB);
		verify(communicator, times(2)).isCrumbSecurityEnabled();
		verify(communicator, times(2)).isJenkinsPluginInstalled();
		ArgumentCaptor<Site> argumentCaptor = ArgumentCaptor.forClass(Site.class);
		verify(siteService, times(2)).save(argumentCaptor.capture());
		Site value = argumentCaptor.getAllValues().get(0);
		assertThat(value.getId(), is(1));
		assertThat(value.isUseCrumbs(), is(true));
		assertThat(value.isSupportsBackLink(), is(true));
		value = argumentCaptor.getAllValues().get(1);
		assertThat(value.getId(), is(2));
		assertThat(value.isUseCrumbs(), is(false));
		assertThat(value.isSupportsBackLink(), is(false));
	}

}
