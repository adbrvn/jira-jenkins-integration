/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.net.URI;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.SiteType;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Integration Authz Testcase for the build REST resource
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class BuildResourceAuthzIT extends AbstractResourceAuthzTest {

	private static Job job;

	@BeforeClass
	public static void setup() {
		backdoor.sites().addSite(SiteType.JENKINS, "Local Test", URI.create("http://localhost:8080"));
		job = backdoor.jobs().addJob("test-job");
	}

	@AfterClass
	public static void teardown() {
		backdoor.sites().clearSites();
	}

	@Test
	public void testSyncJob() throws Exception {
		testAuthzPut(buildResource(job), anonymous(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testGetBuildLinks() throws Exception {
		testAuthzGet(buildResource(job, 1, "links"), anonymous(ClientResponse.Status.OK));
	}

}
