/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins;

import javax.annotation.Nonnull;
import javax.inject.Singleton;

import org.marvelution.jira.plugins.jenkins.testkit.backdoor.JenkinsBackdoor;

import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.pageobjects.inject.InjectionContext;
import com.atlassian.webdriver.testing.runner.AbstractInjectingRunner;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.runner.RunWith;
import org.junit.runners.model.InitializationError;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@RunWith(AbstractTechnicalTest.TechnicalTestInjectionRunner.class)
public abstract class AbstractTechnicalTest extends AbstractIntegrationTest {

	public static class TechnicalTestInjectionRunner extends AbstractInjectingRunner implements InjectionContext {

		private volatile Injector injector;

		public TechnicalTestInjectionRunner(Class<?> klass) throws InitializationError {
			super(klass);
			injector = Guice.createInjector(new TechnicalTestInjectionRunner.BackdoorModule());
		}

		@Override
		protected InjectionContext getInjectionContext() {
			return this;
		}

		@Nonnull
		@Override
		public <T> T getInstance(@Nonnull Class<T> targetClass) {
			return injector.getInstance(targetClass);
		}

		@Override
		public void injectStatic(@Nonnull Class<?> targetClass) {
			injector.createChildInjector(new AbstractModule() {
				@Override
				protected void configure() {
					requestStaticInjection(targetClass);
				}
			});
		}

		@Override
		public void injectMembers(@Nonnull Object target) {
			injector.injectMembers(target);
		}

		@Nonnull
		@Override
		public <T> T inject(@Nonnull T target) {
			injector.injectMembers(target);
			return target;
		}

		private class BackdoorModule extends AbstractModule {

			@Override
			protected void configure() {
				bind(JIRAEnvironmentData.class).to(TestKitLocalEnvironmentData.class).in(Singleton.class);
				bind(Backdoor.class).to(JenkinsBackdoor.class);
				bind(JenkinsBackdoor.class);
			}

		}
	}
}
