/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor;

import java.net.URI;
import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;

import com.atlassian.jira.testkit.client.JIRAEnvironmentData;
import com.sun.jersey.api.client.GenericType;

import static java.lang.String.valueOf;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class JenkinsSiteControl extends JenkinsBackdoorControl<JenkinsSiteControl> {

	private static final GenericType<List<Site>> LIST_GENERIC_TYPE = new GenericType<List<Site>>() {};

	JenkinsSiteControl(JIRAEnvironmentData environmentData) {
		super(environmentData);
	}

	public List<Site> getSites() {
		return createSiteResource().get(LIST_GENERIC_TYPE);
	}

	public void clearSites() {
		createSiteResource().delete();
	}

	public Site getSite(int siteId) {
		return getSite(siteId, true);
	}

	public Site getSite(int siteId, boolean includeJobs) {
		return createSiteResource().path(valueOf(siteId)).queryParam("includeJobs", valueOf(includeJobs)).get(Site.class);
	}

	public Site addSite(SiteType type, String name, URI rpcUrl) {
		return addSite(type, name, rpcUrl, false);
	}

	public Site addSite(SiteType type, String name, URI rpcUrl, boolean autoLink) {
		Site site = new Site();
		site.setType(type);
		site.setName(name);
		site.setRpcUrl(rpcUrl);
		site.setAutoLink(autoLink);
		return saveSite(site);
	}

	public Site saveSite(Site site) {
		return createSiteResource().type(APPLICATION_JSON_TYPE).accept(APPLICATION_JSON_TYPE).post(Site.class, site);
	}

}
