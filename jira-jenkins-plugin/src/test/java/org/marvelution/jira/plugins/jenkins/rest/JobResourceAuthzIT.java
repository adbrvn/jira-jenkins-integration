/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.net.URI;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.RestData;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.Before;
import org.junit.Test;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * Integration Authz Testcase for the job REST resource
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class JobResourceAuthzIT extends AbstractResourceAuthzTest {

	private Job job;

	@Before
	public void setUp() throws Exception {
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
		job = backdoor.jobs().addJob(site.getId(), testName.getMethodName());
	}

	@Test
	public void testGetAll() throws Exception {
		testAuthzGet(jobResource(),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testGetJob() throws Exception {
		testAuthzGet(jobResource(job),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testRemoveJob() throws Exception {
		job.setDeleted(true);
		backdoor.jobs().saveJob(job);
		testAuthzDelete(jobResource(job),
		                anonymous(ClientResponse.Status.UNAUTHORIZED),
		                authenticatedUser(ClientResponse.Status.FORBIDDEN),
		                administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testGetSyncState() throws Exception {
		testAuthzGet(jobResource(job, "sync", "status"),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testSyncJob() throws Exception {
		testAuthzPut(jobResource(job, "sync"),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testEnableJobAutoLink() throws Exception {
		testAuthzPost(jobResource(job, "autolink"),
		              original -> original.entity(new RestData(Boolean.TRUE.toString()), APPLICATION_JSON_TYPE),
		              anonymous(ClientResponse.Status.UNAUTHORIZED),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN),
		              administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testRemoveAllBuilds() throws Exception {
		testAuthzDelete(jobResource(job, "builds"),
		                anonymous(ClientResponse.Status.UNAUTHORIZED),
		                authenticatedUser(ClientResponse.Status.FORBIDDEN),
		                administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testRebuildJobCache() throws Exception {
		testAuthzPost(jobResource(job, "rebuild"),
		              anonymous(ClientResponse.Status.UNAUTHORIZED),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN),
		              administrator(ClientResponse.Status.NO_CONTENT));
	}

}
