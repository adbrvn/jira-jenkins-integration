/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import org.marvelution.jira.plugins.jenkins.model.ErrorMessages;

import com.sun.jersey.api.client.ClientResponse;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Description;
import org.hamcrest.DiagnosingMatcher;
import org.hamcrest.Matcher;

/**
 * Common project specific Matchers
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class Matchers {

	/**
	 * Returns a {@link Matcher} to match is an {@link ErrorMessages.ErrorMessage} if for the specified {@code field}
	 */
	public static Matcher<ErrorMessages.ErrorMessage> errorForField(final String field) {
		return new DiagnosingMatcher<ErrorMessages.ErrorMessage>() {
			@Override
			protected boolean matches(Object item, Description mismatchDescription) {
				if (item == null) {
					mismatchDescription.appendText("null");
					return false;
				} else if (!(item instanceof ErrorMessages.ErrorMessage)) {
					mismatchDescription.appendValue(item).appendValue(" is a " + item.getClass().getName());
					return false;
				}
				ErrorMessages.ErrorMessage error = (ErrorMessages.ErrorMessage) item;
				return field.equals(error.field);
			}

			@Override
			public void describeTo(Description description) {
				description.appendText("error for field ").appendValue(field);
			}
		};
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches {@link ClientResponse.Status#OK}.
	 */
	public static Matcher<ClientResponse> successful() {
		return status(ClientResponse.Status.OK);
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches {@link ClientResponse.Status#NO_CONTENT}.
	 */
	public static Matcher<ClientResponse> noContent() {
		return status(ClientResponse.Status.NO_CONTENT);
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches {@link ClientResponse.Status#BAD_REQUEST}.
	 */
	public static Matcher<ClientResponse> badRequest() {
		return status(ClientResponse.Status.BAD_REQUEST);
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches {@link ClientResponse.Status#NOT_FOUND}.
	 */
	public static Matcher<ClientResponse> notFound() {
		return status(ClientResponse.Status.NOT_FOUND);
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches {@link ClientResponse.Status#FORBIDDEN}.
	 */
	public static Matcher<ClientResponse> forbidden() {
		return status(ClientResponse.Status.FORBIDDEN);
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches {@link ClientResponse.Status#UNAUTHORIZED}.
	 */
	public static Matcher<ClientResponse> unauthorized() {
		return status(ClientResponse.Status.UNAUTHORIZED);
	}

	/**
	 * Creates a matcher that matches if the examined object response status matches the specified {@link ClientResponse.Status expected}
	 * status.
	 */
	public static Matcher<ClientResponse> status(final ClientResponse.Status expected) {
		return new DiagnosingMatcher<ClientResponse>() {

			@Override
			public void describeTo(Description description) {
				description.appendText("response with status ")
				           .appendValue(expected.getStatusCode())
				           .appendText(" ")
				           .appendText(expected.getReasonPhrase());
			}

			@Override
			protected boolean matches(Object item, Description mismatchDescription) {
				if (item == null) {
					mismatchDescription.appendText("null");
					return false;
				}
				if (!(item instanceof ClientResponse)) {
					mismatchDescription.appendValue(item).appendValue(" is a " + item.getClass().getName());
					return false;
				}
				ClientResponse response = (ClientResponse) item;
				if (response.getClientResponseStatus() != expected) {
					mismatchDescription.appendText("resulted in " + response.getClientResponseStatus());
					try {
						String message = response.getEntity(String.class);
						if (StringUtils.isNotBlank(message)) {
							mismatchDescription.appendText(" response body: ").appendText(message);
						}
					} catch (Exception ignore) {
					}
					return false;
				}
				return true;
			}

		};
	}

}
