/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.net.URI;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.RestData;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;

import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Integration Authz Testcase for the site REST resource
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class SiteResourceAuthzIT extends AbstractResourceAuthzTest {

	private Site site;

	@Before
	public void setUp() throws Exception {
		site = backdoor.sites().addSite(SiteType.JENKINS, "Jenkins", URI.create("http://localhost:8080"));
	}

	@After
	public void tearDown() throws Exception {
		backdoor.sites().clearSites();
	}

	@Test
	public void testGetAll() throws Exception {
		testAuthzGet(siteResource(),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testGetSite() throws Exception {
		testAuthzGet(siteResource(site),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testAddUpdateDeleteSite() {
		ClientResponse response = testAuthzPost(siteResource(),
		                                        original -> {
			                                        Site site = new Site();
			                                        site.setType(SiteType.JENKINS);
			                                        site.setName("authz-add");
			                                        site.setRpcUrl(URI.create("http://localhost:8080"));
			                                        return original.entity(site, MediaType.APPLICATION_JSON_TYPE);
		                                        },
		                                        anonymous(ClientResponse.Status.UNAUTHORIZED),
		                                        authenticatedUser(ClientResponse.Status.FORBIDDEN),
		                                        administrator(ClientResponse.Status.OK));
		final Site site = response.getEntity(Site.class);
		assertThat(site.getName(), is("authz-add"));
		response = testAuthzPost(siteResource(site),
		                         original -> {
			                         site.setName("authz-update");
			                         return original.entity(site, MediaType.APPLICATION_JSON_TYPE);
		                         },
		                         anonymous(ClientResponse.Status.UNAUTHORIZED),
		                         authenticatedUser(ClientResponse.Status.FORBIDDEN),
		                         administrator(ClientResponse.Status.OK));
		final Site updated = response.getEntity(Site.class);
		assertThat(updated.getName(), is("authz-update"));
		testAuthzDelete(siteResource(updated),
		                anonymous(ClientResponse.Status.UNAUTHORIZED),
		                authenticatedUser(ClientResponse.Status.FORBIDDEN),
		                administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testSyncJobList() throws Exception {
		testAuthzPost(siteResource(site, "sync"),
		              anonymous(ClientResponse.Status.UNAUTHORIZED),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN),
		              administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testSyncStatus() throws Exception {
		testAuthzGet(siteResource(site, "sync", "status"),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testEnableAutoLink() throws Exception {
		testAuthzPost(siteResource(site, "autolink"),
		              original -> original.entity(new RestData(Boolean.TRUE.toString()),
		                                          MediaType.APPLICATION_JSON_TYPE),
		              anonymous(ClientResponse.Status.UNAUTHORIZED),
		              authenticatedUser(ClientResponse.Status.FORBIDDEN),
		              administrator(ClientResponse.Status.NO_CONTENT));
	}

	@Test
	public void testSiteStatus() throws Exception {
		testAuthzGet(siteResource(site, "status"),
		             anonymous(ClientResponse.Status.UNAUTHORIZED),
		             authenticatedUser(ClientResponse.Status.FORBIDDEN),
		             administrator(ClientResponse.Status.OK));
	}

	@Test
	public void testRemoveJobs() throws Exception {
		testAuthzDelete(siteResource(site, "jobs"),
		                anonymous(ClientResponse.Status.UNAUTHORIZED),
		                authenticatedUser(ClientResponse.Status.FORBIDDEN),
		                administrator(ClientResponse.Status.NO_CONTENT));
	}

}
