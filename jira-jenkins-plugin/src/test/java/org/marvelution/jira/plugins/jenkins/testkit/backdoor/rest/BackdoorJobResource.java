/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor.rest;

import java.util.List;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.services.JobService;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@AnonymousAllowed
@Path("job")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorJobResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(BackdoorJobResource.class);
	private static final String LOGGING_PREFIX = "BACKDOOR NOT FOR PRODUCTION:";
	private final JobService jobService;

	public BackdoorJobResource(JobService jobService) {
		this.jobService = jobService;
	}

	@GET
	public List<Job> getAll() {
		LOGGER.warn("{} Returning all available jobs", LOGGING_PREFIX);
		return jobService.getAll(true);
	}

	@POST
	public Job save(Job job) {
		LOGGER.warn("{} Saving job {}", LOGGING_PREFIX, job);
		return jobService.save(job);
	}

	@GET
	@Path("{jobId}")
	public Job get(@PathParam("jobId") Integer jobId) {
		LOGGER.warn("{} Returning job with id {}", LOGGING_PREFIX, jobId);
		return jobService.get(jobId);
	}

	@DELETE
	@Path("{jobId}")
	public void delete(@PathParam("jobId") int jobId) {
		LOGGER.warn("{} Deleting job with id {}", LOGGING_PREFIX, jobId);
		Optional.ofNullable(jobService.get(jobId)).ifPresent(jobService::delete);
	}

}
