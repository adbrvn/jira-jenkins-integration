/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit.backdoor.rest;

import java.util.List;
import java.util.Optional;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.JobService;
import org.marvelution.jira.plugins.jenkins.services.SiteService;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@AnonymousAllowed
@Path("site")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BackdoorSiteResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(BackdoorSiteResource.class);
	private static final String LOGGING_PREFIX = "BACKDOOR NOT FOR PRODUCTION:";
	private final SiteService siteService;
	private final JobService jobService;

	public BackdoorSiteResource(SiteService siteService, JobService jobService) {
		this.siteService = siteService;
		this.jobService = jobService;
	}

	@GET
	public List<Site> getAll() {
		LOGGER.warn("{} Returning all installed sites", LOGGING_PREFIX);
		return siteService.getAll(true);
	}

	@POST
	public Site save(Site site) {
		LOGGER.warn("{} Saving site {}", LOGGING_PREFIX, site);
		return siteService.save(site);
	}

	@DELETE
	public void clear() {
		LOGGER.warn("{} Clearing all installed sites", LOGGING_PREFIX);
		siteService.getAll(false).forEach(siteService::delete);
	}

	@GET
	@Path("{siteId}")
	@Nullable
	public Site get(@PathParam("siteId") Integer siteId, @QueryParam("includeJobs") boolean includeJobs) {
		LOGGER.warn("{} Returning site with id {}", LOGGING_PREFIX, siteId);
		return Optional.ofNullable(siteService.get(siteId))
		               .map(site -> {
			               if (includeJobs) {
				               site.setJobs(jobService.getAllBySite(site, true));
			               }
			               return site;
		               })
		               .orElse(null);
	}

	@DELETE
	@Path("{siteId}")
	public void delete(@PathParam("siteId") int siteId) {
		LOGGER.warn("{} Deleting site with id {}", LOGGING_PREFIX, siteId);
		Optional.ofNullable(siteService.get(siteId)).ifPresent(siteService::delete);
	}

}
