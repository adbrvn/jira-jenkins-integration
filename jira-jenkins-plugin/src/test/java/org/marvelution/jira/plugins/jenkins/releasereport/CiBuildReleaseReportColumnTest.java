/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.releasereport;

import java.util.Arrays;
import java.util.Map;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.sync.BuildIssueLinksUpdatedEvent;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import com.atlassian.cache.memory.MemoryCacheManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryRequest;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.marvelution.jira.plugins.jenkins.model.Result.ABORTED;
import static org.marvelution.jira.plugins.jenkins.model.Result.FAILURE;
import static org.marvelution.jira.plugins.jenkins.model.Result.SUCCESS;
import static org.marvelution.jira.plugins.jenkins.model.Result.UNSTABLE;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testcase for the {@link CiBuildReleaseReportColumn}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class CiBuildReleaseReportColumnTest {

	@Mock
	private VersionWarningCategoryRequest context;
	@Mock
	private JenkinsPluginUtil pluginUtil;
	@Mock
	private ReleaseReportHelper reportHelper;
	@Mock
	private EventPublisher eventPublisher;
	@Mock
	private I18nHelper i18nHelper;
	@Mock
	private SoyTemplateRenderer soyTemplateRenderer;
	private CiBuildReleaseReportColumn ciBuildReleaseReportColumn;
	@Mock
	private Issue issue;
	@Captor
	private ArgumentCaptor<Map<String, Object>> contextCaptor;

	@Before
	public void setUp() throws Exception {
		ciBuildReleaseReportColumn = new CiBuildReleaseReportColumn(pluginUtil, reportHelper, eventPublisher, i18nHelper,
		                                                            new MemoryCacheManager(), soyTemplateRenderer);
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testRenderIssues() throws Exception {
		when(issue.getKey()).thenReturn("JJI-1");
		when(reportHelper.getBuildInformation("JJI-1")).thenAnswer(invocation -> {
			Job foo = new Job(1, 1, "foo");
			Build build1 = new Build(1, 1, 1);
			build1.setResult(SUCCESS);
			foo.getBuilds().add(build1);
			Build build2 = new Build(2, 1, 2);
			build2.setResult(UNSTABLE);
			foo.getBuilds().add(build2);
			Job bar = new Job(2, 2, "bar");
			Build build3 = new Build(3, 2, 1);
			build3.setResult(FAILURE);
			bar.getBuilds().add(build3);
			Build build4 = new Build(4, 2, 2);
			build4.setResult(ABORTED);
			bar.getBuilds().add(build4);
			return Arrays.asList(foo, bar);
		});
		when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenReturn("JJI-1");
		ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
		verify(reportHelper).getBuildInformation("JJI-1");
		verify(soyTemplateRenderer).render(anyString(), anyString(), contextCaptor.capture());
		Map<String, Object> context = contextCaptor.getValue();
		assertThat(context.keySet(), hasSize(6));
		assertThat(context.keySet(), hasItem("issueKey"));
		assertThat(context.get("issueKey"), is("JJI-1"));
		assertThat(context.keySet(), hasItem("count"));
		assertThat(context.get("count"), is(2));
		assertThat(context.keySet(), hasItem("successfulBuildCount"));
		assertThat(context.get("successfulBuildCount"), is(0));
		assertThat(context.keySet(), hasItem("unstableBuildCount"));
		assertThat(context.get("unstableBuildCount"), is(1));
		assertThat(context.keySet(), hasItem("failedBuildCount"));
		assertThat(context.get("failedBuildCount"), is(0));
		assertThat(context.keySet(), hasItem("unknownBuildCount"));
		assertThat(context.get("unknownBuildCount"), is(1));
		Map<String, String> issueHtml = ciBuildReleaseReportColumn.getIssueHtml();
		assertThat(issueHtml.keySet(), hasSize(1));
		assertThat(issueHtml.keySet(), hasItem("JJI-1"));
		assertThat(issueHtml.get("JJI-1"), is("JJI-1"));
	}

	@Test
	@SuppressWarnings("unchecked")
	public void testRenderIssues_BuildIssueLinksUpdateEvent() {
		when(issue.getKey()).thenReturn("JJI-1");
		when(reportHelper.getBuildInformation("JJI-1")).thenAnswer(invocation -> {
			Job foo = new Job(1, 1, "foo");
			Build build1 = new Build(1, 1, 1);
			build1.setResult(SUCCESS);
			foo.getBuilds().add(build1);
			return singletonList(foo);
		});
		when(soyTemplateRenderer.render(anyString(), anyString(), anyMap())).thenReturn("JJI-1");
		// Initial call to fill the cache
		ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
		verify(reportHelper, times(1)).getBuildInformation("JJI-1");
		verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
		// Calling renderIssues should return from cache
		ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
		verify(reportHelper, times(1)).getBuildInformation("JJI-1");
		verify(soyTemplateRenderer, times(1)).render(anyString(), anyString(), anyMap());
		// BuildIssueLinksUpdateEvent should clear cache
		ciBuildReleaseReportColumn.onBuildIssueLinksUpdatedEvent(new BuildIssueLinksUpdatedEvent(1, singleton("JJI-1")));
		ciBuildReleaseReportColumn.renderIssues(singleton(issue), context);
		verify(reportHelper, times(2)).getBuildInformation("JJI-1");
		verify(soyTemplateRenderer, times(2)).render(anyString(), anyString(), anyMap());
	}

}
