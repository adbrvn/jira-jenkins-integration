/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.testkit;

import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.marvelution.jira.plugins.jenkins.AbstractTechnicalTest;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Result;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;
import org.marvelution.jira.plugins.jenkins.model.TestResults;

import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.rest.api.issue.IssueFields;
import com.atlassian.jira.rest.api.issue.IssueUpdateRequest;
import com.atlassian.jira.testkit.beans.Priority;
import com.atlassian.jira.testkit.beans.UserDTO;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.restclient.IssueClient;
import com.atlassian.jira.testkit.client.restclient.Project;
import com.atlassian.jira.testkit.client.restclient.ProjectClient;
import com.atlassian.jira.testkit.client.restclient.Version;
import com.atlassian.jira.testkit.client.restclient.VersionClient;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.sun.jersey.api.client.ClientResponse;
import org.junit.After;
import org.junit.Test;

import static com.atlassian.extras.common.org.springframework.util.StringUtils.capitalize;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withId;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withKey;
import static com.atlassian.jira.rest.api.issue.ResourceRef.withName;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

/**
 * Tests here are for generating data to be used for development and/or screenshot taking for the marketplace.
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class DataGenerator extends AbstractTechnicalTest {

	private Random random = new Random();

	@After
	public void reIndexIssues() {
		backdoor.indexing().reindexAll();
	}

	@Test
	public void clearAllProjectsAndSites() {
		backdoor.sites().clearSites();
		ProjectClient client = backdoor.createRestClient(ProjectClient.class);
		client.getProjects().stream().map(project -> project.key).forEach(client::delete);
	}

	@Test
	public void generateUsersForUI() throws JSONException {
		Stream.of("mark", "astrid", "jeff", "luuk", "alina", "mike", "james")
		      .peek(username -> backdoor.usersAndGroups()
		                                .addUserEvenIfUserExists(username, username, capitalize(username),
		                                                         username + "@fake.marvelution.com", false)
		                                .addUserToGroup(username, "jira-software-users"))
		      .peek(username -> {
			      try {
				      ClientResponse response = backdoor.rawRestApiControl()
				                                        .createResourceForModule("api").path("user").path("avatars")
				                                        .queryParam("username", username)
				                                        .get(ClientResponse.class);
				      JSONArray avatars = new JSONObject(response.getEntity(String.class)).optJSONArray("system");
				      JSONObject avatar = avatars.getJSONObject(random.nextInt(avatars.length()));
				      backdoor.rawRestApiControl()
				              .createResourceForModule("api").path("user").path("avatar")
				              .queryParam("username", username)
				              .put(avatar.toString());
			      } catch (JSONException e) {
				      e.printStackTrace();
			      }
		      })
		      .collect(toList());
	}

	@Test
	public void generateDataForJenkinsConfigurationPage() throws Exception {
		backdoor.sites().addSite(SiteType.HUDSON, "Hudson CI", URI.create("http://ci.hudson-ci.org/"));
		backdoor.sites().addSite(SiteType.HUDSON, "Jenkins CI", URI.create("https://ci.jenkins.io/"));
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "Local Jenkins", URI.create("http://localhost:8080"));
		site.setAutoLink(true);
		backdoor.sites().saveSite(site);
		backdoor.jobs().addJob(site.getId(), "SonarQube Integration");
		Job job = backdoor.jobs().addJob(site.getId(), "Jenkins Integration for JIRA");
		job.setLinked(true);
		job.setLastBuild(50);
		backdoor.jobs().saveJob(job);
		backdoor.jobs().addJob(site.getId(), "Cloud");
		Job frontendContainer = new Job(site.getId(), "Frontend Container");
		frontendContainer.setUrlName("Cloud/job/Frontend%20Container");
		frontendContainer.setDeleted(true);
		backdoor.jobs().saveJob(frontendContainer);
		Job backendContainer = new Job(site.getId(), "Backend-Container");
		backendContainer.setDeleted(true);
		backendContainer.setUrlName("Cloud/job/Backend-Container");
		backdoor.jobs().saveJob(backendContainer);
	}

	@Test
	public void generateDataForIssueCIBuildPanel() throws Exception {
		Project project = generateScrumProject("My Brilliant Software Project");
		String priorityId = backdoor.priorities().getPriorities().stream()
		                            .filter(p -> p.getName().equals("High"))
		                            .findFirst()
		                            .map(Priority::getId)
		                            .orElse("1");
		String issueType = backdoor.issueType().getIssueTypes().stream()
		                           .filter(type -> type.getName().equals("Story"))
		                           .findFirst()
		                           .map(IssueTypeControl.IssueType::getId)
		                           .get();
		IssueCreateResponse issue = backdoor.issues().createIssue(project.key, "Fantastic Feature", null, priorityId, issueType);
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "CI Panel Jenkins", URI.create("http://localhost:8080"));
		Job job = backdoor.jobs().addJob(site.getId(), "Software Job");
		job.setLinked(true);
		job.setLastBuild(10);
		backdoor.jobs().saveJob(job);
		Build build1 = backdoor.builds().addBuild(job, "SCM triggered build", Result.UNSTABLE);
		build1.setDuration(TimeUnit.SECONDS.toMillis(510));
		build1.setTimestamp(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2));
		backdoor.builds().saveBuild(build1);
		backdoor.builds().linkBuildToIssue(build1, issue.key);
		Build build2 = backdoor.builds().addBuild(job, "Schedule triggered build", Result.FAILURE);
		build2.setDuration(TimeUnit.SECONDS.toMillis(204));
		build2.setTimestamp(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1));
		backdoor.builds().saveBuild(build2);
		backdoor.builds().linkBuildToIssue(build2, issue.key);
		Build build3 = backdoor.builds().addBuild(job, "SCM triggered build", Result.SUCCESS);
		build3.setDuration(TimeUnit.SECONDS.toMillis(687));
		build3.setTimestamp(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(2));
		backdoor.builds().saveBuild(build3);
		backdoor.builds().linkBuildToIssue(build3, issue.key);
	}

	@Test
	public void generateDataForReleaseReport() throws Exception {
		Project project = generateScrumProject("My Brilliant Software Project");
		Site site = backdoor.sites().addSite(SiteType.JENKINS, "CI Release Report Jenkins", URI.create("http://localhost:8080"));
		Job[] jobs = { backdoor.jobs().addJob(site.getId(), "Software Job"), backdoor.jobs().addJob(site.getId(), "Brilliant Job") };
		Version version = backdoor.createRestClient(VersionClient.class).create(new Version().name("1.0.0").project(project.key));
		backdoor.createRestClient(VersionClient.class).create(new Version().name("2.0.0").project(project.key));
		List<Priority> priorities = backdoor.priorities().getPriorities();
		List<IssueTypeControl.IssueType> issueTypes = backdoor.issueType().getIssueTypesForProject(project.key).stream()
		                                                      .filter(type -> !type.isSubtask())
		                                                      .filter(type -> !type.getName().equals("Epic"))
		                                                      .collect(toList());
		IssueClient issueClient = backdoor.createRestClient(IssueClient.class);
		Set<String> issueKeys = new HashSet<>();

		Result[][] scenarios = {
				{ Result.SUCCESS },
				{ Result.FAILURE },
				{ Result.UNSTABLE },
				{ Result.UNKNOWN },
				{ Result.SUCCESS, Result.SUCCESS },
				{ Result.SUCCESS, Result.FAILURE },
				{ Result.SUCCESS, Result.UNSTABLE },
				{ Result.SUCCESS, Result.UNKNOWN },
				{ Result.FAILURE, Result.SUCCESS },
				{ Result.FAILURE, Result.FAILURE },
				{ Result.FAILURE, Result.UNSTABLE },
				{ Result.FAILURE, Result.UNKNOWN },
				{ Result.UNSTABLE, Result.SUCCESS },
				{ Result.UNSTABLE, Result.FAILURE },
				{ Result.UNSTABLE, Result.UNSTABLE },
				{ Result.UNSTABLE, Result.UNKNOWN },
				{ Result.UNKNOWN, Result.SUCCESS },
				{ Result.UNKNOWN, Result.FAILURE },
				{ Result.UNKNOWN, Result.UNSTABLE },
				{ Result.UNKNOWN, Result.UNKNOWN }
		};

		List<String> usernames = backdoor.usersAndGroups().getAllUsers().stream()
		                                 .map(UserDTO::getUsername)
		                                 .filter(username -> !ADMIN.equals(username))
		                                 .collect(toList());

		for (Result[] scenario : scenarios) {
			Priority priority = randomEntry(priorities);
			IssueTypeControl.IssueType issueType = randomEntry(issueTypes);
			IssueCreateResponse response = issueClient.create(new IssueUpdateRequest()
					                                                  .fields(new IssueFields()
							                                                          .project(withKey(project.key))
							                                                          .fixVersions(withId(String.valueOf(version.id)))
							                                                          .summary("Ye Ole " + issueType.getName())
							                                                          .priority(withId(priority.getId()))
							                                                          .issueType(withId(issueType.getId()))
							                                                          .assignee(withName(randomEntry(usernames)))
							                                                          .reporter(withName(randomEntry(usernames)))));
			if (issueKeys.size() < 12) {
				issueClient.transition(response.key, new IssueUpdateRequest().transition(withId("31")));
			} else if (issueKeys.size() < 16) {
				issueClient.transition(response.key, new IssueUpdateRequest().transition(withId("21")));
			}
			issueKeys.add(response.key);

			for (int j = 0; j < scenario.length; j++) {
				Build build = addRandomBuildToJob(jobs[j], scenario[j]);
				backdoor.builds().linkBuildToIssue(build, response.key);
			}
		}
		jobs[1].setDeleted(true);
		stream(jobs).forEach(backdoor.jobs()::saveJob);
	}

	private <T> T randomEntry(List<T> items) {
		return items.get(random.nextInt(items.size()));
	}

	private Build addRandomBuildToJob(Job job, Result result) {
		Build build = backdoor.builds().addBuild(job, result);
		build.setDuration(TimeUnit.MINUTES.toMillis(random.nextInt(30)));
		int total = random.nextInt(250);
		int failed = result.isBetterOrEqualTo(Result.SUCCESS) ? random.nextInt(total) : 0;
		int skipped = result.isBetterOrEqualTo(Result.SUCCESS) ? random.nextInt(total - failed) : 0;
		build.setTestResults(new TestResults(failed, skipped, total));
		return backdoor.builds().saveBuild(build);
	}

}
