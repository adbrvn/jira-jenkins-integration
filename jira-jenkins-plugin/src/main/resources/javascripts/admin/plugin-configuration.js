/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define('jira-jenkins-integration/admin/plugin-configuration', [
	'jquery',
	'aui/flag',
	'jira-jenkins-integration/utils'
], function (jquery, flag, utils) {
	'use strict';

	var DIALOG_ID = '#advanced-configuration-dialog';
	var REST_URL = AJS.contextPath() + '/rest/jenkins/1.0/configuration';

	return {

		init: function () {
			jquery('#plugin-actions').append(jquery(JJI.Templates.PluginConfiguration.headerButton()).click(this.show));
		},

		show: function () {
			jquery.ajax({
				type: 'GET',
				dataType: 'json',
				url: REST_URL,
				success: function (configuration) {
					AJS.dialog2(JJI.Templates.PluginConfiguration.dialog(configuration)).show();
					jquery(DIALOG_ID + ' .aui-button-link').on('click', function () {
						AJS.dialog2(DIALOG_ID).hide();
						return false;
					});
					jquery(DIALOG_ID + ' .aui-button-primary').on('click', function () {
						utils.blanketDialog(DIALOG_ID);
						jquery(DIALOG_ID + ' div.error').html('');
						var config = utils.serializeObject(jquery(DIALOG_ID + ' form'));
						jquery.ajax({
							type: 'POST',
							dataType: 'json',
							contentType: 'application/json',
							url: REST_URL,
							data: JSON.stringify(config),
							success: function () {
								flag({
									type: 'success',
									close: 'auto',
									title: AJS.I18n.getText('plugin.config.save.success.title')
								});
								AJS.dialog2(DIALOG_ID).hide();
							}
						}).error(function (xhr) {
							utils.unblanketDialog(DIALOG_ID);
							var data = JSON.parse(xhr.responseText);
							if (data.errors !== undefined) {
								jquery.each(data.errors, function (a, error) {
									jquery('#' + error.field).parent().append(aui.form.fieldError(error));
								});
							} else {
								AJS.messages.error('#advanced-configuration-errors', {
									insert: 'prepend',
									title: AJS.I18n.getText('plugin.config.save.failed.title'),
									body: utils.getErrorMessage(data)
								});
							}
						});
						return false;
					});
				}
			}).error(function () {
				flag({
					type: 'error',
					title: AJS.I18n.getText('plugin.config.load.failed.title')
				})
			});
			return false;
		}

	};

});
