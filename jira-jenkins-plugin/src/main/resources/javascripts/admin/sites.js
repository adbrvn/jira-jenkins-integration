/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define('jira-jenkins-integration/admin/sites', [
	'jquery',
	'aui/flag',
	'jira-jenkins-integration/utils',
	'jira-jenkins-integration/admin/jobs'
], function (jquery, flag, utils, jobs) {
	'use strict';

	var DIALOG_ID = '#site-dialog';
	var DELETE_DIALOG_ID = '#site-delete-dialog';
	var DELETE_JOBS_DIALOG_ID = '#site-jobs-delete-dialog';

	var sites = {

		syncRetrievalErrorCount: 0,

		init: function () {
			window.setTimeout(this.retrieveSiteSyncStatuses, 1000);
			jquery('[jenkins-site-id]').each(function () {
				sites.checkSiteStatus(jquery(this).attr('jenkins-site-id'));
			});
			jquery('#plugin-actions').append(jquery(JJI.Templates.Admin.Sites.headerButton()).click(this.addSite));
			jquery(document).on('click', '.jenkins-job-list-toggle span', this.toggleJobList);
			jquery(document).on('click', '.edit-site', function (event) {
				sites.doDropdownActionForSite(event, sites.editSite);
			});
			jquery(document).on('click', '.delete-site', function (event) {
				sites.doDropdownActionForSite(event, sites.deleteSite);
			});
			jquery(document).on('click', '.delete-site-jobs', function (event) {
				sites.doDropdownActionForSite(event, sites.deleteJobs);
			});
			jquery(document).on('click', '.sync-site', function (event) {
				sites.doDropdownActionForSite(event, sites.syncSite);
			});
			jquery(document).on('aui-dropdown2-item-check aui-dropdown2-item-uncheck', '.autolink-site', function (event) {
				switch (event.type) {
					case 'aui-dropdown2-item-check':
						sites.doDropdownActionForSite(event, function (siteId) {
							sites.autoLinkSite(siteId, true);
						});
						break;
					case 'aui-dropdown2-item-uncheck':
						sites.doDropdownActionForSite(event, function (siteId) {
							sites.autoLinkSite(siteId, false);
						});
						break;
				}
			});
		},

		doDropdownActionForSite: function (event, callback) {
			var siteId = jquery(event.target).closest('[jenkins-site-id]').attr('jenkins-site-id');
			if (siteId !== undefined) {
				callback(siteId);
			}
		},

		getSiteContainerSelector: function (siteId) {
			return '[jenkins-site-id="' + siteId + '"]';
		},

		addSite: function () {
			sites.showSiteDialog();
			return false;
		},

		editSite: function (siteId) {
			jquery.ajax({
				type: 'GET',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + siteId,
				success: function (site) {
					if (site.displayUrl === site.rpcUrl) {
						site.displayUrl = '';
					}
					sites.showSiteDialog(site);
				}
			}).error(function () {
				flag({
					type: 'error',
					title: AJS.I18n.getText('site.load.failed.title'),
					body: AJS.I18n.getText('site.load.failed.message')
				});
			});
		},

		showSiteDialog: function (site) {
			AJS.dialog2(JJI.Templates.Admin.Sites.siteDialog(site)).show();
			jquery('#changeToken').off('click').on('click', function () {
				if (jquery(this).is(':checked')) {
					jquery('#newtoken').parent('.field-group').removeClass('hidden');
				} else {
					jquery('#newtoken').parent('.field-group').addClass('hidden');
				}
			});
			jquery(DIALOG_ID + ' .aui-button-link').off('click').on('click', function () {
				AJS.dialog2(DIALOG_ID).hide();
				return false;
			});
			jquery(DIALOG_ID + ' .aui-button-primary').off('click').on('click', function () {
				utils.blanketDialog(DIALOG_ID);
				jquery(DIALOG_ID + ' div.error').html('');
				var site = utils.serializeObject(jquery(DIALOG_ID + ' form'));
				jquery.ajax({
					type: 'POST',
					dataType: 'json',
					contentType: 'application/json',
					url: AJS.contextPath() + '/rest/jenkins/1.0/site' + (site.id !== undefined ? '/' + site.id : ''),
					data: JSON.stringify(site),
					success: function (site) {
						flag({
							type: 'success',
							close: 'auto',
							title: AJS.I18n.getText('site.save.success.title', site.name)
						});
						AJS.dialog2(DIALOG_ID).hide();
						site.jobs = site.jobs || [];
						if (jquery(sites.getSiteContainerSelector(site.id)).length == 0) {
							jquery('#jenkins-site-list').append(JJI.Templates.Admin.Sites.siteContainer({'site': site}));
							sites.checkSiteStatus(site.id);
						} else {
							jquery(sites.getSiteContainerSelector(site.id) + ' header.aui-page-header')
									.replaceWith(JJI.Templates.Admin.Sites.siteHeader({'site': site}))
						}
						sites.updateSiteSyncStatus(site.id, true);
					}
				}).error(function (xhr) {
					utils.unblanketDialog(DIALOG_ID);
					var data = JSON.parse(xhr.responseText);
					if (data.errors !== undefined) {
						jquery.each(data.errors, function (a, error) {
							jquery('#' + error.field).parent().append(aui.form.fieldError(error));
						});
					} else {
						AJS.messages.error('#site-errors', {
							insert: 'prepend',
							title: AJS.I18n.getText('site.save.failed.title'),
							body: utils.getErrorMessage(data)
						});
					}
				});
				return false;
			});
		},

		getSiteName: function (id) {
			return jquery(sites.getSiteContainerSelector(id) + ' .aui-page-header-main h4 a').text();
		},

		deleteSite: function (id) {
			var name = sites.getSiteName(id);
			AJS.dialog2(JJI.Templates.Admin.Sites.deleteSiteDialog({'name': name})).show();
			jquery(DELETE_DIALOG_ID + ' .aui-button-link').on('click', function () {
				AJS.dialog2(DELETE_DIALOG_ID).hide();
				return false;
			});
			jquery(DELETE_DIALOG_ID + ' .aui-button-primary').on('click', function () {
				utils.blanketDialog(DELETE_DIALOG_ID);
				jquery.ajax({
					type: 'DELETE',
					url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + id,
					success: function () {
						flag({
							type: 'success',
							close: 'auto',
							title: AJS.I18n.getText('site.delete.success.message', name)
						});
						AJS.dialog2(DELETE_DIALOG_ID).hide();
						jquery(sites.getSiteContainerSelector(id)).remove();
					}
				}).error(function () {
					utils.unblanketDialog(DELETE_DIALOG_ID);
					flag({
						type: 'error',
						title: AJS.I18n.getText('site.delete.failed.title', name),
						body: AJS.I18n.getText('site.delete.failed.message')
					});
					AJS.dialog2(DELETE_DIALOG_ID).hide();
				});
				return false;
			});
		},

		deleteJobs: function (id) {
			var name = sites.getSiteName(id);
			AJS.dialog2(JJI.Templates.Admin.Sites.deleteSiteJobsDialog({'name': name})).show();
			jquery(DELETE_JOBS_DIALOG_ID + ' .aui-button-link').on('click', function () {
				AJS.dialog2(DELETE_JOBS_DIALOG_ID).hide();
				return false;
			});
			jquery(DELETE_JOBS_DIALOG_ID + ' .aui-button-primary').on('click', function () {
				utils.blanketDialog(DELETE_JOBS_DIALOG_ID);
				jquery.ajax({
					type: 'DELETE',
					url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + id + '/jobs',
					success: function () {
						flag({
							type: 'success',
							close: 'auto',
							title: AJS.I18n.getText('site.jobs.delete.success.message', name)
						});
						AJS.dialog2(DELETE_JOBS_DIALOG_ID).hide();
						jquery(sites.getSiteContainerSelector(id) + ' .job-deleted').remove();
					}
				}).error(function () {
					utils.unblanketDialog(DELETE_JOBS_DIALOG_ID);
					flag({
						type: 'error',
						title: AJS.I18n.getText('site.jobs.delete.failed.title', name),
						body: AJS.I18n.getText('site.jobs.delete.failed.message')
					});
					AJS.dialog2(DELETE_JOBS_DIALOG_ID).hide();
				});
				return false;
			});
		},

		syncSite: function (id) {
			jquery.ajax({
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + id + '/sync',
				success: function () {
					var site = {'id': id, 'name': sites.getSiteName(id)};
					flag({
						type: 'success',
						close: 'auto',
						title: AJS.I18n.getText('site.sync.status.trigger.title', site.name)
					});
					sites.retrieveSiteSyncStatus(site)
				}
			}).error(function () {
				flag({
					type: 'error',
					title: AJS.I18n.getText('site.sync.status.update.failed.title', sites.getSiteName(id))
				});
			});
		},

		autoLinkSite: function (siteId, enabled) {
			jquery.ajax({
				type: 'POST',
				dataType: 'json',
				contentType: 'application/json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + siteId + '/autolink',
				data: '{ "payload" : "' + enabled + '"}'
			}).error(function () {
				flag({
					type: 'error',
					title: AJS.I18n.getText('site.autolink.failed.title', sites.getSiteName(id)),
					body: AJS.I18n.getText('site.autolink.failed.message')
				});
				jquery(sites.getSiteContainerSelector(siteId) + ' .autolink-site')
						.removeClass('checked').attr('aria-checked', 'false');
			});
		},

		showBackgroundSyncDoneFlag: function () {
			flag({
				type: 'info',
				close: 'auto',
				title: AJS.I18n.getText('site.background.sync.done.title'),
				body: AJS.I18n.getText('site.background.sync.done.message', utils.getRefreshUrl())
			});
		},

		updateSiteSyncStatus: function (siteId, show) {
			var header = jquery(this.getSiteContainerSelector(siteId) + ' .aui-page-header-main h4');
			if (show) {
				header.spin('small', {'left': header.children('a').outerWidth() + 10, 'top': 2, 'zIndex': 0});
			} else {
				if (header.children('.spinner').length == 1) {
					sites.showBackgroundSyncDoneFlag();
				}
				header.spin(false);
			}
		},

		checkSiteStatus: function (siteId) {
			jquery.ajax({
				type: 'GET',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + siteId + '/status',
				success: function (data) {
					var status = data.status || '';
					var pluginInstalled = data.pluginInstalled || false;
					var site = data.site || {};
					var context = sites.getSiteContainerSelector(site.id) + ' .message-bar';
					if (status == 'OFFLINE') {
						AJS.messages.warning(context, {
							body: AJS.I18n.getText('site.offline.message', site.name)
						});
					} else if (status == 'NOT_ACCESSIBLE') {
						AJS.messages.error(context, {
							title: AJS.I18n.getText('site.auth.error.title'),
							body: AJS.I18n.getText('site.auth.error.message', site.name)
						});
					} else if (!pluginInstalled) {
						AJS.messages.hint(context, {
							title: AJS.I18n.getText('site.plugin.missing.title', site.name),
							body: AJS.I18n.getText('site.plugin.missing.message')
						});
					}
				}
			});
		},

		retrieveSiteSyncStatuses: function () {
			jquery.ajax({
				type: 'GET',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/site',
				success: function (data) {
					data.syncRetrievalErrorCount = 0;
					if (data != null) {
						jquery.each(data, function (a, site) {
							sites.retrieveSiteSyncStatus(site);
						});
						window.setTimeout(sites.retrieveSiteSyncStatuses, 5000);
					}
				}
			}).error(function () {
				sites.syncRetrievalErrorCount++;
				if (sites.syncRetrievalErrorCount <= 10) {
					window.setTimeout(sites.retrieveSiteSyncStatuses, 10000);
				} else {
					flag({
						type: 'error',
						title: AJS.I18n.getText('site.background.sync.failed.title'),
						body: AJS.I18n.getText('site.background.sync.failed.message', utils.getRefreshUrl())
					});
				}
			});
		},

		retrieveSiteSyncStatus: function (site) {
			jquery.ajax({
				type: 'GET',
				dataType: 'json',
				url: AJS.contextPath() + '/rest/jenkins/1.0/site/' + site.id + '/sync/status?includeJobs=true',
				success: function (status) {
					var show = !(status.progress === undefined || status.progress.finished);
					if (status.jobs !== undefined) {
						jquery.each(status.jobs, function (a, job) {
							if (jobs.handleJobSyncStatus(job)) {
								show = true;
							}
						});
					}
					sites.updateSiteSyncStatus(status.site.id, show);
				}
			}).error(function () {
				flag({
					type: 'warning',
					title: AJS.I18n.getText('site.sync.status.update.failed.title', site.name)
				});
			});
		},

		toggleJobList: function () {
			var toggle = jquery(this);
			var list = toggle.parent().next('.joblist');
			list.children('tbody').hide();
			if (toggle.hasClass('jenkins-collapsed')) {
				toggle.removeClass('jenkins-collapsed');
				list.children('tbody.site-jobs-expanded').show();
			} else {
				toggle.addClass('jenkins-collapsed');
				list.children('tbody.site-jobs-collapsed').show();
			}
		}

	};

	return sites;

});
