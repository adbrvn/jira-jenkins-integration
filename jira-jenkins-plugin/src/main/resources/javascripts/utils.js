/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define('jira-jenkins-integration/utils', ['jquery'], function (jquery) {
	'use strict';
	
	return {

		serializeObject: function (form) {
			var o = {};
			var a = form.serializeArray();
			jquery.each(a, function () {
				if (o[this.name]) {
					if (!o[this.name].push) {
						o[this.name] = [o[this.name]];
					}
					o[this.name].push(this.value || '');
				} else {
					o[this.name] = this.value || '';
				}
			});
			return o;
		},

		blanketDialog: function (dialogId) {
			jquery(dialogId + ' .aui-dialog2-content').spin('large');
			jquery(dialogId + ' .aui-button-primary').prop('disabled', true);
		},

		unblanketDialog: function (dialogId) {
			jquery(dialogId + ' .aui-dialog2-content').spin(false);
			jquery(dialogId + ' .aui-button-primary').prop('disabled', false);
		},

		getErrorMessage: function (data) {
			if (data.message !== undefined) {
				var elements = data.message.split('\n');
				if (elements.length > 0) {
					return elements[0];
				} else {
					return data.message;
				}
			} else {
				return AJS.I18n.getText('unknown.error.message');
			}
		},
		
		addTimestampTooltip: function(element) {
			var timestamp = element.find("time.livestamp");
			timestamp.livestamp();
			timestamp.each(function(){
				var time_el = jquery(this);
				var time_val = time_el.attr("datetime");
				var fixed_time = isNaN(time_val) ? time_val : +time_val;
				var fixedMoment = moment(fixed_time).zone(fixed_time);
				if (!fixedMoment.isValid()) {
					fixedMoment = moment(fixed_time);
				}
				time_el.attr("title", fixedMoment.format('LLL'));
				time_el.tipsy();
			})
		},

		getRefreshUrl: function() {
			return location.href.substr(0, location.href.indexOf('#'));
		}

	};
});
