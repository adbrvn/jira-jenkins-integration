/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.releasereport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.sync.BuildIssueLinksUpdatedEvent;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettings;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.cluster.ClusterSafe;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.devstatus.api.VersionWarningCategoryRequest;
import com.atlassian.jira.plugin.devstatus.api.releasereport.column.AbstractPluggableColumn;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;

/**
 * {@link AbstractPluggableColumn} implementation that provides Jenkins build information in the release report of a version.
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
@Scanned
public class CiBuildReleaseReportColumn extends AbstractPluggableColumn {

	private static final Logger LOGGER = LoggerFactory.getLogger(CiBuildReleaseReportColumn.class);
	private static final String HTML_CACHE_NAME = CiBuildReleaseReportColumn.class.getName();
	private static final CacheSettings HTML_CACHE_SETTINGS = new CacheSettingsBuilder()
			.expireAfterAccess(30L, TimeUnit.MINUTES)
			.flushable()
			.build();
	private final EventPublisher eventPublisher;
	@ClusterSafe
	private final Cache<String, String> issueHtmlCache;
	private final JenkinsPluginUtil pluginUtil;
	private final ReleaseReportHelper reportHelper;
	private final I18nHelper i18nHelper;

	@Inject
	public CiBuildReleaseReportColumn(JenkinsPluginUtil pluginUtil, ReleaseReportHelper reportHelper,
	                                  @ComponentImport @Nonnull EventPublisher eventPublisher,
	                                  @ComponentImport @Nonnull I18nHelper i18nHelper,
	                                  @ComponentImport @Nonnull CacheManager cacheManager,
	                                  @ComponentImport @Nonnull SoyTemplateRenderer soyTemplateRenderer) {
		super("cibuilds", "cibuilds");
		this.pluginUtil = pluginUtil;
		this.reportHelper = reportHelper;
		this.eventPublisher = eventPublisher;
		this.i18nHelper = i18nHelper;
		issueHtmlCache = cacheManager.getCache(HTML_CACHE_NAME, getHtmlCacheLoader(soyTemplateRenderer), HTML_CACHE_SETTINGS);
	}

	private CacheLoader<String, String> getHtmlCacheLoader(SoyTemplateRenderer soyTemplateRenderer) {
		return new CacheLoader<String, String>() {
			@Nonnull
			@Override
			public String load(@Nonnull String issueKey) {
				LOGGER.debug("Generating Jenkins release column widget for {}", issueKey);
				try {
					return soyTemplateRenderer.render(pluginUtil.getCompleteModuleKey("release-report-resources"),
					                                  "JJI.Templates.ReleaseReport.jenkinsBuilds", getTemplateData(issueKey));
				} catch (SoyException e) {
					LOGGER.error("Failed to generate Jenkins widget for {}", issueKey, e);
					return "";
				}
			}
		};
	}

	private Map<String, Object> getTemplateData(String issueKey) {
		Map<String, Object> context = new HashMap<>();
		context.put("issueKey", issueKey);
		List<Job> relatedJobs = reportHelper.getBuildInformation(issueKey);
		context.put("count", relatedJobs.size());
		AtomicInteger successfulBuildCount = new AtomicInteger(0),
				unstableBuildCount = new AtomicInteger(0),
				failedBuildCount = new AtomicInteger(0),
				unknownBuildCount = new AtomicInteger(0);
		relatedJobs.stream().map(Job::getBuilds)
		           .forEach(builds -> builds.stream().sorted(comparing(Build::getNumber, reverseOrder()))
		                                    .findFirst().ifPresent(build -> {
					           switch (build.getResult()) {
						           case SUCCESS:
							           successfulBuildCount.incrementAndGet();
							           break;
						           case UNSTABLE:
							           unstableBuildCount.incrementAndGet();
							           break;
						           case FAILURE:
							           failedBuildCount.incrementAndGet();
							           break;
						           default:
							           unknownBuildCount.incrementAndGet();
							           break;
					           }
				           }));
		context.put("successfulBuildCount", successfulBuildCount.intValue());
		context.put("unstableBuildCount", unstableBuildCount.intValue());
		context.put("failedBuildCount", failedBuildCount.intValue());
		context.put("unknownBuildCount", unknownBuildCount.intValue());
		return context;
	}

	@Nonnull
	@Override
	protected Function<Issue, String> getHtmlGenerator(@Nonnull VersionWarningCategoryRequest request) {
		return new Function<Issue, String>() {
			@Nullable
			@Override
			public String apply(@Nullable Issue issue) {
				return issue != null ? issueHtmlCache.get(issue.getKey()) : null;
			}
		};
	}

	@Nonnull
	@Override
	public String getHeading() {
		return i18nHelper.getText("release.report.column.heading.jenkins");
	}

	@EventListener
	public void onBuildIssueLinksUpdatedEvent(BuildIssueLinksUpdatedEvent event) {
		event.getIssueKeys().stream().forEach(issueHtmlCache::remove);
	}

	@PostConstruct
	public void init() {
		eventPublisher.register(this);
	}

	@PreDestroy
	public void destroy() {
		eventPublisher.unregister(this);
	}

}
