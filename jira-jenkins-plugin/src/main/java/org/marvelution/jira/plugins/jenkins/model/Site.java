/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.marvelution.jira.plugins.jenkins.adapters.URIAdapter;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Site {

	private int id;
	private boolean useCrumbs;
	private boolean supportsBackLink;
	private boolean autoLink;
	private boolean deleted;
	private String name;
	@XmlJavaTypeAdapter(value = URIAdapter.class, type = URI.class)
	private URI rpcUrl;
	@XmlJavaTypeAdapter(value = URIAdapter.class, type = URI.class)
	private URI displayUrl;
	private List<Job> jobs;
	private SiteType type;
	private String user;
	@XmlTransient
	private String token;
	private boolean changeToken;
	private String newtoken;

	/**
	 * JAXB Default Constructor
	 */
	public Site() {
		id = 0;
	}

	public Site(int id) {
		this.id = id;
	}

	/**
	 * Getter for the site Id
	 *
	 * @return the site id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for the crumb usage state
	 *
	 * @return the crumb usage state
	 */
	public boolean isUseCrumbs() {
		return useCrumbs;
	}

	/**
	 * Setter for the crumb usage state
	 *
	 * @param useCrumbs the crumb usage state
	 */
	public void setUseCrumbs(boolean useCrumbs) {
		this.useCrumbs = useCrumbs;
	}

	/**
	 * Getter for the autoLink support state
	 *
	 * @return the autoLink support state
	 */
	public boolean isSupportsBackLink() {
		return supportsBackLink;
	}

	/**
	 * Setter for the autoLink support state
	 *
	 * @param supportsBackLink the autoLink support state
	 */
	public void setSupportsBackLink(boolean supportsBackLink) {
		this.supportsBackLink = supportsBackLink;
	}

	/**
	 * Getter for the autoLink state
	 *
	 * @return the autoLink state
	 */
	public boolean isAutoLink() {
		return autoLink;
	}

	/**
	 * Setter for the autoLink state
	 *
	 * @param autoLink the new autoLink state
	 */
	public void setAutoLink(boolean autoLink) {
		this.autoLink = autoLink;
	}

	/**
	 * Getter for the delete state
	 *
	 * @return the deleted state
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Setter for the deleted state
	 *
	 * @param deleted the new deleted state
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Getter for the Site name
	 *
	 * @return the name of the site
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for the Site name
	 *
	 * @param name the name of the site
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the RPC URL
	 *
	 * @return the RPC URL
	 * @since 1.6.0
	 */
	public URI getRpcUrl() {
		return rpcUrl;
	}

	/**
	 * Setter for the RPC URL
	 *
	 * @param rpcUrl the RPC URL
	 * @since 1.6.0
	 */
	public void setRpcUrl(URI rpcUrl) {
		this.rpcUrl = rpcUrl;
	}

	/**
	 * Getter for the Display URL
	 *
	 * @return the display URL
	 */
	public URI getDisplayUrl() {
		return displayUrl != null ? displayUrl : rpcUrl;
	}

	/**
	 * Setter for the display URL
	 *
	 * @param displayUrl the display URL
	 */
	public void setDisplayUrl(URI displayUrl) {
		this.displayUrl = displayUrl != null && displayUrl.equals(rpcUrl) ? null : displayUrl;
	}

	/**
	 * Return whether this site has a display url configured
	 */
	public boolean hasDisplayUrl() {
		return displayUrl != null;
	}

	/**
	 * Getter for the {@link Job} {@link List}
	 *
	 * @return the {@link List} of {@link Job} objects, never {@code null} called
	 */
	public List<Job> getJobs() {
		if (jobs == null) {
			jobs = new ArrayList<>();
		}
		return jobs;
	}

	/**
	 * Setter for the {@link Job} {@link List}
	 *
	 * @param jobs the {@link Job} {@link List}
	 */
	public void setJobs(List<Job> jobs) {
		this.jobs = jobs;
	}

	/**
	 * Getter for the {@link SiteType}
	 *
	 * @return the {@link SiteType}
	 */
	public SiteType getType() {
		return type;
	}

	/**
	 * Setter for the {@link SiteType}
	 *
	 * @param type the {@link SiteType}
	 */
	public void setType(SiteType type) {
		this.type = type;
	}

	/**
	 * Returns whether this site requires authentication
	 *
	 * @return {@literal true} in case both {@link #user} and {@link #token} are not {@literal blank}
	 */
	public boolean isSecured() {
		return isNotBlank(user) && isNotBlank(token);
	}

	/**
	 * Getter for the authentication user
	 *
	 * @return the user to authenticate with, may be {@literal null} for anonymous access
	 * @since 1.6.0
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Setter for the authentication user
	 *
	 * @param user the user to authenticate with, may be {@literal null} for anonymous access
	 * @since 1.6.0
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Getter for the authentication token
	 *
	 * @return the token to authenticate with, may be {@literal null} for anonymous access
	 * @since 1.6.0
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Setter for the authentication token
	 *
	 * @param token the token to authenticate with, may be {@literal null} for anonymous access
	 * @since 1.6.0
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * Getter if the authentication token is being changed
	 *
	 * @return {@literal true} in case the token is being changed, the new token is stored obtainable using {@link #getNewtoken()}
	 * @since 1.6.0
	 */
	public boolean isChangeToken() {
		return changeToken;
	}

	/**
	 * Setter if the authentication token is being changed
	 *
	 * @param changeToken {@literal true} in case the token is being changed
	 * @since 1.6.0
	 */
	public void setChangeToken(boolean changeToken) {
		this.changeToken = changeToken;
	}

	/**
	 * Getter for the authentication token
	 *
	 * @return the token to authenticate with, may be {@literal null} for anonymous access
	 * @since 1.6.0
	 */
	public String getNewtoken() {
		return newtoken;
	}

	/**
	 * Setter for the authentication token
	 *
	 * @param newtoken the token to authenticate with, may be {@literal null} for anonymous access
	 * @since 1.6.0
	 */
	public void setNewtoken(String newtoken) {
		this.newtoken = newtoken;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Site site = (Site) o;
		return id == site.id;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
				.append("id", id)
				.append("name", name)
				.append("rpcUrl", rpcUrl)
				.append("useCrumbs", useCrumbs)
				.append("supportsBackLink", supportsBackLink)
				.append("autoLink", autoLink)
				.toString();
	}

}
