/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.releasereport;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.JobService;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

/**
 * Helper for the Release Report features
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
@Named
public class ReleaseReportHelper {

	private final JobService jobService;
	private final BuildService buildService;

	@Inject
	public ReleaseReportHelper(JobService jobService, BuildService buildService) {
		this.jobService = jobService;
		this.buildService = buildService;
	}

	public List<Job> getBuildInformation(String issueKey) {
		return buildService.getByIssueKey(issueKey).stream().collect(groupingBy(Build::getJobId, toList()))
		                   .entrySet().stream()
		                   .map(match -> {
			                   Job job = jobService.get(match.getKey());
			                   Communicator communicator = jobService.getCommunicatorForJob(job);
			                   job.setAbsoluteUri(communicator.getJobUrl(job));
			                   match.getValue().stream()
			                        .map(build -> {
				                        build.setAbsoluteUri(communicator.getBuildUrl(job, build));
				                        return build;
			                        })
			                        .collect(toCollection(job::getBuilds));
			                   return job;
		                   })
		                   .collect(toList());
	}

}
