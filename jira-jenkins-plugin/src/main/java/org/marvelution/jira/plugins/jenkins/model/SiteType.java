/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.util.Locale;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * {@link Site} type enum
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlEnum
@XmlType
public enum SiteType {

	@XmlEnumValue("hudson")
	HUDSON,
	@XmlEnumValue("jenkins")
	JENKINS;

	public String i18nKey() {
		return name().toLowerCase(Locale.ENGLISH);
	}

}
