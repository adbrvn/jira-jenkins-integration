/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest.filter;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.ext.Provider;

import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import static java.util.Collections.singletonList;

/**
 * {@link Provider} for the {@link ResourceFilterFactory}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Provider
@Scanned
public class AdminRequiredResourceFilterFactory implements ResourceFilterFactory {

	private final JiraAuthenticationContext authenticationContext;
	private final GlobalPermissionManager globalPermissionManager;

	@Inject
	public AdminRequiredResourceFilterFactory(@ComponentImport JiraAuthenticationContext authenticationContext,
	                                          @ComponentImport GlobalPermissionManager globalPermissionManager) {
		this.authenticationContext = authenticationContext;
		this.globalPermissionManager = globalPermissionManager;
	}

	@Override
	public List<ResourceFilter> create(AbstractMethod abstractMethod) {
		return singletonList(new AdminRequiredResourceFilter(abstractMethod, authenticationContext, globalPermissionManager));
	}

}
