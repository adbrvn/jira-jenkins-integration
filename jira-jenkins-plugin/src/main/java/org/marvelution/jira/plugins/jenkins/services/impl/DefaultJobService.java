/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.dao.IssueDAO;
import org.marvelution.jira.plugins.jenkins.dao.JobDAO;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.JobSyncProgress;
import org.marvelution.jira.plugins.jenkins.model.JobSyncStatus;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.services.JobService;
import org.marvelution.jira.plugins.jenkins.sync.Synchronizer;
import org.marvelution.jira.plugins.jenkins.sync.impl.JobSynchronizationOperation;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

/**
 * Default {@link JobService} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(JobService.class)
public class DefaultJobService implements JobService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultJobService.class);
	private final JobDAO jobDAO;
	private final IssueDAO issueDAO;
	private final BuildService buildService;
	private final Synchronizer synchronizer;
	private final CommunicatorFactory communicatorFactory;

	@Inject
	public DefaultJobService(JobDAO jobDAO, IssueDAO issueDAO, BuildService buildService, Synchronizer synchronizer,
	                         CommunicatorFactory communicatorFactory) {
		this.jobDAO = jobDAO;
		this.issueDAO = issueDAO;
		this.buildService = buildService;
		this.synchronizer = synchronizer;
		this.communicatorFactory = communicatorFactory;
	}

	@Override
	public void sync(int jobId) {
		Job job = jobDAO.get(jobId);
		if (job != null) {
			doSync(job);
		}
	}

	@Override
	public void syncAllFromSite(Site site) {
		getAllBySite(site).forEach(this::doSync);
	}

	@Override
	public JobSyncStatus getSyncStatus(Job job) {
		JobSyncStatus status = new JobSyncStatus();
		status.setJob(requireNonNull(job));
		status.setProgress((JobSyncProgress) synchronizer.getProgress(JobSynchronizationOperation.operationId(job)));
		return status;
	}

	@Override
	public List<Job> getAllBySite(Site site) {
		return jobDAO.getAllBySiteId(site.getId(), false);
	}

	@Override
	public List<Job> getAllBySite(Site site, boolean includeDeleted) {
		return jobDAO.getAllBySiteId(site.getId(), includeDeleted);
	}

	@Override
	public List<Job> getAllFromSameSite(Job job) {
		return jobDAO.getAllBySiteId(job.getSiteId(), false);
	}

	@Override
	public List<Job> getAllFromSameSite(Job job, boolean includeDeleted) {
		return jobDAO.getAllBySiteId(job.getSiteId(), true);
	}

	@Override
	public List<Job> getAll() {
		return jobDAO.getAll(false);
	}

	@Override
	public List<Job> getAll(boolean includeDeleted) {
		return jobDAO.getAll(includeDeleted);
	}

	@Override
	public Job get(int jobId) {
		return jobDAO.get(jobId);
	}

	@Override
	public List<Job> get(String name) {
		return jobDAO.get(name);
	}

	@Override
	public void enable(int jobId, boolean enabled) {
		Job job = jobDAO.get(jobId);
		if (job != null) {
			if (!enabled) {
				synchronizer.stopSynchronization(JobSynchronizationOperation.operationId(job));
			}
			job.setLinked(enabled);
			jobDAO.save(job);
		}
	}

	@Override
	public Job save(Job job) {
		return jobDAO.save(job);
	}

	@Override
	public void delete(Job job) {
		buildService.deleteAllInJob(job);
		jobDAO.delete(job.getId());
	}

	@Override
	public void deleteAllFromSite(Site site) {
		getAllBySite(site).forEach(buildService::deleteAllInJob);
		jobDAO.deleteAllBySiteId(site.getId());
	}

	@Override
	public void markAsDeleted(Job job) {
		buildService.markAllInJobAsDeleted(job);
		jobDAO.markAsDeleted(job);
	}

	@Override
	public void markAllFromSiteAsDeleted(Site site) {
		getAllBySite(site).forEach(this::markAsDeleted);
	}

	@Override
	public Set<String> getRelatedIssuesKeys(Job job) {
		return issueDAO.getIssueKeysByJob(job);
	}

	@Override
	public Communicator getCommunicatorForJob(Job job) {
		return communicatorFactory.get(job.getSiteId());
	}

	/**
	 * Trigger the synchronization of the given {@link Job}
	 *
	 * @param job the {@link Job} to synchronize
	 */
	private void doSync(Job job) {
		if (job.isLinked() && !job.isDeleted()) {
			synchronizer.schedule(new JobSynchronizationOperation(job));
		} else {
			LOGGER.debug("Skipping synchronization of {} as its not syncable at this time", job.getName());
		}
	}

}
