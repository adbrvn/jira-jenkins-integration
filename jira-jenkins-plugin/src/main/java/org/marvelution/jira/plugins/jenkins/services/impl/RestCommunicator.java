/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javax.annotation.Nullable;
import javax.ws.rs.core.UriBuilder;

import org.marvelution.jira.plugins.jenkins.model.Artifact;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.ChangeSet;
import org.marvelution.jira.plugins.jenkins.model.Culprit;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Result;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.Status;
import org.marvelution.jira.plugins.jenkins.model.TestResults;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseConnectTimeoutException;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseReadTimeoutException;
import com.google.common.base.Joiner;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * JSON Rest Implementation of the {@link Communicator}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class RestCommunicator implements Communicator {

	private static final int CALL_TIMEOUT = 50000;
	private static final Logger LOGGER = LoggerFactory.getLogger(RestCommunicator.class);
	private static final String[] COMMIT_ID_FIELDS = new String[] {
			"id", // GIT
			"node", // Mercurial
			"revision", // Subversion
			"changeItem", // Perforce
			"commitId" // Default
	};
	private static final String[] COMMIT_MESSAGE_FIELDS = new String[] {
			"comment",
			"msg" // Default
	};
	private final JenkinsPluginConfiguration pluginConfiguration;
	private final Site site;
	private final NonMarshallingRequestFactory<?> requestFactory;
	private final Function<String, UriBuilder> siteUriBuilder = new Function<String, UriBuilder>() {
		@Override
		public UriBuilder apply(String path) {
			return UriBuilder.fromUri(site.getRpcUrl()).path(path);
		}
	};

	public RestCommunicator(JenkinsPluginConfiguration pluginConfiguration, Site site, NonMarshallingRequestFactory<?> requestFactory) {
		this.pluginConfiguration = pluginConfiguration;
		this.site = site;
		this.requestFactory = requestFactory;
	}

	@Override
	public Status getRemoteStatus() {
		RestResponse response = createAndExecuteRequest(Request.MethodType.GET, siteUriBuilder.apply("api/json").build());
		LOGGER.debug("Remote status check for {} returned {} [{}]", site.getName(), response.getStatusCode(), response.getStatusMessage());
		if (response.isSuccessful()) {
			return Status.ONLINE;
		} else if (response.hasServerResponse()) {
			return Status.NOT_ACCESSIBLE;
		} else {
			return Status.OFFLINE;
		}
	}

	@Override
	public boolean isJenkinsPluginInstalled() {
		RestResponse response = createAndExecuteRequest(Request.MethodType.GET,
		                                                siteUriBuilder.apply("plugin/jenkins-jira-plugin/ping.html").build());
		LOGGER.debug("Backlink support check on {}; code: {}", site.getName(), response.getStatusCode());
		return response.isSuccessful();
	}

	@Override
	public boolean isCrumbSecurityEnabled() {
		RestResponse response = createAndExecuteRequest(Request.MethodType.GET, siteUriBuilder.apply("api/json").build());
		JSONObject json = response.getJson();
		return json == null || json.optBoolean("useCrumbs", true);
	}

	@Override
	public boolean registerWithSite() {
		if (site.isSupportsBackLink()) {
			LOGGER.debug("About to register this JIRA instance with {}", site);
			Request<?, ?> request = createRequest(Request.MethodType.POST,
			                                      siteUriBuilder.apply("plugin/jenkins-jira-plugin/register").build());
			request.addRequestParameters("uri", pluginConfiguration.getJIRABaseRpcUrl().toASCIIString());
			request.addRequestParameters("name", pluginConfiguration.getJIRAInstanceName());
			RestResponse response = executeRequest(request);
			if (!response.isSuccessful()) {
				LOGGER.warn("Unable to register with {} at {}, {} [{}]",
				            site.getName(), site.getRpcUrl(), response.getStatusMessage(), response.getStatusCode());
			}
			return response.isSuccessful();
		} else {
			LOGGER.debug("Site {} doesn't support the build notifier", site.getName());
		}
		return false;
	}

	@Override
	public List<Job> getJobs() {
		List<Job> jobs = new ArrayList<>();
		try {
			RestResponse response = createAndExecuteRequest(Request.MethodType.GET, siteUriBuilder.apply("api/json").build());
			if (response.getJson() != null && response.getJson().has("jobs")) {
				parseJobs(response.getJson(), jobs);
			} else {
				handleNoJsonResponse(response);
			}
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Job list from {}: {}", site.getName(), e.getMessage());
		}
		return jobs;
	}

	private void parseJobs(JSONObject json, List<Job> parsedJobs) throws JSONException {
		if (json.has("jobs")) {
			JSONArray jobs = json.getJSONArray("jobs");
			LOGGER.info("Found {} jobs", json.length());
			for (int index = 0; index < jobs.length(); index++) {
				final JSONObject jobJson = jobs.getJSONObject(index);
				Job job = new Job(site.getId(), jobJson.getString("name"));
				job.setLinked(site.isAutoLink());
				String url = StringUtils.stripEnd(jobJson.getString("url"), "/");
				String urlName = url.substring(url.indexOf("/job/") + 5);
				if (!job.getName().equals(urlName)) {
					job.setUrlName(urlName);
				}
				parsedJobs.add(job);
			}
		}
	}

	@Override
	public Job getDetailedJob(Job job) {
		try {
			RestResponse response = createAndExecuteRequest(Request.MethodType.GET,
			                                                siteUriBuilder.apply("job/{urlName}/api/json")
			                                                              .buildFromEncoded(job.getUrlName()));
			JSONObject json = response.getJson();
			if (response.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
				job.setDeleted(true);
			} else if (json != null) {
				job.setDisplayName(optJsonString(json, "displayName"));
				job.setDescription(optJsonString(json, "description"));
				job.setBuildable(json.optBoolean("buildable", false));
				final JSONObject firstBuild = json.optJSONObject("firstBuild");
				if (firstBuild != null && firstBuild.has("number")) {
					job.setOldestBuild(firstBuild.optInt("number", -1));
				}
				final JSONArray builds = json.optJSONArray("builds");
				if (builds != null && builds.length() > 0) {
					for (int index = 0; index < builds.length(); index++) {
						final JSONObject jsonBuild = builds.getJSONObject(index);
						final Build build = new Build(job.getId(), jsonBuild.getInt("number"));
						job.getBuilds().add(build);
					}
				}
				parseJobs(json, job.getJobs());
			} else {
				handleNoJsonResponse(response);
			}
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Detailed Job {} from {}: {}", job.getName(), site.getName(), e.getMessage());
		}
		return job;
	}

	@Override
	public URI getJobUrl(Job job) {
		return UriBuilder.fromUri(site.getDisplayUrl()).path("job/{urlName}/").buildFromEncoded(job.getUrlName());
	}

	@Override
	public Build getDetailedBuild(Job job, Build build) {
		return getDetailedBuild(job, build.getNumber());
	}

	@Override
	public Build getDetailedBuild(Job job, int number) {
		Build build = new Build(job.getId(), number);
		try {
			RestResponse response = createAndExecuteRequest(Request.MethodType.GET,
			                                                siteUriBuilder.apply("job/{urlName}/{buildNumber}/api/json")
			                                                              .buildFromEncoded(job.getUrlName(), number));
			if (response.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
				build.setDeleted(true);
			} else if (response.getJson() != null) {
				final JSONObject jsonBuild = response.getJson();
				String displayName = optJsonString(jsonBuild, "fullDisplayName");
				if (displayName != null && !(displayName.startsWith(job.getDisplayName()) && displayName.endsWith("#" + number))) {
					// Only set the full display name if it doesn't start with the job display name and end with #[number]
					// which is the default format
					build.setDisplayName(displayName);
				}
				build.setDescription(optJsonString(jsonBuild, "description"));
				build.setBuiltOn(optJsonString(jsonBuild, "builtOn"));
				build.setResult(Result.fromString(optJsonString(jsonBuild, "result")));
				build.setTimestamp(jsonBuild.getLong("timestamp"));
				build.setDuration(jsonBuild.getLong("duration"));
				final JSONArray actions = jsonBuild.optJSONArray("actions");
				if (actions != null) {
					for (int index = 0; index < actions.length(); index++) {
						final JSONObject object = actions.optJSONObject(index);
						if (object == null) {
							continue;
						}
						// Look for the build cause
						if (object.has("causes")) {
							JSONArray causes = object.optJSONArray("causes");
							for (int ii = 0; ii < causes.length(); ii++) {
								final JSONObject cause = causes.getJSONObject(ii);
								if (cause.has("shortDescription")) {
									// There should only be one build cause
									build.setCause(cause.getString("shortDescription"));
									break;
								}
							}
						}
						// Look for the test results
						else if (object.has("urlName") && "testReport".equals(object.getString("urlName"))) {
							// Located test results action
							build.setTestResults(new TestResults(object.getInt("failCount"), object.getInt("skipCount"),
							                                     object.getInt("totalCount")));
						}
					}
				}
				try {
					final JSONArray artifacts = jsonBuild.optJSONArray("artifacts");
					if (artifacts != null && artifacts.length() > 0) {
						for (int index = 0; index < artifacts.length(); index++) {
							final JSONObject artifact = artifacts.getJSONObject(index);
							build.getArtifacts().add(new Artifact(artifact.getString("fileName"),
							                                      artifact.getString("displayPath"), artifact.getString("relativePath")));
						}
					}
				} catch (JSONException e) {
					LOGGER.error("Unable to detect build artifacts for build {} of {} from {}: {}",
					             number, job.getName(), site.getName(), e.getMessage());
					LOGGER.debug("JSON Exception", e);
				}
				try {
					final JSONArray culprits = jsonBuild.optJSONArray("culprits");
					if (culprits != null && culprits.length() > 0) {
						for (int index = 0; index < culprits.length(); index++) {
							final JSONObject culprit = culprits.getJSONObject(index);
							String id = optJsonString(culprit, "id");
							if (id == null && culprit.has("absoluteUrl")) {
								String absoluteUrl = culprit.getString("absoluteUrl");
								id = absoluteUrl.substring(absoluteUrl.lastIndexOf("/"));
							} else if (id == null) {
								id = culprit.getString("fullName");
							}
							build.getCulprits().add(new Culprit(id, culprit.getString("fullName")));
						}
					}
				} catch (JSONException e) {
					LOGGER.error("Unable to detect build culprits for build {} of {} from {}: {}",
					             number, job.getName(), site.getName(), e.getMessage());
					LOGGER.debug("JSON Exception", e);
				}
				try {
					if (jsonBuild.has("changeSet")) {
						JSONArray items = null;
						Object changeSet = jsonBuild.opt("changeSet");
						if (changeSet != null) {
							if (changeSet instanceof JSONArray) {
								items = (JSONArray) changeSet;
							} else if (changeSet instanceof JSONObject) {
								items = ((JSONObject) changeSet).optJSONArray("items");
							}
						}
						if (items != null && items.length() > 0) {
							for (int index = 0; index < items.length(); index++) {
								final JSONObject item = items.getJSONObject(index);
								String commitId = optJsonString(item, COMMIT_ID_FIELDS);
								if (commitId == null) {
									commitId = String.valueOf(index);
								}
								build.getChangeSet().add(new ChangeSet(commitId, optJsonString(item, COMMIT_MESSAGE_FIELDS)));
							}
						}
					}
				} catch (JSONException e) {
					LOGGER.error("Unable to detect build changelog for build {} of {} from {}: {}",
					             number, job.getName(), site.getName(), e.getMessage());
					LOGGER.debug("JSON Exception", e);
				}
			} else {
				handleNoJsonResponse(response);
			}
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Detailed Build {} of {} from {}: {}",
			             number, job.getName(), site.getName(), e.getMessage());
			LOGGER.debug("JSON Exception", e);
		}
		return build;
	}

	@Override
	public URI getBuildUrl(Job job, Build build) {
		return UriBuilder.fromUri(getJobUrl(job)).path("{buildNumber}/").build(build.getNumber());
	}

	/**
	 * Helper method to get the optional {@link String} element of the given {@link JSONObject} given by the key given
	 *
	 * @param json the {@link JSONObject} to get the {@link String} value from
	 * @param key  the key to get he corresponding value of
	 * @return the {@link String} value, maybe {@code null}
	 */
	@Nullable
	private String optJsonString(JSONObject json, String... key) {
		for (String k : key) {
			if (json.has(k) && !json.isNull(k)) {
				return json.optString(k);
			}
		}
		return null;
	}

	private void handleNoJsonResponse(RestResponse response) {
		if (response.getErrors().isEmpty()) {
			LOGGER.warn("No JSON response came from the Jenkins server");
		} else {
			LOGGER.error("The Jenkins server response leaded to errors: \n - {} \n\n Response: \n{}",
			             Joiner.on("\n - ").join(response.getErrors()), response.getResponseBody());
		}
	}

	/**
	 * Creates a new {@link Request} using the {@link RequestFactory}
	 *
	 * @param methodType the request {@link Request.MethodType method}
	 * @param uri        the target {@link URI}
	 */
	private Request<?, ?> createRequest(Request.MethodType methodType, URI uri) {
		String url = uri.toASCIIString();
		Request<?, ?> request = requestFactory.createRequest(methodType, url);
		if (site.isSecured()) {
			request.addBasicAuthentication(uri.getHost(), site.getUser(), site.getToken());
		}
		request.setConnectionTimeout(CALL_TIMEOUT);
		request.setSoTimeout(CALL_TIMEOUT);
		if (Request.MethodType.POST.equals(methodType) && site.isUseCrumbs()) {
			LOGGER.debug("A Crumb is required to have a change at completing {} successfully", url);
			RestResponse crumbResponse = createAndExecuteRequest(Request.MethodType.GET,
			                                                     siteUriBuilder.apply("crumbIssuer/api/json").build());
			JSONObject crumbJson = crumbResponse.getJson();
			try {
				if (crumbJson.has("crumb") && crumbJson.has("crumbRequestField")) {
					request.addHeader(crumbJson.getString("crumbRequestField"), crumbJson.getString("crumb"));
				} else {
					LOGGER.error("Invalid crumb response received from remote {}", site.getName());
				}
			} catch (Exception e) {
				LOGGER.warn("Unable to get crumb information from {}, this may affect POST request to {}", site.getName(), url);
			}
		}
		return request;
	}

	/**
	 * Creates and executes a new request
	 *
	 * @param methodType the request {@link Request.MethodType method}
	 * @param uri        the target {@link URI}
	 * @return the {@link RestResponse}
	 */
	private RestResponse createAndExecuteRequest(Request.MethodType methodType, URI uri) {
		return executeRequest(createRequest(methodType, uri));
	}

	/**
	 * Execute the given {@link ApplicationLinkRequest}
	 *
	 * @param request the {@link ApplicationLinkRequest} to execute
	 * @return the {@link RestResponse} response
	 */
	private <R extends Response> RestResponse executeRequest(Request<?, R> request) {
		RestResponse response;
		try {
			response = request.executeAndReturn(RestResponse::new);
		} catch (ResponseConnectTimeoutException | ResponseReadTimeoutException e) {
			LOGGER.warn("Connection timeout occurred on {} [{}]", site.getName(), site.getRpcUrl(), e);
			response = new RestResponse("Unable to connect to Jenkins. Connection timed out.");
		} catch (ResponseException e) {
			LOGGER.error("Failed to connect to {} [{}]; {}", site.getName(), site.getRpcUrl(), e.getMessage(), e);
			response = new RestResponse("Failed to execute request: " + e.getMessage());
		}
		if (response.hasAuthErrors()) {
			LOGGER.error("Authentication failure on {} [{}]", site.getName(), site.getRpcUrl());
		}
		return response;
	}

}
