/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao;

import org.marvelution.jira.plugins.jenkins.model.TestResults;

/**
 * Interface for {@link TestResults} related Data Access
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
public interface TestResultDAO {

	/**
	 * Get the {@link TestResults} for a specific build
	 *
	 * @param buildId the build Id
	 * @return the {@link TestResults}
	 */
	TestResults getForBuild(int buildId);

	/**
	 * Save the {@link TestResults}
	 *
	 * @param buildId the Id of the build
	 * @param results the {@link TestResults} to save
	 * @return the {@link TestResults}
	 */
	TestResults save(int buildId, TestResults results);

	/**
	 * Delete all the {@link TestResults}s linked to the given builds by buildId
	 *
	 * @param buildIds the Ids of the builds to delete the {@link TestResults} from
	 */
	void delete(int[] buildIds);

}
