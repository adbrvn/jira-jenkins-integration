/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Job Mappings
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Preload
public interface JobMapping extends Entity {

	String SITE_ID = "SITE_ID";
	String NAME = "NAME";
	String URL_NAME = "URL_NAME";
	String DISPLAY_NAME = "DISPLAY_NAME";
	String LAST_BUILD = "LAST_BUILD";
	String LINKED = "LINKED";
	String DELETED = "DELETED";

	int getSiteId();

	void setSiteId(int siteId);

	String getName();

	void setName(String name);

	/**
	 * @since 2.1.0
	 */
	String getUrlName();

	/**
	 * @since 2.1.0
	 */
	void setUrlName(String urlName);

	String getDisplayName();

	void setDisplayName(String displayName);

	int getLastBuild();

	void setLastBuild(int lastBuild);

	boolean isLinked();

	void setLinked(boolean linked);

	boolean isDeleted();

	void setDeleted(boolean deleted);

}
