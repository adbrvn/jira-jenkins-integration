/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import org.marvelution.jira.plugins.jenkins.dao.IssueDAO;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.BUILD_ID;
import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.ISSUE_KEY;
import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.JOB_ID;
import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.PROJECT_KEY;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;
import static net.java.ao.Query.select;

/**
 * Default {@link IssueDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultIssueDAO extends AbstractActiveObjectsDAO<IssueMapping> implements IssueDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultIssueDAO.class);
	private final IssueManager issueManager;

	@Inject
	public DefaultIssueDAO(@ComponentImport ActiveObjects activeObjects, @ComponentImport IssueManager issueManager) {
		super(activeObjects, IssueMapping.class);
		this.issueManager = issueManager;
	}

	@Override
	public Set<String> getIssueKeysByBuild(Build build) {
		return getIssueKeysByQuery(select().where(BUILD_ID + " = ?", build.getId()));
	}

	@Override
	public Set<String> getIssueKeysByJob(Job job) {
		return getIssueKeysByQuery(select().where(JOB_ID + " = ?", job.getId()));
	}

	@Override
	public Set<String> getProjectKeysByBuild(Build build) {
		return find(select().where(BUILD_ID + " = ?", build.getId())).map(IssueMapping::getProjectKey).collect(toSet());
	}

	@Override
	public int getIssueLinkCount(Build build) {
		return executeInTransaction(() -> activeObjects.count(IssueMapping.class, select().where(BUILD_ID + " = ?", build.getId())));
	}

	@Override
	public boolean link(Build build, String issueKey) {
		requireNonNull(build, "build may not be null");
		requireNonNull(issueKey, "issueKey may not be null");
		Issue issue = issueManager.getIssueObject(issueKey);
		if (issue != null) {
			LOGGER.debug("Linking {} to {}", issue.getKey(), build.toString());
			upsert(0, mapping -> {
				mapping.setJobId(build.getJobId());
				mapping.setBuildId(build.getId());
				mapping.setIssueKey(issue.getKey());
				mapping.setProjectKey(issue.getProjectObject().getKey());
				mapping.setBuildDate(build.getTimestamp());
			});
			return true;
		} else {
			LOGGER.debug("No issue with key {} on this JIRA instance, skipping link", issueKey);
			return false;
		}
	}

	@Override
	public void unlinkForIssueKey(String issueKey) {
		requireNonNull(issueKey, "issueKey may not be null");
		LOGGER.debug("Unlinked issue {} from all known builds", issueKey);
		delete(ISSUE_KEY + " = ?", issueKey);
	}

	@Override
	public void unlinkForProjectKey(String projectKey) {
		requireNonNull(projectKey, "projectKey may not be null");
		LOGGER.debug("Unlinked project {} from all known builds", projectKey);
		delete(PROJECT_KEY + " = ?", projectKey);
	}

	private Set<String> getIssueKeysByQuery(Query query) {
		return find(query).map(IssueMapping::getIssueKey).collect(toSet());
	}

}
