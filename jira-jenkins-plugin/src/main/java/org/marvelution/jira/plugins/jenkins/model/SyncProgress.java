/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.XmlElement;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public abstract class SyncProgress {

	private volatile boolean shouldStop = false;
	private boolean finished = false;
	private String error;
	private int syncCount = 0;
	private int syncErrorCount = 0;

	/**
	 * Called when the synchronization is started to record the start time
	 */
	public void start() {
	}

	/**
	 * Called when the synchronization is finished
	 */
	public void finish() {
		finished = true;
	}

	/**
	 * Called when the synchronization operation is queued
	 */
	public void queued() {
	}

	/**
	 * Update the progress by updating the sync count, issue count and sync error count
	 *
	 * @param syncCount      the number of items processed
	 * @param syncErrorCount the number of error that occurred
	 */
	protected void updateProgress(int syncCount, int syncErrorCount) {
		this.syncCount = syncCount;
		this.syncErrorCount = syncErrorCount;
	}

	@XmlElement
	public boolean isFinished() {
		return finished;
	}

	@XmlElement
	public boolean isShouldStop() {
		return shouldStop;
	}

	public void setShouldStop(boolean shouldStop) {
		this.shouldStop = shouldStop;
	}

	@XmlElement
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@XmlElement
	public int getSyncCount() {
		return syncCount;
	}

	@XmlElement
	public int getSyncErrorCount() {
		return syncErrorCount;
	}

}
