/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.utils;

import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.software.api.conditions.IsSoftwareProjectCondition;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.osgi.bridge.external.PluginRetrievalService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.osgi.framework.Version;

import static java.util.Collections.singletonMap;

/**
 * Plugin Util
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class JenkinsPluginUtil {

	private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;
	private final IsSoftwareProjectCondition isSoftwareProjectCondition;
	private final ProjectManager projectManager;
	private final IssueManager issueManager;
	private final PluginRetrievalService pluginRetrievalService;

	@Inject
	public JenkinsPluginUtil(ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition,
	                         IsSoftwareProjectCondition isSoftwareProjectCondition,
	                         @ComponentImport ProjectManager projectManager, @ComponentImport IssueManager issueManager,
	                         @ComponentImport PluginRetrievalService pluginRetrievalService) {
		this.projectDevToolsIntegrationFeatureCondition = projectDevToolsIntegrationFeatureCondition;
		this.isSoftwareProjectCondition = isSoftwareProjectCondition;
		this.projectManager = projectManager;
		this.issueManager = issueManager;
		this.pluginRetrievalService = pluginRetrievalService;
	}

	/**
	 * Returns the {@link Plugin}
	 */
	public Plugin getPlugin() {
		return pluginRetrievalService.getPlugin();
	}

	/**
	 * Returns the plugin key
	 */
	public String getPluginKey() {
		return getPlugin().getKey();
	}

	/**
	 * Returns the complete key of the given plugin specific {@literal moduleKey}
	 *
	 * @since 2.0.0
	 */
	public String getCompleteModuleKey(String moduleKey) {
		return String.format("%s:%s", getPluginKey(), moduleKey);
	}

	/**
	 * Returns the plugin version
	 */
	public Version getVersion() {
		return Version.parseVersion(getPlugin().getPluginInformation().getVersion());
	}

	public boolean isFeatureEnabled(String issueKey) {
		Issue issue = issueManager.getIssueObject(issueKey);
		return issue != null && isFeatureEnabled(issue);
	}

	public boolean isFeatureEnabled(Issue issue) {
		Project project = issue.getProjectObject();
		if (project == null) {
			project = projectManager.getProjectObj(issue.getProjectId());
		}
		return project != null && isFeatureEnabled(project);
	}

	public boolean isFeatureEnabled(Project project) {
		Map<String, Object> browseContext = singletonMap(ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT, project);
		return isSoftwareProjectCondition.shouldDisplay(browseContext) &&
				projectDevToolsIntegrationFeatureCondition.shouldDisplay(browseContext);
	}

}
