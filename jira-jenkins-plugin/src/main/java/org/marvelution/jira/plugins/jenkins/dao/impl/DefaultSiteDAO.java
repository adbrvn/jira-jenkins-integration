/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import java.net.URI;
import java.util.List;
import java.util.function.Function;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.ao.SiteMapping;
import org.marvelution.jira.plugins.jenkins.dao.SiteDAO;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteType;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static net.java.ao.Query.select;
import static org.apache.commons.lang.StringUtils.stripEnd;

/**
 * Default {@link SiteDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultSiteDAO extends AbstractActiveObjectsDAO<SiteMapping> implements SiteDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSiteDAO.class);
	private final Function<SiteMapping, Site> mappingToObject = from -> {
		Site site = new Site(from.getID());
		site.setType(SiteType.valueOf(from.getSiteType()));
		site.setRpcUrl(URI.create(stripEnd(from.getRpcUrl(), "/") + "/"));
		if (from.getName() != null) {
			site.setName(from.getName());
		} else {
			site.setName(String.format("%s at %s", StringUtils.capitalize(site.getType().i18nKey()), site.getRpcUrl()));
		}
		if (from.getDisplayUrl() != null) {
			site.setDisplayUrl(URI.create(stripEnd(from.getDisplayUrl(), "/") + "/"));
		}
		site.setSupportsBackLink(from.isSupportsBackLink());
		site.setAutoLink(from.isAutoLink());
		site.setUseCrumbs(from.isUseCrumbs());
		site.setUser(from.getUser());
		site.setToken(from.getUserToken());
		return site;
	};

	@Inject
	public DefaultSiteDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, SiteMapping.class);
	}

	@Override
	public Site get(int siteId) {
		return getOne(siteId)
				.map(mappingToObject).orElse(null);
	}

	@Override
	public List<Site> getAll() {
		return find(select())
				.map(mappingToObject).collect(toList());
	}

	@Override
	public Site save(Site site) {
		LOGGER.debug("Saving {}", requireNonNull(site).toString());
		return upsert(site.getId(), mapping -> {
			mapping.setSiteType(site.getType().name());
			mapping.setName(site.getName());
			mapping.setRpcUrl(site.getRpcUrl().toASCIIString());
			mapping.setDisplayUrl(site.hasDisplayUrl() ? site.getDisplayUrl().toASCIIString() : null);
			mapping.setSupportsBackLink(site.isSupportsBackLink());
			mapping.setAutoLink(site.isAutoLink());
			mapping.setUseCrumbs(site.isUseCrumbs());
			if (site.getUser() != null && site.getToken() != null) {
				mapping.setUser(site.getUser());
				mapping.setUserToken(site.getToken());
			} else {
				mapping.setUser(null);
				mapping.setUserToken(null);
			}
		}).map(mappingToObject).orElse(null);
	}

	@Override
	public void delete(int siteId) {
		deleteOne(siteId);
	}

}
