/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.util.List;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.dao.SiteDAO;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteSyncProgress;
import org.marvelution.jira.plugins.jenkins.model.SiteSyncStatus;
import org.marvelution.jira.plugins.jenkins.services.JobService;
import org.marvelution.jira.plugins.jenkins.services.SiteService;
import org.marvelution.jira.plugins.jenkins.sync.Synchronizer;
import org.marvelution.jira.plugins.jenkins.sync.impl.SiteSynchronizationOperation;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Default {@link SiteService} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(SiteService.class)
public class DefaultSiteService implements SiteService {

	private final SiteDAO siteDAO;
	private final JobService jobService;
	private final Synchronizer synchronizer;

	@Inject
	public DefaultSiteService(SiteDAO siteDAO, JobService jobService, Synchronizer synchronizer) {
		this.siteDAO = siteDAO;
		this.jobService = jobService;
		this.synchronizer = synchronizer;
	}

	@Override
	public Site get(int siteId) {
		return siteDAO.get(siteId);
	}

	@Override
	public List<Site> getAll(final boolean includeJobs) {
		List<Site> sites = siteDAO.getAll();
		if (includeJobs) {
			return sites.stream().map(site -> {
				site.setJobs(jobService.getAllBySite(site, true));
				return site;
			}).collect(toList());
		} else {
			return sites;
		}
	}

	@Override
	public void enable(int siteId, boolean enabled) {
		Site site = get(siteId);
		if (site != null) {
			site.setAutoLink(enabled);
			siteDAO.save(site);
		}
	}

	@Override
	public Site save(Site site) {
		return siteDAO.save(site);
	}

	@Override
	public void delete(Site site) {
		synchronizer.stopSynchronization(SiteSynchronizationOperation.operationId(site));
		siteDAO.delete(site.getId());
		jobService.deleteAllFromSite(site);
	}

	@Override
	public void sync(int siteId) {
		sync(get(siteId));
	}

	@Override
	public void sync(Site site) {
		requireNonNull(site);
		synchronizer.schedule(new SiteSynchronizationOperation(this, site));
	}

	@Override
	public SiteSyncStatus getSyncStatus(Site site) {
		SiteSyncStatus status = new SiteSyncStatus();
		status.setSite(requireNonNull(site));
		status.setProgress((SiteSyncProgress) synchronizer.getProgress(SiteSynchronizationOperation.operationId(site)));
		return status;
	}

}
