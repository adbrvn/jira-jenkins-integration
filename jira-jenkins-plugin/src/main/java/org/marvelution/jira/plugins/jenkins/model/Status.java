/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.XmlEnum;

/**
 * The Status types
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlEnum(String.class)
public enum Status {

	OFFLINE(false, false),
	ONLINE(true, true),
	NOT_ACCESSIBLE(true, false);

	private final boolean online;
	private final boolean accessible;

	Status(boolean online, boolean accessible) {
		this.online = online;
		this.accessible = accessible;
	}

	/**
	 * Returns whether in this state the remote is online or not
	 */
	public boolean isOnline() {
		return online;
	}

	/**
	 * Returns whether in this state the remote is online and accessible or not
	 */
	public boolean isAccessible() {
		return accessible;
	}

}
