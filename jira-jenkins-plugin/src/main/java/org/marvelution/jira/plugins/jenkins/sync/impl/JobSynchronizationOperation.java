/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.ChangeSet;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.JobSyncProgress;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.sync.BuildIssueLinksUpdatedEvent;
import org.marvelution.jira.plugins.jenkins.sync.OperationId;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperationException;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.index.IssueIndexingParams;
import com.atlassian.jira.issue.index.IssueIndexingService;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.commons.lang3.StringUtils;

import static java.util.stream.Collectors.toSet;

/**
 * Job Specific {@link SynchronizationOperation}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Scanned
public class JobSynchronizationOperation extends AbstractJobListSynchronizationOperation<JobSyncProgress> {

	private final JobSyncProgress progress;
	private Job job;
	@Inject
	private BuildService buildService;
	@Inject
	@ComponentImport
	private IssueManager issueManager;
	@Inject
	@ComponentImport
	private IssueIndexingService issueIndexingService;
	@Inject
	@ComponentImport
	private EventPublisher eventPublisher;

	public JobSynchronizationOperation(Job job) {
		this.job = job;
		progress = new JobSyncProgress();
	}

	public static OperationId operationId(Job job) {
		return OperationId.of("job-build-sync-" + job.getId());
	}

	@Nonnull
	@Override
	protected Communicator createCommunicator(@Nonnull CommunicatorFactory factory) {
		return factory.get(job.getSiteId());
	}

	@Override
	public OperationId getOperationId() {
		return operationId(job);
	}

	@Override
	protected void doSynchronize() throws SynchronizationOperationException {
		int buildCount = 0, issueCount = 0, errorCount = 0;
		try {
			job = getCommunicator().getDetailedJob(job);
			if (!job.isDeleted()) {
				if (!job.getJobs().isEmpty()) {
					logger.debug("Synchronizing sub-jobs of {}", job.getName());
					synchronizeJobs(job.getJobs(), getJobService().getAllFromSameSite(job, true), job -> getJobService().sync(job.getId()));
				}
				logger.debug("Synchronizing builds from Job {}", job.getName());
				for (Build build : getBuildsToSync(job)) {
					if (progress.isShouldStop()) {
						break;
					}
					try {
						Build detailedBuild = getCommunicator().getDetailedBuild(job, build);
						if (!detailedBuild.isDeleted() && detailedBuild.getResult() != null) {
							if (build.getNumber() > job.getLastBuild()) {
								job.setLastBuild(build.getNumber());
							}
							buildCount++;
							logger.debug("Synchronizing Build {} from {}", detailedBuild.getNumber(), job.getName());
							detailedBuild = buildService.save(detailedBuild);
							Set<String> existingRelations = buildService.getRelatedIssueKeys(build);
							Set<String> issueKeys = new HashSet<>();
							for (String key : extractIssueKeys(detailedBuild)) {
								try {
									if (!existingRelations.contains(key) && buildService.link(detailedBuild, key)) {
										issueKeys.add(key);
									}
								} catch (Exception e) {
									logger.warn("Failed to link build {} of {} with issue {}", build.getNumber(), job.getName(), key);
								}
							}
							if (!issueKeys.isEmpty()) {
								eventPublisher.publish(new BuildIssueLinksUpdatedEvent(build.getId(), issueKeys));
								issueIndexingService.reIndexIssueObjects(issueKeys.stream().map(issueManager::getIssueObject).collect(toSet()),
								                                         IssueIndexingParams.INDEX_ISSUE_ONLY, true);
								issueCount += issueKeys.size();
							}
						} else {
							logger.debug("Skipping Build {} from {}", detailedBuild.getNumber(), job.getName());
						}
					} catch (Exception e) {
						logger.warn("Failed to synchronize build {} of {}: {}", build.getNumber(), job.getName(), e.getMessage(), e);
						errorCount++;
					}
					progress.updateProgress(buildCount, issueCount, errorCount);
				}
				logger.info("Marking all builds of {} prior to {} as deleted", job.getName(), job.getOldestBuild());
				buildService.markAllInJobAsDeleted(job, job.getOldestBuild());
			} else {
				logger.info("Seems job {} is no longer available, marking it as deleted", job.getName());
				getJobService().markAsDeleted(job);
			}
			getJobService().save(job);
		} catch (Exception e) {
			logger.warn("Failed to Synchronize {}: {}", job.getName(), e.getMessage(), e);
		}
	}

	@Override
	public JobSyncProgress getProgress() {
		return progress;
	}

	/**
	 * Get all the remote builds from the Job given starting from the last build given
	 *
	 * @param job the {@link Job} to get the builds for
	 * @return collection of {@link Build} objects
	 */
	private Set<Build> getBuildsToSync(final Job job) {
		return job.getBuilds().stream()
		          .filter(build -> build.getNumber() > job.getLastBuild())
		          .sorted(Comparator.comparingInt(Build::getNumber).reversed())
		          .collect(toSet());
	}

	/**
	 * Extract all the issue keys from the given {@link Build} It will look for issue keys in the build cause,
	 * change-sets and artifacts
	 *
	 * @param build the {@link Build} to extract the issue keys from
	 * @return the {@link Set} of extracted issue keys
	 */
	private Set<String> extractIssueKeys(Build build) {
		Set<String> keys = new HashSet<>();
		keys.addAll(IssueKeyExtractor.extractIssueKeys(job.getDisplayName(), job.getDescription()));
		keys.addAll(IssueKeyExtractor.extractIssueKeys(build.getDisplayName(), build.getDescription(), build.getCause()));
		for (ChangeSet changeSet : build.getChangeSet()) {
			keys.addAll(IssueKeyExtractor.extractIssueKeys(changeSet.getMessage()));
		}
		logger.debug("Found [{}] related to {}", StringUtils.join(keys, ", "), build.toString());
		return keys;
	}

}
