/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.panels;

import java.util.ArrayList;
import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Build;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel2;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsReply;
import com.atlassian.jira.plugin.issuetabpanel.GetActionsRequest;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelReply;
import com.atlassian.jira.plugin.issuetabpanel.ShowPanelRequest;

/**
 * The {@link AbstractIssueTabPanel2} implementation to get all the {@link Build}s related to teh {@link Issue}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsIssuePanel extends AbstractIssueTabPanel2 {

	private final BuildPanelHelper buildPanelHelper;

	public JenkinsIssuePanel(BuildPanelHelper buildPanelHelper) {
		this.buildPanelHelper = buildPanelHelper;
	}

	@Override
	public GetActionsReply getActions(GetActionsRequest getActionsRequest) {
		List<IssueAction> buildActions = new ArrayList<>();
		Iterable<? extends Build> builds = buildPanelHelper.getBuildsByRelation(getActionsRequest.issue());
		buildActions.addAll(buildPanelHelper.getBuildActions(builds, true));
		return GetActionsReply.create(buildActions);
	}

	@Override
	public ShowPanelReply showPanel(ShowPanelRequest showPanelRequest) {
		return ShowPanelReply.create(buildPanelHelper.showPanel(showPanelRequest.issue()));
	}

}
