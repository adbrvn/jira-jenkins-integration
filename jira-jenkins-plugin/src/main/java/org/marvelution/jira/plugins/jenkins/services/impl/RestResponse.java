/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import org.apache.commons.httpclient.HttpStatus;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class RestResponse {

	private final List<String> errors = new ArrayList<>();
	private final Response response;
	private String responseBody;
	private JSONObject json;

	public RestResponse(Response response) {
		this.response = response;
		try {
			responseBody = response.getResponseBodyAsString();
		} catch (Throwable e) {
			errors.add("Failed to retrieve the response from Jenkins: " + e.getMessage());
		}
	}

	public RestResponse(String... errors) {
		response = new FakeResponse();
		this.errors.addAll(Arrays.asList(errors));
	}

	/**
	 * Getter for the success state
	 *
	 * @return the success state
	 */
	public boolean isSuccessful() {
		return response.isSuccessful() && !hasAuthErrors() && !hasErrors();
	}

	/**
	 * Returns whether there where auth related errors in hte response
	 *
	 * @return {@literal true} in case the {@link #getStatusCode() response status code} is {@link HttpStatus#SC_FORBIDDEN forbidden} or
	 * {@link HttpStatus#SC_UNAUTHORIZED unauthorized}
	 */
	public boolean hasAuthErrors() {
		int statusCode = getStatusCode();
		return statusCode == HttpStatus.SC_FORBIDDEN || statusCode == HttpStatus.SC_UNAUTHORIZED;
	}

	public boolean hasServerResponse() {
		return !(response instanceof FakeResponse);
	}

	/**
	 * Getter for the status code
	 *
	 * @return the status code
	 */
	public int getStatusCode() {
		return response.getStatusCode();
	}

	/**
	 * Getter for the status message
	 *
	 * @return the status message
	 */
	public String getStatusMessage() {
		return response.getStatusText();
	}

	/**
	 * Getter for the response body as a {@link String}
	 * If you're expecting a {@link JSONObject} then you can use the {@link #getJson()}
	 *
	 * @return the response body as a {@link String}
	 * @see #getJson()
	 */
	public String getResponseBody() {
		return responseBody;
	}

	/**
	 * Getter for the response body as an {@link JSONObject}
	 *
	 * @return the response body as a {@link JSONObject}, may be {@code null}
	 */
	public JSONObject getJson() {
		if (json == null && isNotBlank(responseBody)) {
			try {
				json = new JSONObject(responseBody);
			} catch (JSONException e) {
				json = null;
				errors.add("Failed to parse server response as JSON: " + e.getMessage());
			}
		}
		return json;
	}

	/**
	 * Getter for the error collection
	 *
	 * @return the error collection, may be {@code empty}
	 */
	public List<String> getErrors() {
		return errors;
	}

	/**
	 * Returns whether the {@link #errors} list contains any errors
	 */
	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	private static class FakeResponse implements Response {

		@Override
		public int getStatusCode() {
			return HttpStatus.SC_CONFLICT;
		}

		@Override
		public String getResponseBodyAsString() throws ResponseException {
			return null;
		}

		@Override
		public InputStream getResponseBodyAsStream() throws ResponseException {
			return null;
		}

		@Override
		public <T> T getEntity(Class<T> entityClass) throws ResponseException {
			return null;
		}

		@Override
		public String getStatusText() {
			return null;
		}

		@Override
		public boolean isSuccessful() {
			return false;
		}

		@Override
		public String getHeader(String name) {
			return null;
		}

		@Override
		public Map<String, String> getHeaders() {
			return null;
		}
	}

}
