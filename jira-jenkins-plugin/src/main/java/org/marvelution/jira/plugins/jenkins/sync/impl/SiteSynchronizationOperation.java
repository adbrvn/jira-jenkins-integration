/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import javax.annotation.Nonnull;

import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteSyncProgress;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.services.SiteService;
import org.marvelution.jira.plugins.jenkins.sync.OperationId;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperationException;

/**
 * Site Specific {@link SynchronizationOperation}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class SiteSynchronizationOperation extends AbstractJobListSynchronizationOperation<SiteSyncProgress> {

	private final SiteService siteService;
	private final SiteSyncProgress progress;
	private final Site site;

	public SiteSynchronizationOperation(SiteService siteService, Site site) {
		this.siteService = siteService;
		this.site = site;
		progress = new SiteSyncProgress();
	}

	public static OperationId operationId(Site site) {
		return OperationId.of("site-jobs-sync-" + site.getId());
	}

	@Nonnull
	@Override
	protected Communicator createCommunicator(@Nonnull CommunicatorFactory factory) {
		return factory.get(site);
	}

	@Override
	public OperationId getOperationId() {
		return operationId(site);
	}

	@Override
	protected void doSynchronize() throws SynchronizationOperationException {
		logger.debug("Checking if the Jenkins plugin is installed and if crumb security is used on {}", site);
		site.setSupportsBackLink(getCommunicator().isJenkinsPluginInstalled());
		if (site.isSupportsBackLink()) {
			getCommunicator().registerWithSite();
		}
		site.setUseCrumbs(getCommunicator().isCrumbSecurityEnabled());
		siteService.save(site);
		logger.info("Synchronizing all Jobs from {}", site.getName());
		progress.updateProgress(synchronizeJobs(getCommunicator().getJobs(), getJobService().getAllBySite(site, true)), 0);
		getJobService().syncAllFromSite(site);
	}

	@Override
	public SiteSyncProgress getProgress() {
		return progress;
	}

}
