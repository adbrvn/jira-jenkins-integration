/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync;

import org.marvelution.jira.plugins.jenkins.model.SyncProgress;

/**
 * Synchronizer interface for synchronizing builds of jobs
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface Synchronizer {

	/**
	 * Schedule the specified {@link SynchronizationOperation} for execution
	 */
	void schedule(SynchronizationOperation operation);

	/**
	 * Request the operation with the specified {@link OperationId} to be stopped
	 */
	void stopSynchronization(OperationId operationId);

	/**
	 * Returns the {@link SyncProgress} of the {@link SynchronizationOperation} with teh specified {@link OperationId}
	 */
	SyncProgress getProgress(OperationId operationId);

}
