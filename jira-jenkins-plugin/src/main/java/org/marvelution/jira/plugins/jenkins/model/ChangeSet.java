/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Build change set
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ChangeSet {

	private String commitId;
	private String message;

	/**
	 * Default constructor for JAXB
	 */
	ChangeSet() {
	}

	public ChangeSet(String commitId, @Nullable String message) {
		this.commitId = commitId;
		this.message = message;
	}

	/**
	 * Getter for the Commit Id
	 *
	 * @return the Commit Id
	 */
	public String getCommitId() {
		return commitId;
	}

	/**
	 * Getter for the commit message
	 *
	 * @return the commit message
	 */
	@Nullable
	public String getMessage() {
		return message;
	}

}
