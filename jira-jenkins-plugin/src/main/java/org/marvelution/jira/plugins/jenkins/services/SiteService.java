/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services;

import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.model.SiteSyncStatus;

/**
 * Service interface for the {@link Site} objects
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SiteService {

	/**
	 * Getter for a {@link Site} by its Id
	 *
	 * @param siteId the Id to get
	 * @return the {@link Site}, may be {@code null}
	 */
	Site get(int siteId);

	/**
	 * Getter for all the {@link Site}s
	 *
	 * @param includeJobs flag whether to include the jobs or not
	 * @return collection of all the {@link Site}s
	 */
	List<Site> getAll(boolean includeJobs);

	/**
	 * Enable/Disable the default enabled state for new jobs of a Site
	 *
	 * @param siteId  the ID of the {@link Site} to update the synchronization state of
	 * @param enabled the synchronization state ({@code true} to enable synchronization and {@code false} to disable it)
	 */
	void enable(int siteId, boolean enabled);

	/**
	 * Save the given {@link Site}
	 *
	 * @param site the {@link Site} to save
	 * @return the saved {@link Site}
	 */
	Site save(Site site);

	/**
	 * Delete the given site and all its related jobs and builds
	 *
	 * @param site the {@link Site} to delete
	 */
	void delete(Site site);

	/**
	 * Synchronize the specified {@link Site} by its Id
	 */
	void sync(int siteId);

	/**
	 * Synchronize the specified {@link Site}
	 */
	void sync(Site site);

	/**
	 * Get the current synchronization status of the site given
	 *
	 * @param site the {@link Site} to get the {@link SiteSyncStatus} for
	 * @return the {@link SiteSyncStatus}, may be {@code null} in case of no current status
	 */
	SiteSyncStatus getSyncStatus(Site site);

}
