/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.net.URI;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * Default {@link JenkinsPluginConfiguration} implementation
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@Named
@ExportAsDevService(JenkinsPluginConfiguration.class)
public class DefaultJenkinsPluginConfiguration implements JenkinsPluginConfiguration {

	public static final String SETTING_PREFIX = "jji.";
	private static final String MAX_BUILDS_PER_PAGE = SETTING_PREFIX + "max_builds_per_page";
	private static final String JIRA_BASE_RPC_URL = SETTING_PREFIX + "jira_base_rpc_url";
	private final ApplicationProperties applicationProperties;
	private final PluginSettingsFactory pluginSettingsFactory;
	private transient PluginSettings pluginSettings;

	@Inject
	public DefaultJenkinsPluginConfiguration(@ComponentImport ApplicationProperties applicationProperties,
	                                         @ComponentImport PluginSettingsFactory pluginSettingsFactory) {
		this.applicationProperties = applicationProperties;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	public static PluginSettings getPluginSettings(PluginSettingsFactory pluginSettingsFactory) {
		return pluginSettingsFactory.createGlobalSettings();
	}

	@Override
	public URI getJIRABaseUrl() {
		return URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL));
	}

	@Override
	public URI getJIRABaseRpcUrl() {
		String uri = (String) getPluginSettings().get(JIRA_BASE_RPC_URL);
		return isNotBlank(uri) ? URI.create(uri) : getJIRABaseUrl();
	}

	@Override
	public void setJIRABaseRpcUrl(URI rpcUrl) {
		if (rpcUrl == null || rpcUrl.equals(getJIRABaseUrl())) {
			getPluginSettings().remove(JIRA_BASE_RPC_URL);
		} else {
			getPluginSettings().put(JIRA_BASE_RPC_URL, rpcUrl.toASCIIString());
		}
	}

	@Override
	public int getMaximumBuildsPerPage() {
		Integer count;
		try {
			count = Integer.parseInt((String) getPluginSettings().get(MAX_BUILDS_PER_PAGE));
		} catch (Exception e) {
			count = DEFAULT_MAX_BUILDS_PER_PAGE;
		}
		return count;
	}

	@Override
	public void setMaximumBuildsPerPage(Integer maximumBuildsPerPage) {
		if (maximumBuildsPerPage == null || DEFAULT_MAX_BUILDS_PER_PAGE == maximumBuildsPerPage ||
				(maximumBuildsPerPage < 1 && maximumBuildsPerPage != -1)) {
			getPluginSettings().remove(MAX_BUILDS_PER_PAGE);
		} else {
			getPluginSettings().put(MAX_BUILDS_PER_PAGE, String.valueOf(maximumBuildsPerPage));
		}
	}

	@Override
	public String getJIRAInstanceName() {
		return applicationProperties.getDisplayName();
	}

	private PluginSettings getPluginSettings() {
		if (pluginSettings == null) {
			pluginSettings = getPluginSettings(pluginSettingsFactory);
		}
		return pluginSettings;
	}

}
