/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao;

import java.util.Set;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;

import org.apache.commons.lang.math.Range;

/**
 * {@link Build} Data Access Service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface BuildDAO {

	/**
	 * Get the {@link Build} by its ID
	 *
	 * @param buildId the build id
	 * @return the {@link Build}
	 */
	Build get(int buildId);

	/**
	 * Get a single {@link Build} by its jobId reference and build number
	 *
	 * @param jobId       the jobId reference of the {@link Build} to get
	 * @param buildNumber the build number of the {@link Build}
	 * @return the {@link Build}, may be {@code null}
	 */
	Build get(int jobId, int buildNumber);

	/**
	 * Get all the {@link Build}s by there jobId reference and build number range
	 *
	 * @param jobId      the jobId reference of the {@link Build} to get
	 * @param buildRange the range of build number of the {@link Build}
	 * @return the {@link Build}s, may be {@code empty}
	 * @since 1.4.8
	 */
	Set<Build> getAllInRange(int jobId, Range buildRange);

	/**
	 * Get all the {@link Build}s by there jobId reference
	 *
	 * @param jobId the Id of the Job to get all the builds for
	 * @return all the {@link Build}s
	 */
	Set<Build> getAllByJob(int jobId);

	/**
	 * Get all the builds of links by the given issue key
	 *
	 * @param issueKey the issue key to get all the links for
	 * @return collection of all the builds the given issue key links with, may be {@code empty} but not {@code null}
	 * @since 1.5.0
	 */
	Set<Build> getByIssueKey(String issueKey);

	/**
	 * Get all the builds of links by the given project key
	 *
	 * @param projectKey the project key to get all the links for
	 * @return collection of all the builds the given project key links with, may be {@code empty} but not {@code null}
	 * @since 1.5.0
	 */
	Set<Build> getByProjectKey(String projectKey);

	/**
	 * Get all the builds that match the given {@link BuildIssueFilter}
	 *
	 * @param maxResults the maximum number of results
	 * @param filter     the {@link BuildIssueFilter}
	 * @return collection of all the matching builds
	 * @since 1.5.0
	 */
	Set<Build> getLatestByFilter(int maxResults, BuildIssueFilter filter);

	/**
	 * /**
	 * Save the given {@link Build}
	 *
	 * @param build the {@link Build} to save
	 * @return the saved {@link Build}
	 */
	Build save(Build build);

	/**
	 * Delete a {@link Build}
	 *
	 * @param buildId the id of the {@link Build} to delete
	 * @return the id of the removed build, should match the {@code buildId} parameter
	 */
	int delete(int buildId);

	/**
	 * Delete all the {@link Build}s by the jobId given
	 *
	 * @param jobId the Id of the Job to delete all the builds for
	 * @return the build ids that where deleted
	 */
	int[] deleteAllByJob(int jobId);

	/**
	 * Mark the given {@link Build} as deleted on the remote site
	 *
	 * @param build the {@link Build} to mark as deleted
	 */
	void markAsDeleted(Build build);

	/**
	 * Mark all the Builds with the jobId reference as deleted
	 *
	 * @param jobId the jobId reference to mark the builds of
	 */
	void markAllInJobAsDelete(int jobId);

	/**
	 * Mark all the Builds up to the given number of the given job Id as deleted
	 *
	 * @param jobId       the jobId to mark all the builds from
	 * @param buildNumber the build number till which the build should be marked as deleted
	 */
	void markAllInJobAsDelete(int jobId, int buildNumber);

}
