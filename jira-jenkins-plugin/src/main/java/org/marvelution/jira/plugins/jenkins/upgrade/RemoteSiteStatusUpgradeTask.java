/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.upgrade;

import java.util.Collection;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.services.SiteService;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link PluginUpgradeTask} implementation to update flags stored that related to remote site state link back-link and crumb usages
 * configuration.
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class RemoteSiteStatusUpgradeTask implements PluginUpgradeTask {

	private static final Logger LOGGER = LoggerFactory.getLogger(RemoteSiteStatusUpgradeTask.class);
	private final JenkinsPluginUtil pluginUtil;
	private final SiteService siteService;
	private final CommunicatorFactory communicatorFactory;

	@Inject
	public RemoteSiteStatusUpgradeTask(JenkinsPluginUtil pluginUtil, SiteService siteService, CommunicatorFactory communicatorFactory) {
		this.pluginUtil = pluginUtil;
		this.siteService = siteService;
		this.communicatorFactory = communicatorFactory;
	}

	@Override
	public int getBuildNumber() {
		return 5;
	}

	@Override
	public String getShortDescription() {
		return "Update remote site status";
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		for (Site site : siteService.getAll(false)) {
			Communicator communicator = communicatorFactory.get(site);
			site.setSupportsBackLink(communicator.isJenkinsPluginInstalled());
			site.setUseCrumbs(communicator.isCrumbSecurityEnabled());
			LOGGER.info("Updated site status of {}", site.getName());
			siteService.save(site);
		}
		return null;
	}

	@Override
	public String getPluginKey() {
		return pluginUtil.getPluginKey();
	}
}
