/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.jql;

import org.marvelution.jira.plugins.jenkins.model.Result;

import com.atlassian.fugue.Option;
import com.atlassian.query.clause.Property;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.clause.TerminalClauseImpl;
import com.atlassian.query.operand.SingleValueOperand;
import com.atlassian.query.operator.Operator;

import static java.util.Collections.singletonList;

/**
 * Helper for JQL related functions
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public class JqlHelper {

	/**
	 * Returns a {@link TerminalClause} for the {@code worstresult} build field that equals the specified {@link Result result}
	 */
	public static TerminalClause createWorstBuildTerminalClause(Result result) {
		return createTerminalClause(Operator.EQUALS, result.key(), "worstresult");
	}

	/**
	 * Returns a {@link TerminalClause} for a specific {@code field} and {@code value} using the specified {@link Operator operator}
	 */
	private static TerminalClause createTerminalClause(Operator operator, String value, String field) {
		return new TerminalClauseImpl("issue.property", operator, new SingleValueOperand(value),
		                              Option.some(new Property(singletonList("jenkins"), singletonList(field))));
	}

}
