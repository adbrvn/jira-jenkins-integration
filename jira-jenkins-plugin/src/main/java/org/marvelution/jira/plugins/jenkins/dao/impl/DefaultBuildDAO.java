/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.ao.BuildMapping;
import org.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import org.marvelution.jira.plugins.jenkins.dao.BuildDAO;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Result;
import org.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import org.marvelution.jira.plugins.jenkins.services.impl.BuildIssueFilterQueryBuilder;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import org.apache.commons.lang.math.Range;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jira.plugins.jenkins.ao.BuildMapping.BUILD_NUMBER;
import static org.marvelution.jira.plugins.jenkins.ao.BuildMapping.JOB_ID;
import static org.marvelution.jira.plugins.jenkins.ao.BuildMapping.TIME_STAMP;

import static java.util.Collections.singleton;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;
import static net.java.ao.Query.select;

/**
 * Default {@link BuildDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultBuildDAO extends AbstractActiveObjectsDAO<BuildMapping> implements BuildDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultBuildDAO.class);
	private static final int MAX_SIZE = 200;
	private final Function<BuildMapping, Build> mappingToObject = mapping -> {
		Build build = new Build(mapping.getID(), mapping.getJobId(), mapping.getBuildNumber());
		build.setDisplayName(mapping.getDisplayName());
		build.setCause(mapping.getCause());
		build.setDuration(mapping.getDuration());
		build.setTimestamp(mapping.getTimeStamp());
		build.setResult(Result.fromString(mapping.getResult()));
		build.setBuiltOn(mapping.getBuiltOn());
		build.setDeleted(mapping.isDeleted());
		return build;
	};
	private final BuildIssueFilterQueryBuilder queryBuilder;

	@Inject
	public DefaultBuildDAO(@ComponentImport ActiveObjects activeObjects, BuildIssueFilterQueryBuilder queryBuilder) {
		super(activeObjects, BuildMapping.class);
		this.queryBuilder = queryBuilder;
	}

	@Override
	public Build get(int buildId) {
		return getOne(buildId)
				.map(mappingToObject).orElse(null);
	}

	@Override
	public Build get(int jobId, int buildNumber) {
		return findOne(select().where(JOB_ID + " =  ? AND " + BUILD_NUMBER + " = ?", jobId, buildNumber))
				.map(mappingToObject).orElse(null);
	}

	@Override
	public Set<Build> getAllInRange(int jobId, Range buildRange) {
		return find(select().where(JOB_ID + " = ? AND " + BUILD_NUMBER + " >= ? AND " + BUILD_NUMBER + " <= ?", jobId,
		                           buildRange.getMinimumInteger(),
		                           buildRange.getMaximumInteger()).order(TIME_STAMP))
				.map(mappingToObject).collect(toSet());
	}

	@Override
	public Set<Build> getAllByJob(int jobId) {
		return find(select().where(JOB_ID + " = ?", jobId).order(TIME_STAMP))
				.map(mappingToObject).collect(toSet());
	}

	@Override
	public Set<Build> getByIssueKey(String issueKey) {
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.setInIssueKeys(singleton(issueKey));
		return getLatestByFilter(-1, filter);
	}

	@Override
	public Set<Build> getByProjectKey(String projectKey) {
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.setInProjectKeys(singleton(projectKey));
		return getLatestByFilter(-1, filter);
	}

	@Override
	public Set<Build> getLatestByFilter(int maxResults, BuildIssueFilter filter) {
		Query query = queryBuilder.toBuildMappingQuery(filter, getIssueMappingsByFilter(maxResults, filter));
		if (query != null) {
			return find(query.limit(maxResults))
					.map(mappingToObject).collect(toSet());
		} else {
			return new HashSet<>();
		}
	}

	@Override
	public Build save(Build build) {
		LOGGER.debug("Saving {}", requireNonNull(build));
		return upsert(build.getId(), mapping -> {
			mapping.setJobId(build.getJobId());
			mapping.setBuildNumber(build.getNumber());
			if (StringUtils.length(build.getDisplayName()) <= MAX_SIZE) {
				mapping.setDisplayName(build.getDisplayName());
			} else {
				LOGGER.warn("Display name of build {} of job {} is to big to store...", build.getNumber(), build.getJobId());
			}
			mapping.setCause(StringUtils.substring(build.getCause(), 0, MAX_SIZE));
			mapping.setResult(build.getResult().name());
			mapping.setDuration(build.getDuration());
			mapping.setTimeStamp(build.getTimestamp());
			mapping.setBuiltOn(build.getBuiltOnOrNull());
			mapping.setDeleted(build.isDeleted());
		}).map(mappingToObject).orElse(null);
	}

	@Override
	public int delete(int buildId) {
		return deleteOne(buildId).map(BuildMapping::getID).orElse(-1);
	}

	@Override
	public int[] deleteAllByJob(int jobId) {
		return delete(select().where(JOB_ID + " = ?", jobId)).stream().mapToInt(BuildMapping::getID).toArray();
	}

	@Override
	public void markAsDeleted(Build build) {
		build.setDeleted(true);
		save(build);
	}

	@Override
	public void markAllInJobAsDelete(int jobId) {
		markAllInJobAsDelete(jobId, -1);
	}

	@Override
	public void markAllInJobAsDelete(int jobId, int buildNumber) {
		Query query = select();
		if (buildNumber > 0) {
			query.where(JOB_ID + " = ? AND " + BUILD_NUMBER + " < ?", jobId, buildNumber);
		} else {
			query.where(JOB_ID + " = ?", jobId);
		}
		process(query, mapping -> {
			mapping.setDeleted(true);
			return mapping;
		});
	}

	/**
	 * Returns all the {@link IssueMapping}s that match the given {@code filter}
	 *
	 * @param maxResults the maximum number of results to return
	 * @param filter     the {@link BuildIssueFilter}
	 * @return the {@link List} of {@link IssueMapping}s that match the given {@code filter}
	 */
	private List<IssueMapping> getIssueMappingsByFilter(int maxResults, BuildIssueFilter filter) {
		Query query = queryBuilder.toIssueMappingQuery(filter);
		if (query != null) {
			return executeInTransaction(() -> Arrays.asList(activeObjects.find(IssueMapping.class, query.limit(maxResults))));
		} else {
			return new ArrayList<>();
		}
	}

}
