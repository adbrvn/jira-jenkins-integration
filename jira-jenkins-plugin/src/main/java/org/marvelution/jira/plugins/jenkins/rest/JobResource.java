/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.util.List;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.JobSyncStatus;
import org.marvelution.jira.plugins.jenkins.model.RestData;
import org.marvelution.jira.plugins.jenkins.rest.exception.BadRequestException;
import org.marvelution.jira.plugins.jenkins.rest.exception.NotFoundException;
import org.marvelution.jira.plugins.jenkins.rest.security.AdminRequired;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.JobService;

/**
 * REST resource for {@link Job}s
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@AdminRequired
@Path("job")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JobResource {

	private final JobService jobService;
	private final BuildService buildService;

	public JobResource(JobService jobService, BuildService buildService) {
		this.jobService = jobService;
		this.buildService = buildService;
	}

	/**
	 * Get all the Jobs available
	 *
	 * @param includeDeleted flag where to include or exclude deleted jobs
	 * @return collections of Jobs
	 */
	@GET
	public List<Job> getAll(@QueryParam("includeDeleted") @DefaultValue("false") boolean includeDeleted) {
		return jobService.getAll(includeDeleted);
	}

	/**
	 * Get a single Job by its Id
	 *
	 * @param jobId the job Id
	 * @return 200 OK (With the Job) if there is a job with the given id, 404 NOT FOUND otherwise
	 */
	@GET
	@Path("{jobId}")
	public Job getJob(@PathParam("jobId") int jobId) {
		return getJobInternal(jobId);
	}

	/**
	 * Delete a Job with specified {@literal jobId} from the job cache
	 *
	 * @since 2.2.0
	 */
	@DELETE
	@Path("{jobId}")
	public void removeJob(@PathParam("jobId") int jobId) {
		Job job = getJobInternal(jobId);
		if (job.isDeleted()) {
			jobService.delete(job);
		} else {
			throw new BadRequestException();
		}
	}

	/**
	 * Trigger the synchronization of a specific job
	 */
	@PUT
	@Path("{jobId}/sync")
	public void syncJob(@PathParam("jobId") int jobId) {
		Job job = getJobInternal(jobId);
		jobService.sync(job.getId());
	}

	/**
	 * Get the current synchronization status of a job
	 *
	 * @param jobId the job id
	 * @return 200 OK with {@link JobSyncStatus} status, 204 NO CONTENT or 404 NOT FOUND if the job is not found
	 */
	@GET
	@Path("{jobId}/sync/status")
	public JobSyncStatus getSyncStatus(@PathParam("jobId") int jobId) {
		return jobService.getSyncStatus(getJobInternal(jobId));
	}

	/**
	 * Enable or Disable the auto linking of the job
	 *
	 * @param jobId    the id of the job
	 * @param restData the {@link RestData} payload send
	 */
	@POST
	@Path("{jobId}/autolink")
	public void enableJobAutoLink(@PathParam("jobId") int jobId, RestData restData) {
		Job job = getJobInternal(jobId);
		jobService.enable(job.getId(), Boolean.parseBoolean(restData.getPayload()));
	}

	/**
	 * Remove all the builds of a job
	 *
	 * @param jobId the id of the job to remove all the builds from
	 */
	@DELETE
	@Path("{jobId}/builds")
	public void removeAllBuilds(@PathParam("jobId") int jobId) {
		Job job = getJobInternal(jobId);
		buildService.deleteAllInJob(job);
		job.setLastBuild(0);
		jobService.save(job);
	}

	/**
	 * Mark all the builds of a job as deleted and start a full sync to rebuild the build cache
	 *
	 * @param jobId the job Id to restart the cache on
	 */
	@POST
	@Path("{jobId}/rebuild")
	public void rebuildJobCache(@PathParam("jobId") int jobId) {
		Job job = getJobInternal(jobId);
		buildService.markAllInJobAsDeleted(job);
		job.setLastBuild(0);
		jobService.save(job);
		jobService.sync(job.getId());
	}

	private Job getJobInternal(int jobId) {
		return Optional.ofNullable(jobService.get(jobId))
		               .orElseThrow(NotFoundException::new);
	}

}
