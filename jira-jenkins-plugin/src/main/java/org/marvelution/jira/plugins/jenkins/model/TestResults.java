/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * Test Results Model
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TestResults {

	@XmlAttribute
	private int id = 0;
	@XmlAttribute
	private int buildId = 0;
	@XmlElement
	private int failed = 0;
	@XmlElement
	private int skipped = 0;
	@XmlElement
	private int total = 0;

	TestResults() {
	}

	public TestResults(int id, int buildId, int failed, int skipped, int total) {
		this.id = id;
		this.buildId = buildId;
		this.failed = failed;
		this.skipped = skipped;
		this.total = total;
	}

	public TestResults(int failed, int skipped, int total) {
		this(0, 0, failed, skipped, total);
	}

	public int getId() {
		return id;
	}

	public int getBuildId() {
		return buildId;
	}

	public int getFailed() {
		return failed;
	}

	public int getSkipped() {
		return skipped;
	}

	public int getTotal() {
		return total;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, buildId);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		TestResults that = (TestResults) o;
		return id == that.id && buildId == that.buildId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
				.append("id", id)
				.append("buildId", buildId)
				.append("total", total)
				.toString();
	}

}
