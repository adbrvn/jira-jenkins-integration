/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.marvelution.jira.plugins.jenkins.model.Status;
import org.marvelution.jira.plugins.jenkins.model.SyncProgress;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

/**
 * Base {@link SynchronizationOperation} that uses a {@link Communicator}.
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
public abstract class AbstractCommunicatorAwareSynchronizationOperation<P extends SyncProgress> implements SynchronizationOperation<P> {

	protected final Logger logger = LoggerFactory.getLogger(getClass());
	private Communicator communicator;

	@Override
	public final void synchronize() throws SynchronizationOperationException {
		Status status = getCommunicator().getRemoteStatus();
		if (status.isAccessible()) {
			doSynchronize();
		} else if (!status.isOnline()) {
			logger.info("Skipping synchronization operation {}, remote is not online", getOperationId().getId());
		} else {
			logger.error("Skipping synchronization operation {}, remote is online but not accessible", getOperationId().getId());
		}
	}

	/**
	 * Called by {@link #synchronize()} only in the case the remote site is online and accessible
	 */
	protected abstract void doSynchronize() throws SynchronizationOperationException;

	@Nonnull
	protected Communicator getCommunicator() {
		return communicator;
	}

	@Inject
	public void setCommunicatorFactory(CommunicatorFactory communicatorFactory) {
		communicator = requireNonNull(createCommunicator(communicatorFactory));
	}

	@Nonnull
	protected abstract Communicator createCommunicator(@Nonnull CommunicatorFactory factory);

}
