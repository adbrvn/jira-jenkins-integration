/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.ErrorMessages;
import org.marvelution.jira.plugins.jenkins.model.PluginConfiguration;
import org.marvelution.jira.plugins.jenkins.rest.exception.BadRequestException;
import org.marvelution.jira.plugins.jenkins.rest.security.AdminRequired;
import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;

import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

/**
 * REST resource for plugin configurations
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@Scanned
@AdminRequired
@Path("configuration")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ConfigurationResource {

	private final JenkinsPluginConfiguration pluginConfiguration;
	private final I18nResolver i18nResolver;

	public ConfigurationResource(JenkinsPluginConfiguration pluginConfiguration, @ComponentImport I18nResolver i18nResolver) {
		this.pluginConfiguration = pluginConfiguration;
		this.i18nResolver = i18nResolver;
	}

	/**
	 * @since 2.0.0
	 */
	@GET
	public PluginConfiguration getPluginConfiguration() {
		PluginConfiguration configuration = new PluginConfiguration();
		configuration.setJiraBaseUrl(pluginConfiguration.getJIRABaseUrl());
		configuration.setJIRABaseRpcUrl(pluginConfiguration.getJIRABaseRpcUrl());
		configuration.setMaxBuildsPerPage(pluginConfiguration.getMaximumBuildsPerPage());
		return configuration;
	}

	@POST
	public void savePluginConfiguration(PluginConfiguration configuration) {
		ErrorMessages errorMessages = new ErrorMessages();
		if (configuration.getJIRABaseRpcUrl() != null && pluginConfiguration.getJIRABaseRpcUrl().getHost() == null) {
			errorMessages.addError("jiraBaseRpcUrl", i18nResolver.getText("url.invalid", "host"));
		}
		if (!errorMessages.hasErrors()) {
			pluginConfiguration.setJIRABaseRpcUrl(configuration.getJIRABaseRpcUrl());
			pluginConfiguration.setMaximumBuildsPerPage(configuration.getMaxBuildsPerPage());
		} else {
			throw new BadRequestException(errorMessages);
		}
	}

}
