/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.util.Set;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.dao.BuildDAO;
import org.marvelution.jira.plugins.jenkins.dao.IssueDAO;
import org.marvelution.jira.plugins.jenkins.dao.TestResultDAO;
import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import org.marvelution.jira.plugins.jenkins.services.BuildService;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import org.apache.commons.lang.math.Range;

import static java.util.stream.Collectors.toSet;

/**
 * Default {@link org.marvelution.jira.plugins.jenkins.services.BuildService} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(BuildService.class)
public class DefaultBuildService implements BuildService {

	private final BuildDAO buildDAO;
	private final IssueDAO issueDAO;
	private final TestResultDAO testResultDAO;

	@Inject
	public DefaultBuildService(BuildDAO buildDAO, IssueDAO issueDAO, TestResultDAO testResultDAO) {
		this.buildDAO = buildDAO;
		this.issueDAO = issueDAO;
		this.testResultDAO = testResultDAO;
	}

	@Override
	public Build get(int buildId) {
		return enrichBuild(buildDAO.get(buildId));
	}

	@Override
	public Build get(Job job, int buildNumber) {
		return enrichBuild(buildDAO.get(job.getId(), buildNumber));
	}

	@Override
	public Set<Build> getAllInRange(Job job, Range buildRange) {
		return enrichBuilds(buildDAO.getAllInRange(job.getId(), buildRange));
	}

	@Override
	public Set<Build> getByJob(Job job) {
		return enrichBuilds(buildDAO.getAllByJob(job.getId()));
	}

	@Override
	public Set<Build> getByIssueKey(String issueKey) {
		return enrichBuilds(buildDAO.getByIssueKey(issueKey));
	}

	@Override
	public Set<Build> getByProjectKey(String projectKey) {
		return enrichBuilds(buildDAO.getByProjectKey(projectKey));
	}

	@Override
	public Set<Build> getLatestBuildsByFilter(int maxResults, BuildIssueFilter filter) {
		return enrichBuilds(buildDAO.getLatestByFilter(maxResults, filter));
	}

	@Override
	public Set<String> getRelatedIssueKeys(Build build) {
		return issueDAO.getIssueKeysByBuild(build);
	}

	@Override
	public int getRelatedIssueKeyCount(Build build) {
		return issueDAO.getIssueLinkCount(build);
	}

	@Override
	public Set<String> getRelatedProjectKeys(Build build) {
		return issueDAO.getProjectKeysByBuild(build);
	}

	@Override
	public boolean link(Build build, String issueKey) {
		return issueDAO.link(build, issueKey);
	}

	@Override
	public Build save(Build build) {
		Build saved = buildDAO.save(build);
		if (build.getTestResults() != null) {
			saved.setTestResults(testResultDAO.save(saved.getId(), build.getTestResults()));
		}
		// Copy over the collections that are not saved in the Build Mapping
		saved.getCulprits().addAll(build.getCulprits());
		saved.getArtifacts().addAll(build.getArtifacts());
		saved.getChangeSet().addAll(build.getChangeSet());
		return saved;
	}

	@Override
	public void delete(Build build) {
		testResultDAO.delete(new int[] { buildDAO.delete(build.getId()) });
	}

	@Override
	public void deleteAllInJob(Job job) {
		testResultDAO.delete(buildDAO.deleteAllByJob(job.getId()));
	}

	@Override
	public void markAsDelete(Build build) {
		buildDAO.markAsDeleted(build);
	}

	@Override
	public void markAllInJobAsDeleted(Job job) {
		buildDAO.markAllInJobAsDelete(job.getId());
	}

	@Override
	public void markAllInJobAsDeleted(Job job, int buildNumber) {
		buildDAO.markAllInJobAsDelete(job.getId(), buildNumber);
	}

	private Set<Build> enrichBuilds(Set<Build> builds) {
		return builds.stream().map(this::enrichBuild).collect(toSet());
	}

	private Build enrichBuild(Build build) {
		if (build != null) {
			build.setTestResults(testResultDAO.getForBuild(build.getId()));
		}
		return build;
	}

}
