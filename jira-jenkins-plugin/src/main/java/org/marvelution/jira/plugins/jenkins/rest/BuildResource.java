/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.rest.exception.NotFoundException;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;
import org.marvelution.jira.plugins.jenkins.services.JobService;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import static java.util.stream.Collectors.toList;
import static javax.ws.rs.core.UriBuilder.fromUri;
import static org.apache.commons.codec.digest.DigestUtils.sha1Hex;

/**
 * REST resource for the Jenkins plugin, with endpoints to trigger the synchronization of new builds and get issue link information.
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
@AnonymousAllowed
@Path("build")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BuildResource {

	private final JenkinsPluginConfiguration pluginConfiguration;
	private final JobService jobService;
	private final BuildService buildService;

	public BuildResource(JenkinsPluginConfiguration pluginConfiguration, JobService jobService, BuildService buildService) {
		this.pluginConfiguration = pluginConfiguration;
		this.jobService = jobService;
		this.buildService = buildService;
	}

	/**
	 * Trigger the synchronization of a specific Job
	 *
	 * @param jobHash the hash of the Job URL name
	 */
	@PUT
	@Path("{jobHash}")
	public void syncJob(@PathParam("jobHash") String jobHash) {
		getJobs(jobHash).stream().map(Job::getId).forEach(jobService::sync);
	}

	/**
	 * Returns a list of issues linked to the specific build
	 *
	 * @param jobHash     the hash of the Job URL name
	 * @param buildNumber the build number of the job
	 * @return 200 OK with a map of issue keys to issue urls, 404 NOT FOUND in case either the job of build is not found
	 */
	@GET
	@Path("{jobHash}/{buildNumber}/links")
	public Map<String, String> getBuildLinks(@PathParam("jobHash") String jobHash, @PathParam("buildNumber") int buildNumber) {
		Map<String, String> links = new HashMap<>();
		getJobs(jobHash).stream()
		                .map(job -> buildService.get(job, buildNumber))
		                .filter(Objects::nonNull)
		                .forEach(build -> {
			                for (String issueKey : buildService.getRelatedIssueKeys(build)) {
				                links.put(issueKey, fromUri(pluginConfiguration.getJIRABaseUrl()).path("browse").path(issueKey).build()
				                                                                                 .toASCIIString());
			                }
		                });
		return links;
	}

	private List<Job> getJobs(String hash) {
		List<Job> jobs = jobService.getAll(false).stream()
		                           .filter(job -> hash.equals(sha1Hex(job.getUrlName())))
		                           .collect(toList());
		if (jobs.isEmpty()) {
			throw new NotFoundException();
		}
		return jobs;
	}

}
