/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services;

import java.net.URI;
import java.net.URL;
import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Status;

/**
 * Communicator Service
 * Used for communicating with the remote Jenkins site using the Applications Links
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface Communicator {

	/**
	 * Returns the {@link Status} of the remote site
	 *
	 * @since 2.0.0
	 */
	Status getRemoteStatus();

	/**
	 * Check whether the remote application supports back linking
	 *
	 * @return {@code true} if back linking is supported, {@code false} otherwise
	 */
	boolean isJenkinsPluginInstalled();

	/**
	 * Checks whether crumb security is enabled on the remote application
	 *
	 * @return {@literal true} if the crumb issuer is configured on the application, {@literal false} otherwise
	 */
	boolean isCrumbSecurityEnabled();

	/**
	 * Register this JIRA instance with the remote site
	 *
	 * @since 2.2.0
	 */
	boolean registerWithSite();

	/**
	 * Get all the Jobs from the remote application
	 *
	 * @return {@link List} of all the {@link Job} objects
	 */
	List<Job> getJobs();

	/**
	 * Get all the details for the given {@link Job}
	 *
	 * @param job the {@link Job}
	 * @return the detailed {@link Job}
	 */
	Job getDetailedJob(Job job);

	/**
	 * Get the {@link URL} of the given {@link Job}
	 *
	 * @param job the {@link Job}
	 * @return the {@link URL}
	 */
	URI getJobUrl(Job job);

	/**
	 * Get all the details of a {@link Build}
	 *
	 * @param job   the {@link Job} to get a build from
	 * @param build the {@link Build} to get more details on
	 * @return the {@link Build} with all the details
	 */
	Build getDetailedBuild(Job job, Build build);

	/**
	 * Get all the details of a {@link Build}
	 *
	 * @param job    the {@link Job} to get a build from
	 * @param number the build number to get
	 * @return the {@link Build} with all the details
	 */
	Build getDetailedBuild(Job job, int number);

	/**
	 * Get the {@link URL} of the given {@link Job}
	 *
	 * @param job   the {@link Job}
	 * @param build the {@link Build}
	 * @return the {@link URL}
	 */
	URI getBuildUrl(Job job, Build build);

}
