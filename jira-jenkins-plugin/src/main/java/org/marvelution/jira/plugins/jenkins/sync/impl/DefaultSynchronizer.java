/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.model.SyncProgress;
import org.marvelution.jira.plugins.jenkins.sync.OperationId;
import org.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import org.marvelution.jira.plugins.jenkins.sync.Synchronizer;
import org.marvelution.jira.plugins.jenkins.utils.InjectionHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Synchronization Service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ParametersAreNonnullByDefault
public class DefaultSynchronizer implements Synchronizer {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizer.class);
	private final ConcurrentMap<OperationId, SyncProgress> progressMap = new ConcurrentHashMap<>();
	private final ExecutorService executorService;
	private final InjectionHelper injectionHelper;

	@Inject
	public DefaultSynchronizer(ExecutorService executorService, InjectionHelper injectionHelper) {
		this.executorService = executorService;
		this.injectionHelper = injectionHelper;
	}

	@Override
	public void schedule(SynchronizationOperation operation) {
		SyncProgress progress = progressMap.get(operation.getOperationId());
		if (progress == null || progress.isFinished() || progress.isShouldStop()) {
			LOGGER.debug("Scheduling synchronization for {}", operation.getOperationId());
			scheduleInternal(operation);
		}
	}

	@Override
	public void stopSynchronization(OperationId operationId) {
		SyncProgress progress = progressMap.get(operationId);
		if (progress != null) {
			LOGGER.debug("Requesting synchronization stop for {}", operationId);
			progress.setShouldStop(true);
		}
	}

	@Override
	@Nullable
	public SyncProgress getProgress(OperationId operationId) {
		return progressMap.get(operationId);
	}

	/**
	 * Add a {@link SynchronizationOperation} to the Queue on the {@link #executorService}
	 *
	 * @param operation the {@link SynchronizationOperation} to perform
	 */
	private void scheduleInternal(final SynchronizationOperation operation) {
		final SyncProgress progress = operation.getProgress();
		progressMap.put(operation.getOperationId(), progress);
		executorService.submit(() -> {
			try {
				progress.start();
				if (progress.isShouldStop()) {
					return;
				}
				injectionHelper.injectMembers(operation).synchronize();
			} catch (Throwable e) {
				String message = (e.getMessage() != null ? e.getMessage() : e.toString());
				progress.setError(message);
				LOGGER.warn(message, e);
			} finally {
				progress.finish();
			}
		});
		progress.queued();
	}

	/**
	 * Cleanup just before the bean is set to be destroyed.
	 * This will trigger an all-stop for all ongoing synchronization jobs without logging errors
	 */
	@PreDestroy
	public void destroy() throws Exception {
		executorService.shutdownNow();
	}

}
