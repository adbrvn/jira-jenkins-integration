/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.sal.api.transaction.TransactionCallback;
import net.java.ao.Entity;
import net.java.ao.Query;
import net.java.ao.RawEntity;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

/**
 * Base implementation for DAO based on {@link ActiveObjects}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public abstract class AbstractActiveObjectsDAO<M extends Entity> {

	protected final ActiveObjects activeObjects;
	private final Class<M> mappingClass;

	protected AbstractActiveObjectsDAO(ActiveObjects activeObjects, Class<M> mappingClass) {
		this.activeObjects = activeObjects;
		this.mappingClass = mappingClass;
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} with the specified {@code id}
	 */
	protected Optional<M> getOne(int id) {
		return Optional.ofNullable(executeInTransaction(() -> activeObjects.get(mappingClass, id)));
	}

	/**
	 * Returns an {@link Optional} reference to the {@link Entity} that is the first result of the specified {@link Query}
	 */
	protected Optional<M> findOne(Query query) {
		return find(query).findFirst();
	}

	/**
	 * Returns a {@link Stream} to {@link Entity Entities} that match the specified {@link Query}
	 */
	protected Stream<M> find(Query query) {
		return executeInTransaction(() -> asList(activeObjects.find(mappingClass, query))).stream();
	}

	/**
	 * Insert, or Update, an {@link Entity} using the specified {@link Consumer updater} and return it in its new state.
	 */
	protected Optional<M> upsert(int id, Consumer<M> updater) {
		return executeInTransaction(() -> {
			M mapping = null;
			if (id > 0) {
				mapping = activeObjects.get(mappingClass, id);
			} else {
				mapping = activeObjects.create(mappingClass);
			}
			return Optional.ofNullable(mapping).map(m -> {
				updater.accept(m);
				m.save();
				return m;
			});
		});
	}

	/**
	 * Process all {@link Entity Entities} that match the specified {@link Query} using the specified {@link Function}
	 */
	protected void process(Query query, Function<M, M> function) {
		executeInTransaction(() -> {
			Arrays.stream(activeObjects.find(mappingClass, query)).map(function).forEach(RawEntity::save);
			return null;
		});
	}

	/**
	 * Delete a single {@link Entity}, and returns it, with the specified {@code id}
	 */
	protected Optional<M> deleteOne(int id) {
		return getOne(id).map(mapping -> executeInTransaction(() -> {
			activeObjects.delete(mapping);
			return mapping;
		}));
	}

	/**
	 * Delete all {@link Entity Entities}, and returns them, that match the specified {@code query}
	 */
	@SuppressWarnings({ "ConfusingArgumentToVarargsMethod", "unchecked" })
	protected List<M> delete(Query query) {
		List<M> items = find(query).collect(toList());
		executeInTransaction(() -> {
			activeObjects.delete(items.toArray((M[]) Array.newInstance(mappingClass, items.size())));
			return null;
		});
		return items;
	}

	/**
	 * Delete {@link Entity Entities} using a where criteria statement
	 */
	protected void delete(String criteria, Object... parameters) {
		executeInTransaction(() -> activeObjects.deleteWithSQL(mappingClass, criteria, parameters));
	}

	/**
	 * Execute the specified {@link TransactionCallback} with a transaction
	 */
	protected <T> T executeInTransaction(TransactionCallback<T> callback) {
		return activeObjects.executeInTransaction(callback);
	}

}
