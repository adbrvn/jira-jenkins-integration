/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import java.util.function.Function;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.ao.TestResultsMapping;
import org.marvelution.jira.plugins.jenkins.dao.TestResultDAO;
import org.marvelution.jira.plugins.jenkins.model.TestResults;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;
import static net.java.ao.Query.select;

/**
 * Default {@link TestResultDAO} implementation
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
@Named
public class DefaultTestResultsDAO extends AbstractActiveObjectsDAO<TestResultsMapping> implements TestResultDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultTestResultsDAO.class);
	private final Function<TestResultsMapping, TestResults> mappingToObject = mapping ->
			new TestResults(mapping.getID(), mapping.getBuildId(), mapping.getFailed(), mapping.getSkipped(), mapping.getTotal());

	@Inject
	public DefaultTestResultsDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, TestResultsMapping.class);
	}

	@Override
	public TestResults getForBuild(int buildId) {
		return findOne(select().where(TestResultsMapping.BUILD_ID + " = ?", buildId))
				.map(mappingToObject).orElse(null);
	}

	@Override
	public TestResults save(int buildId, TestResults results) {
		LOGGER.debug("Saving {}", requireNonNull(results));
		return upsert(results.getId(), mapping -> {
			mapping.setBuildId(buildId);
			mapping.setTotal(results.getTotal());
			mapping.setSkipped(results.getSkipped());
			mapping.setFailed(results.getFailed());
		}).map(mappingToObject).orElse(null);
	}

	@Override
	public void delete(int[] buildIds) {
		delete(TestResultsMapping.BUILD_ID + stream(buildIds).mapToObj(Integer::toString).collect(joining(",", " IN (", ")")));
	}

}
