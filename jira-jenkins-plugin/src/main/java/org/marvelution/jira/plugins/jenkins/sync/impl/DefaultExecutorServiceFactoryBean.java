/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.sync.ExecutorServiceFactoryBean;

import com.atlassian.util.concurrent.ThreadFactories;

/**
 * Default {@link ExecutorServiceFactoryBean}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultExecutorServiceFactoryBean implements ExecutorServiceFactoryBean {

	private final String name;
	private final int size;

	@Inject
	public DefaultExecutorServiceFactoryBean() {
		this(5, "JenkinsConnectorExecutorServiceThread");
	}

	public DefaultExecutorServiceFactoryBean(int size, String name) {
		this.name = name;
		this.size = size;
	}

	@Override
	public Object getObject() throws Exception {
		return Executors.newFixedThreadPool(size, ThreadFactories.namedThreadFactory(name));
	}

	@Override
	public Class<ExecutorService> getObjectType() {
		return ExecutorService.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
