/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.ao.JobMapping;
import org.marvelution.jira.plugins.jenkins.dao.JobDAO;
import org.marvelution.jira.plugins.jenkins.model.Job;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jira.plugins.jenkins.ao.JobMapping.DELETED;
import static org.marvelution.jira.plugins.jenkins.ao.JobMapping.NAME;
import static org.marvelution.jira.plugins.jenkins.ao.JobMapping.SITE_ID;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static net.java.ao.Query.select;

/**
 * The Default implementation of the {@link JobDAO} interface
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class DefaultJobDAO extends AbstractActiveObjectsDAO<JobMapping> implements JobDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultJobDAO.class);
	private final Function<JobMapping, Job> mappingToObject = mapping -> {
		Job job = new Job(mapping.getID(), mapping.getSiteId(), mapping.getName());
		job.setUrlName(mapping.getUrlName());
		job.setDisplayName(mapping.getDisplayName());
		job.setLinked(mapping.isLinked());
		job.setLastBuild(mapping.getLastBuild());
		job.setDeleted(mapping.isDeleted());
		return job;
	};

	@Inject
	public DefaultJobDAO(@ComponentImport ActiveObjects activeObjects) {
		super(activeObjects, JobMapping.class);
	}

	@Override
	public List<Job> getAllBySiteId(int siteId, boolean includeDeleted) {
		Query query = select();
		if (includeDeleted) {
			query.where(SITE_ID + " = ?", siteId);
		} else {
			query.where(SITE_ID + " = ? AND " + DELETED + " = ?", siteId, Boolean.FALSE);
		}
		query.order(NAME);
		return find(query).map(mappingToObject).collect(toList());
	}

	@Override
	public List<Job> getAll(boolean includeDeleted) {
		Query query = select();
		if (!includeDeleted) {
			query.where(DELETED + " = ?", Boolean.FALSE);
		}
		query.order(NAME);
		return find(query).map(mappingToObject).collect(toList());
	}

	@Override
	public Job save(Job job) {
		LOGGER.debug("Saving {}", requireNonNull(job));
		return upsert(job.getId(), mapping -> {
			mapping.setSiteId(job.getSiteId());
			mapping.setName(job.getName());
			mapping.setUrlName(job.getUrlNameOrNull());
			mapping.setDisplayName(job.getDisplayNameOrNull());
			mapping.setLastBuild(job.getLastBuild());
			mapping.setLinked(job.isLinked());
			mapping.setDeleted(job.isDeleted());
		}).map(mappingToObject).orElse(null);
	}

	@Override
	public void delete(int jobId) {
		deleteOne(jobId);
	}

	@Override
	public void deleteAllBySiteId(int siteId) {
		delete(SITE_ID + " = ?", siteId);
	}

	@Override
	public void markAsDeleted(Job job) {
		job.setDeleted(true);
		save(job);
	}

	@Override
	public Job get(int jobId) {
		return getOne(jobId).map(mappingToObject).orElse(null);
	}

	@Override
	public List<Job> get(String name) {
		return find(select().where(NAME + " = ?", name))
				.map(mappingToObject).collect(toList());
	}

	@Override
	public List<Job> getLinked(String projectKey) {
		return new ArrayList<>();
	}

}
