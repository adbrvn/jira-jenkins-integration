/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.utils;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

/**
 * Helper for dependency injection
 *
 * @author Mark Rekveld
 * @since 2.1.0
 */
@Named
public class InjectionHelper {

	private final AutowireCapableBeanFactory factory;

	@Inject
	public InjectionHelper(AutowireCapableBeanFactory factory) {
		this.factory = factory;
	}

	public <O> O injectMembers(O object) {
		factory.autowireBean(object);
		return object;
	}

}
