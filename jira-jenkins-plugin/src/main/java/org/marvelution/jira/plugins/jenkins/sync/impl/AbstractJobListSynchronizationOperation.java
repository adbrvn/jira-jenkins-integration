/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.Nonnull;
import javax.inject.Inject;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.SyncProgress;
import org.marvelution.jira.plugins.jenkins.services.JobService;

import static java.util.stream.Collectors.toMap;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
public abstract class AbstractJobListSynchronizationOperation<P extends SyncProgress>
		extends AbstractCommunicatorAwareSynchronizationOperation<P> {

	@Inject
	private JobService jobService;

	@Nonnull
	protected JobService getJobService() {
		return jobService;
	}

	protected int synchronizeJobs(List<Job> jobsToSync, List<Job> localJobs) {
		return synchronizeJobs(jobsToSync, localJobs, job -> {
		});
	}

	protected int synchronizeJobs(List<Job> jobsToSync, List<Job> localJobs, Consumer<Job> syncCallback) {
		int syncCount = 0;
		Map<String, Job> localJobsByUrlName = localJobs.stream().collect(toMap(Job::getUrlName, Function.identity()));
		for (Job job : jobsToSync) {
			if (localJobsByUrlName.containsKey(job.getUrlName())) {
				Job localJob = localJobsByUrlName.get(job.getUrlName());
				// Only update the delete state to not-deleted and leave the rest to the synchronization process
				localJob.setDeleted(false);
				syncCallback.accept(jobService.save(localJob));
			} else {
				syncCount++;
				job.setDeleted(false);
				job.setLastBuild(0);
				syncCallback.accept(jobService.save(job));
			}
		}
		return syncCount;
	}

}
