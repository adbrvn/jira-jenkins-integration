/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import static java.util.Objects.requireNonNull;

/**
 * @author Mark Rekveld
 * @since 1.6.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorMessages {

	@XmlElement
	private List<ErrorMessage> errors;

	public ErrorMessages() {
		this.errors = new ArrayList<>();
	}

	public List<ErrorMessage> getErrors() {
		return errors;
	}

	public void setErrors(List<ErrorMessage> errors) {
		this.errors = requireNonNull(errors);
	}

	public void addError(String field, String message) {
		ErrorMessage error = new ErrorMessage();
		error.field = field;
		error.message = message;
		errors.add(error);
	}

	public boolean hasErrors() {
		return !errors.isEmpty();
	}

	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public static class ErrorMessage {

		public String field;
		public String message;

		@Override
		public int hashCode() {
			return Objects.hash(field, message);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}
			ErrorMessage that = (ErrorMessage) o;
			return Objects.equals(field, that.field) && Objects.equals(message, that.message);
		}

	}

}
