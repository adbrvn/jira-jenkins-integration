/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.dao.SiteDAO;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsDevService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.net.NonMarshallingRequestFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Objects.requireNonNull;

/**
 * Default implementation of {@link CommunicatorFactory}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
@ExportAsDevService(CommunicatorFactory.class)
public class DefaultCommunicatorFactory implements CommunicatorFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCommunicatorFactory.class);
	private final JenkinsPluginConfiguration pluginConfiguration;
	private final NonMarshallingRequestFactory<?> requestFactory;
	private final SiteDAO siteDAO;

	@Inject
	public DefaultCommunicatorFactory(JenkinsPluginConfiguration pluginConfiguration, SiteDAO siteDAO,
	                                  @ComponentImport NonMarshallingRequestFactory<?> requestFactory) {
		this.pluginConfiguration = pluginConfiguration;
		this.requestFactory = requestFactory;
		this.siteDAO = siteDAO;
	}

	@Override
	public Communicator get(int siteId) {
		return get(siteDAO.get(siteId));
	}

	@Override
	public Communicator get(Site site) {
		requireNonNull(site, "site cannot be null");
		LOGGER.debug("Getting a Communicator for Site '" + site.getName() + "'");
		return new RestCommunicator(pluginConfiguration, site, requestFactory);
	}

}
