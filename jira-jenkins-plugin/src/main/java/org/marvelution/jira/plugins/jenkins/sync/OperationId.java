/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync;

import java.io.Serializable;
import java.util.Objects;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.Immutable;

import static java.util.Objects.requireNonNull;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
@Immutable
public final class OperationId implements Serializable, Comparable<OperationId> {

	private final String id;

	private OperationId(String id) {
		this.id = requireNonNull(id);
	}

	public static OperationId of(String id) {
		return new OperationId(id);
	}

	public String getId() {
		return id;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OperationId that = (OperationId) o;
		return Objects.equals(id, that.id);
	}

	@Override
	public String toString() {
		return "OperationId[" + id + "]";
	}

	@Override
	public int compareTo(@Nonnull OperationId other) {
		return id.compareTo(other.id);
	}

}
