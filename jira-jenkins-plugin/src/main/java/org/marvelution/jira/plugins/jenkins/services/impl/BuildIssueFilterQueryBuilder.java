/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.services.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.ao.BuildMapping;
import org.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import org.marvelution.jira.plugins.jenkins.dao.JobDAO;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;

import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.marvelution.jira.plugins.jenkins.ao.BuildMapping.JOB_ID;
import static org.marvelution.jira.plugins.jenkins.ao.BuildMapping.TIME_STAMP;
import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.BUILD_DATE;
import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.ISSUE_KEY;
import static org.marvelution.jira.plugins.jenkins.ao.IssueMapping.PROJECT_KEY;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

/**
 * BuildIssueFilter {@link Query} Builder
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class BuildIssueFilterQueryBuilder {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildIssueFilterQueryBuilder.class);
	private static final String AND = " AND ";
	private static final String OR = " OR ";
	private static final String IN = " IN ";
	private static final String NOT_IN = " NOT" + IN;
	private static final String QUOTE_CHAR = "'";
	private static final String DESC = " DESC";

	private final JobDAO jobDAO;

	@Inject
	public BuildIssueFilterQueryBuilder(JobDAO jobDAO) {
		this.jobDAO = jobDAO;
	}

	/**
	 * Build the {@link Query} from the {@link BuildIssueFilter} to select {@link IssueMapping}s
	 *
	 * @param filter the {@link BuildIssueFilter}
	 * @return the {@link Query}
	 */
	public Query toIssueMappingQuery(BuildIssueFilter filter) {
		StringBuilder projectClaus = new StringBuilder();
		StringBuilder issueClaus = new StringBuilder();
		if (filter != null) {
			if (filter.getInProjectKeys() != null && !filter.getInProjectKeys().isEmpty()) {
				projectClaus.append("(")
				            .append(PROJECT_KEY).append(IN).append(joinToSqlIn(filter.getInProjectKeys()));
				filter.setInJobIds(getJobIdsForProjectKeys(filter.getInProjectKeys()));
				if (!filter.getInJobIds().isEmpty()) {
					projectClaus.append(OR).append(JOB_ID).append(IN).append(joinToSqlIn(filter.getInJobIds()));
				}
				projectClaus.append(")");
			}
			if (filter.getNotInProjectKeys() != null && !filter.getNotInProjectKeys().isEmpty()) {
				if (projectClaus.length() != 0) {
					projectClaus.append(AND);
				}
				projectClaus.append("(")
				            .append(PROJECT_KEY).append(NOT_IN).append(joinToSqlIn(filter.getNotInProjectKeys()));
				filter.setNotInJobIds(getJobIdsForProjectKeys(filter.getNotInProjectKeys()));
				if (!filter.getNotInJobIds().isEmpty()) {
					projectClaus.append(AND).append(JOB_ID).append(NOT_IN)
					            .append(joinToSqlIn(filter.getNotInJobIds()));
				}
				projectClaus.append(")");
			}
			if (filter.getInIssueKeys() != null && !filter.getInIssueKeys().isEmpty()) {
				issueClaus.append(ISSUE_KEY).append(IN).append(joinToSqlIn(filter.getInIssueKeys()));
			}
			if (filter.getNotInIssueKeys() != null && !filter.getNotInIssueKeys().isEmpty()) {
				if (issueClaus.length() != 0) {
					issueClaus.append(AND);
				}
				issueClaus.append(ISSUE_KEY).append(NOT_IN)
				          .append(joinToSqlIn(filter.getNotInIssueKeys()));
			}
		}
		StringBuilder finalClaus = new StringBuilder();
		if (projectClaus.length() != 0) {
			finalClaus.append("(").append(projectClaus).append(")");
		}
		if (issueClaus.length() != 0) {
			if (finalClaus.length() != 0) {
				finalClaus.append(AND);
			}
			finalClaus.append("(").append(issueClaus).append(")");
		}
		if (finalClaus.length() == 0) {
			return null;
		} else {
			LOGGER.debug("Transformed filter {} to IssueMapping where claus: {}", filter, finalClaus);
			return Query.select()
			            .from(IssueMapping.class)
			            .where(finalClaus.toString())
			            .order(BUILD_DATE + DESC);
		}
	}

	/**
	 * Build the {@link Query} from the {@link BuildIssueFilter} to select {@link BuildMapping}s
	 *
	 * @param filter        the {@link BuildIssueFilter}
	 * @param issueMappings previously matched {@link IssueMapping}s
	 * @return the {@link Query}
	 */
	public Query toBuildMappingQuery(BuildIssueFilter filter, List<IssueMapping> issueMappings) {
		StringBuilder whereClaus = new StringBuilder();
		if (!filter.getInJobIds().isEmpty()) {
			whereClaus.append(JOB_ID).append(IN).append(joinToSqlIn(filter.getInJobIds()));
		}
		if (!filter.getNotInJobIds().isEmpty()) {
			if (!filter.getInJobIds().isEmpty()) {
				whereClaus.append(AND);
			}
			whereClaus.append(JOB_ID).append(NOT_IN).append(joinToSqlIn(filter.getNotInJobIds()));
		}
		if (whereClaus.length() > 0) {
			whereClaus.insert(0, "(").append(")");
		}
		if (!issueMappings.isEmpty()) {
			if (whereClaus.length() > 0) {
				whereClaus.append(AND);
			}
			whereClaus.append("ID").append(IN).append(joinToSqlIn(issueMappings.stream().map(IssueMapping::getBuildId).collect(toList())));
		}
		if (whereClaus.length() == 0) {
			return null;
		} else {
			LOGGER.debug("Transformed filter {} to BuildMapping where claus: {}", filter, whereClaus);
			return Query.select()
			            .from(BuildMapping.class)
			            .where(whereClaus.toString())
			            .order(TIME_STAMP + DESC);
		}
	}

	/**
	 * Get all the Job Ids that are linked to the given {@code projectKeys}
	 *
	 * @param projectKeys the project keys to get the job ids for
	 * @return the job ids that are linked
	 * @since 1.5.0
	 */
	private Set<Integer> getJobIdsForProjectKeys(Set<String> projectKeys) {
		Set<Integer> jobIds = new HashSet<>();
		for (String projectKey : projectKeys) {
			jobDAO.getLinked(projectKey).stream()
			      .map(Job::getId)
			      .collect(toCollection(() -> jobIds));
		}
		return jobIds;
	}

	/**
	 * Join the given {@link Set} of keys to be used in a SQL IN statement
	 *
	 * @param keys the keys to join
	 * @return the SQL formatted join
	 */
	private String joinToSqlIn(Iterable<?> keys) {
		List<String> parts = new ArrayList<>();
		for (Object key : keys) {
			if (key != null) {
				if (key instanceof String) {
					parts.add(QUOTE_CHAR + key + QUOTE_CHAR);
				} else if (key instanceof Boolean || key instanceof Number) {
					parts.add(String.valueOf(key));
				} else {
					parts.add(QUOTE_CHAR + key.toString() + QUOTE_CHAR);
				}
			}
		}
		return parts.stream().filter(Objects::nonNull).collect(joining(",", "(", ")"));
	}

}
