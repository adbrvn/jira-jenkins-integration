/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.panels;

import java.util.Date;

import com.atlassian.jira.plugin.issuetabpanel.IssueAction;

/**
 * {@link IssueAction} implementation for a Jenkins build
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class BuildAction implements IssueAction {

	public static final BuildAction NO_BUILDS_ACTION = new BuildAction("No builds found.");
	private final String html;
	private final Date timestamp;

	/**
	 * Create a new Message {@link BuildAction}
	 *
	 * @param message a message to display
	 */
	public BuildAction(String message) {
		html = "<div class=\"message-container\">" + message + "</div>";
		timestamp = null;
	}

	/**
	 * Create a new {@link BuildAction} with specific HTML
	 *
	 * @param html      the html of the build to display in JIRA
	 * @param timestamp the timestamp date of the build
	 */
	public BuildAction(String html, Date timestamp) {
		this.html = html;
		this.timestamp = timestamp;
	}

	@Override
	public String getHtml() {
		return html;
	}

	@Override
	public Date getTimePerformed() {
		return timestamp;
	}

	@Override
	public boolean isDisplayActionAllTab() {
		return true;
	}

}
