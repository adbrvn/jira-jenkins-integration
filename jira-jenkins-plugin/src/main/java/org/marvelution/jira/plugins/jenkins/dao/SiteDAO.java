/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.dao;

import java.util.List;

import org.marvelution.jira.plugins.jenkins.model.Site;

/**
 * {@link Site} Data Access Service
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public interface SiteDAO {

	/**
	 * Get the {@link Site} by its Id
	 *
	 * @param siteId the Id of the {@link Site} to get
	 * @return the {@link Site}
	 */
	Site get(int siteId);

	/**
	 * Get all the {@link Site}s
	 *
	 * @return all the {@link Site}s
	 */
	List<Site> getAll();

	/**
	 * Save the given {@link Site}
	 *
	 * @param site the {@link Site} to save
	 * @return the saved {@link Site}
	 */
	Site save(Site site);

	/**
	 * Delete the {@link Site} by its Id
	 *
	 * @param siteId the Id of the {@link Site} to delete
	 */
	void delete(int siteId);

}
