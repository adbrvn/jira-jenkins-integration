/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.sync.impl;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public final class IssueKeyExtractor {

	private static final String SEPARATOR = "[\\s\\p{Punct}]";
	private static final String KEY_PREFIX_REGEX = "(?:(?<=" + SEPARATOR + ")|^)"; //zero-width positive lookbehind
	private static final String KEY_BODY_REGEX = "(\\p{Lu}[\\p{Lu}\\p{Digit}_]+-\\p{Digit}+)";
	private static final String KEY_POSTFIX_REGEX = "(?:(?=" + SEPARATOR + ")|$)";  //zero-width positive lookahead
	private static final String ISSUE_KEY_REGEX = KEY_PREFIX_REGEX + KEY_BODY_REGEX + KEY_POSTFIX_REGEX;

	private IssueKeyExtractor() {
	}

	public static Set<String> extractIssueKeys(String... messages) {
		Set<String> matches = new HashSet<>();
		if (messages != null && messages.length > 0) {
			Pattern keyPattern = Pattern.compile(ISSUE_KEY_REGEX, Pattern.CASE_INSENSITIVE);
			for (String message : messages) {
				if (isNotBlank(message)) {
					Matcher match = keyPattern.matcher(message);
					while (match.find()) {
						for (int i = 1; i <= match.groupCount(); i++) {
							matches.add(match.group(i));
						}
					}
				}
			}
		}
		return matches;
	}

}
