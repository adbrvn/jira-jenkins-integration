/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Culprit model call
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Culprit {

	private String username;
	private String fullName;

	/**
	 * Default constructor for JAXB
	 */
	Culprit() {
	}

	public Culprit(String username, String fullName) {
		this.username = username;
		this.fullName = fullName;
	}

	/**
	 * Getter of the username of the culprit
	 *
	 * @return the username of the culprit
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Getter of the full name of the culprit
	 *
	 * @return the full name of the culprit
	 */
	public String getFullName() {
		return fullName;
	}

}
