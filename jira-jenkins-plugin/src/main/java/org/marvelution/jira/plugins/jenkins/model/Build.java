/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.marvelution.jira.plugins.jenkins.adapters.URIAdapter;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * Build model
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Build {

	private int id;
	private int jobId;
	private int number;
	private String displayName;
	private String description;
	private boolean deleted;
	private String cause;
	private Result result;
	@XmlTransient
	private String builtOn;
	private long duration;
	private long timestamp;
	private List<Artifact> artifacts;
	private List<Culprit> culprits;
	private List<ChangeSet> changeSet;
	private TestResults testResults;
	@XmlJavaTypeAdapter(value = URIAdapter.class, type = URI.class)
	private URI absoluteUri;

	/**
	 * Default constructor for JAXB
	 */
	Build() {
		id = 0;
	}

	public Build(int jobId, int number) {
		this(0, jobId, number);
	}

	public Build(int id, int jobId, int number) {
		this.id = id;
		this.jobId = jobId;
		this.number = number;
	}

	/**
	 * Getter for the ID
	 *
	 * @return the ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for jobId
	 *
	 * @return the jobId
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * Getter for the build number
	 *
	 * @return the build number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Getter for the build display name
	 *
	 * @return the build display name
	 * @since 1.5.0
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Setter for the build display name
	 *
	 * @param displayName the build display name
	 * @since 1.5.0
	 */
	public void setDisplayName(String displayName) {
		this.displayName = StringUtils.defaultIfBlank(displayName, null);
	}

	/**
	 * Returns the build description
	 *
	 * @since 2.2.0
	 */
	@Nullable
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the build description
	 *
	 * @since 2.2.0
	 */
	public void setDescription(@Nullable String description) {
		this.description = description;
	}

	/**
	 * Getter for the build deleted state
	 *
	 * @return the build deleted state
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Setter for the build deleted state
	 *
	 * @param deleted the build deleted state
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Getter for the cause
	 *
	 * @return the cause
	 */
	public String getCause() {
		return cause;
	}

	/**
	 * Setter for the cause
	 *
	 * @param cause the cause
	 */
	public void setCause(String cause) {
		this.cause = cause;
	}

	/**
	 * Getter for the build result
	 *
	 * @return the result
	 */
	public Result getResult() {
		return result;
	}

	/**
	 * Setter for the build result
	 *
	 * @param result the build result
	 */
	public void setResult(Result result) {
		this.result = result;
	}

	/**
	 * Getter for the name of the build agent
	 *
	 * @return the name of the build agent, never {@code null}. If {@link #builtOn} is not set, then {@code master} is returned
	 */
	@XmlElement
	public String getBuiltOn() {
		return StringUtils.defaultIfBlank(builtOn, "master");
	}

	/**
	 * Setter for the name of the build agent
	 *
	 * @param builtOn name of the build agent
	 */
	public void setBuiltOn(@Nullable String builtOn) {
		this.builtOn = builtOn;
	}

	/**
	 * Getter for the name of the build agent
	 *
	 * @return the name of the build agent, may be {@code null}
	 */
	@Nullable
	public String getBuiltOnOrNull() {
		if (isNotBlank(builtOn)) {
			return builtOn;
		} else {
			return null;
		}
	}

	/**
	 * Getter for the build duration
	 *
	 * @return the build duration
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * Setter for the build duration
	 *
	 * @param duration the build duration
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	/**
	 * Getter for the build {@link Date}
	 *
	 * @return the build {@link Date}, may be {@code null} in case the {@link #timestamp} is {@code 0}
	 */
	public Date getBuildDate() {
		if (timestamp > 0) {
			return new Date(timestamp);
		} else {
			return null;
		}
	}

	/**
	 * Getter for the build timestamp
	 *
	 * @return the build timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * Setter for the build timestamp
	 *
	 * @param timestamp the build timestamp
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Getter for the {@link Artifact} {@link List}
	 *
	 * @return the {@link Artifact} {@link List}, never {@link null}
	 */
	public List<Artifact> getArtifacts() {
		if (artifacts == null) {
			artifacts = new ArrayList<>();
		}
		return artifacts;
	}

	/**
	 * Getter for the {@link Culprit} {@link List}
	 *
	 * @return the {@link Culprit} {@link List}, never {@link null}
	 */
	public List<Culprit> getCulprits() {
		if (culprits == null) {
			culprits = new ArrayList<>();
		}
		return culprits;
	}

	/**
	 * Getter for the {@link ChangeSet} {@link List}
	 *
	 * @return the {@link ChangeSet} {@link List}, never {@link null}
	 */
	public List<ChangeSet> getChangeSet() {
		if (changeSet == null) {
			changeSet = new ArrayList<>();
		}
		return changeSet;
	}

	/**
	 * Getter for the {@link TestResults}
	 *
	 * @return for the {@link TestResults}
	 */
	public TestResults getTestResults() {
		return testResults;
	}

	/**
	 * Setter for the {@link TestResults}
	 *
	 * @param testResults the {@link TestResults}
	 */
	public void setTestResults(TestResults testResults) {
		this.testResults = testResults;
	}

	/**
	 * @since 2.1.0
	 */
	public URI getAbsoluteUri() {
		return absoluteUri;
	}

	/**
	 * @since 2.1.0
	 */
	public void setAbsoluteUri(URI absoluteUri) {
		this.absoluteUri = absoluteUri;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, jobId, number);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Build build = (Build) o;
		return id == build.id && jobId == build.jobId && number == build.number;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
				.append("id", id)
				.append("jobId", jobId)
				.append("number", number)
				.append("cause", cause)
				.append("result", result)
				.append("builtOn", builtOn)
				.append("deleted", deleted)
				.toString();
	}

}
