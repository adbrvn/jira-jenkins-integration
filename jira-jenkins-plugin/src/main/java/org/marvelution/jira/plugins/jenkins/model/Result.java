/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.util.Locale;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * Build Result Model
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
@XmlEnum
@XmlType
public enum Result {

	@XmlEnumValue("success")
	SUCCESS,
	@XmlEnumValue("unstable")
	UNSTABLE,
	@XmlEnumValue("failure")
	FAILURE,
	@XmlEnumValue("not_built")
	NOT_BUILT,
	@XmlEnumValue("aborted")
	ABORTED,
	@XmlEnumValue("unknown")
	UNKNOWN;

	public static Result fromString(@Nullable String result) {
		return result == null ? UNKNOWN : valueOf(result.toUpperCase(Locale.ENGLISH));
	}

	public String key() {
		return name().toLowerCase(Locale.ENGLISH).replaceAll("[^a-z]", "");
	}

	public boolean isWorseThan(Result that) {
		return ordinal() > that.ordinal();
	}

	public boolean isWorseOrEqualTo(Result that) {
		return ordinal() >= that.ordinal();
	}

	public boolean isBetterThan(Result that) {
		return ordinal() < that.ordinal();
	}

	public boolean isBetterOrEqualTo(Result that) {
		return ordinal() <= that.ordinal();
	}

}
