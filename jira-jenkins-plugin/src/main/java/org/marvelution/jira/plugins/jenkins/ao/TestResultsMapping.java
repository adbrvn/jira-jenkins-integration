/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Test Results Mappings
 *
 * @author Mark Rekveld
 * @since 1.4.3
 */
@Preload
public interface TestResultsMapping extends Entity {

	String BUILD_ID = "BUILD_ID";
	String SKIPPED = "SKIPPED";
	String FAILED = "FAILED";
	String TOTAL = "TOTAL";

	int getBuildId();

	void setBuildId(int buildId);

	int getSkipped();

	void setSkipped(int skipped);

	int getFailed();

	void setFailed(int failed);

	int getTotal();

	void setTotal(int total);

}
