/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.upgrade;

import java.util.Collection;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

import static org.marvelution.jira.plugins.jenkins.services.impl.DefaultJenkinsPluginConfiguration.SETTING_PREFIX;
import static org.marvelution.jira.plugins.jenkins.services.impl.DefaultJenkinsPluginConfiguration.getPluginSettings;

/**
 * {@link PluginUpgradeTask} to cleanup unneeded settings from {@link PluginSettings}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
@Named
@ExportAsService(PluginUpgradeTask.class)
public class EnabledProjectsSettingsCleanupUpgradeTask implements PluginUpgradeTask {

	private static final String ENABLED_PROJECT_KEYS = SETTING_PREFIX + "enabled_project_keys";
	private final JenkinsPluginUtil pluginUtil;
	private final PluginSettingsFactory pluginSettingsFactory;

	@Inject
	public EnabledProjectsSettingsCleanupUpgradeTask(JenkinsPluginUtil pluginUtil,
	                                                 @ComponentImport PluginSettingsFactory pluginSettingsFactory) {
		this.pluginUtil = pluginUtil;
		this.pluginSettingsFactory = pluginSettingsFactory;
	}

	@Override
	public int getBuildNumber() {
		return 6;
	}

	@Override
	public String getShortDescription() {
		return "Cleanup unused plugin settings";
	}

	@Override
	public Collection<Message> doUpgrade() throws Exception {
		PluginSettings settings = getPluginSettings(pluginSettingsFactory);
		// Since JIRA Software only software projects are enabled
		settings.remove(ENABLED_PROJECT_KEYS);
		return null;
	}

	@Override
	public String getPluginKey() {
		return pluginUtil.getPluginKey();
	}

}
