/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest;

import java.util.List;
import javax.annotation.Nullable;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.releasereport.ReleaseReportHelper;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

/**
 * @author Mark Rekveld
 * @since 2.1.0
 */
@Path("release-report")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReleaseReportResource {

	private final JenkinsPluginUtil pluginUtil;
	private final ReleaseReportHelper reportHelper;

	public ReleaseReportResource(JenkinsPluginUtil pluginUtil, ReleaseReportHelper reportHelper) {
		this.pluginUtil = pluginUtil;
		this.reportHelper = reportHelper;
	}

	@GET
	@Path("build-info/{issueKey}")
	@Nullable
	public List<Job> getBuildInformation(@PathParam("issueKey") String issueKey) {
		if (pluginUtil.isFeatureEnabled(issueKey)) {
			return reportHelper.getBuildInformation(issueKey);
		} else {
			return null;
		}
	}


}
