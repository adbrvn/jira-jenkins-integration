/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.model;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.marvelution.jira.plugins.jenkins.adapters.URIAdapter;

import org.apache.commons.lang3.builder.ToStringBuilder;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Job {

	private int id;
	private int siteId;
	private String name;
	@XmlTransient
	private String urlName;
	@XmlTransient
	private String displayName;
	private String description;
	private int lastBuild = 0;
	private int oldestBuild = -1;
	private boolean linked = false;
	private boolean deleted = false;
	private boolean buildable = true;
	private List<Build> builds;
	private List<Job> jobs;
	@XmlJavaTypeAdapter(value = URIAdapter.class, type = URI.class)
	private URI absoluteUri;

	/**
	 * Default constructor for JAXB
	 */
	Job() {
	}

	public Job(int siteId, String name) {
		this(0, siteId, name);
	}

	public Job(int id, int siteId, String name) {
		this.id = id;
		this.siteId = siteId;
		this.name = name;
	}

	/**
	 * Copy the original Job given. Only the Id, SiteId and Name are copied!
	 *
	 * @param job the original {@link Job} to copy from
	 */
	public Job(Job job) {
		id = job.getId();
		siteId = job.getSiteId();
		name = job.getName();
	}

	/**
	 * Getter for the id
	 *
	 * @return the Job ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for the siteId
	 *
	 * @return the siteId
	 */
	public int getSiteId() {
		return siteId;
	}

	/**
	 * Setter for the siteId
	 *
	 * @param siteId the siteId
	 */
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the name
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for the name
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the job's URL name
	 *
	 * @since 2.1.0
	 */
	@XmlElement
	public String getUrlName() {
		return urlName != null ? urlName : name;
	}

	/**
	 * Sets the job's URL name
	 *
	 * @since 2.1.0
	 */
	public void setUrlName(@Nullable String urlName) {
		this.urlName = urlName;
	}

	/**
	 * Returns the job's ULR name, or {@literal null} in case the url name is not set
	 *
	 * @since 2.1.0
	 */
	@Nullable
	public String getUrlNameOrNull() {
		if (isNotBlank(urlName)) {
			return urlName;
		} else {
			return null;
		}
	}

	/**
	 * Returns the display name of the job.
	 */
	@XmlElement
	public String getDisplayName() {
		if (isNotBlank(displayName)) {
			return displayName;
		} else if (isNotBlank(urlName)) {
			String name = urlName.replace("/job/", " - ")
			                     .replace("/branch/", " - ");
			try {
				return URLDecoder.decode(name, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				return name;
			}
		} else {
			return getName();
		}
	}

	/**
	 * Getter for the display name
	 *
	 * @param displayName the display name
	 */
	public void setDisplayName(@Nullable String displayName) {
		this.displayName = displayName != null && displayName.equals(name) ? null : displayName;
	}

	/**
	 * Getter for the display name
	 *
	 * @return the display name, or {@literal null} if the display name if {@literal blank}
	 */
	@Nullable
	public String getDisplayNameOrNull() {
		if (isNotBlank(displayName)) {
			return displayName;
		} else {
			return null;
		}
	}

	/**
	 * Getter for the Job description
	 *
	 * @return the Job description
	 */
	@Nullable
	public String getDescription() {
		return description;
	}

	/**
	 * Setter for the Job description
	 *
	 * @param description the Job description
	 */
	public void setDescription(@Nullable String description) {
		this.description = description;
	}

	/**
	 * Getter for the lastBuild
	 *
	 * @return the lastBuild number
	 */
	public int getLastBuild() {
		return lastBuild;
	}

	/**
	 * Setter for the lastBuild
	 *
	 * @param lastBuild the last Build number
	 */
	public void setLastBuild(int lastBuild) {
		this.lastBuild = lastBuild;
	}

	/**
	 * Get the oldest available build number
	 *
	 * @return the oldest available build number
	 */
	public int getOldestBuild() {
		return oldestBuild;
	}

	/**
	 * Set the oldest available build number
	 *
	 * @param oldestBuild the oldest available build number
	 */
	public void setOldestBuild(int oldestBuild) {
		this.oldestBuild = oldestBuild;
	}

	/**
	 * Getter for the linked state
	 *
	 * @return the linked state
	 */
	public boolean isLinked() {
		return linked;
	}

	/**
	 * Setter for the linked state
	 *
	 * @param linked the new linked state
	 */
	public void setLinked(boolean linked) {
		this.linked = linked;
	}

	/**
	 * Getter for the delete state
	 *
	 * @return the deleted state
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Setter for the deleted state
	 *
	 * @param deleted the new deleted state
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Getter for the buildable state
	 *
	 * @return the buildable state
	 */
	public boolean isBuildable() {
		return buildable;
	}

	/**
	 * Setter if the job is buildable
	 *
	 * @param buildable buildable state
	 */
	public void setBuildable(boolean buildable) {
		this.buildable = buildable;
	}

	/**
	 * Getter for the job {@link Build} {@link List}
	 *
	 * @return the job {@link Build} {@link List}, never {@code null}
	 */
	public List<Build> getBuilds() {
		if (builds == null) {
			builds = new ArrayList<>();
		}
		return builds;
	}

	/**
	 * Returns this job's sub {@link Job} list
	 *
	 * @since 2.1.0
	 */
	public List<Job> getJobs() {
		if (jobs == null) {
			jobs = new ArrayList<>();
		}
		return jobs;
	}

	/**
	 * @since 2.1.0
	 */
	public URI getAbsoluteUri() {
		return absoluteUri;
	}

	/**
	 * @since 2.1.0
	 */
	public void setAbsoluteUri(URI absoluteUri) {
		this.absoluteUri = absoluteUri;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, siteId, getUrlName());
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Job job = (Job) o;
		return id == job.id && siteId == job.siteId && Objects.equals(getUrlName(), job.getUrlName());
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, SHORT_PREFIX_STYLE)
				.append("id", id)
				.append("siteId", siteId)
				.append("name", name)
				.append("urlName", urlName)
				.append("lastBuild", lastBuild)
				.append("linked", linked)
				.append("deleted", deleted)
				.toString();
	}

}
