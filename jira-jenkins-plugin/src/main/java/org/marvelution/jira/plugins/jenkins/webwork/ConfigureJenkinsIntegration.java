/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.webwork;

import java.util.Collections;
import java.util.Comparator;

import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.model.Site;
import org.marvelution.jira.plugins.jenkins.services.SiteService;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.web.action.JiraWebActionSupport;

/**
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ConfigureJenkinsIntegration extends JiraWebActionSupport {

	private final JenkinsPluginUtil pluginUtil;
	private final SiteService siteService;

	public ConfigureJenkinsIntegration(JenkinsPluginUtil pluginUtil, SiteService siteService) {
		this.pluginUtil = pluginUtil;
		this.siteService = siteService;
	}

	@Override
	public String doDefault() throws Exception {
		return hasPermissions() ? INPUT : PERMISSION_VIOLATION_RESULT;
	}

	private boolean hasPermissions() {
		return hasGlobalPermission(GlobalPermissionKey.ADMINISTER);
	}

	/**
	 * Load all the {@link Site}s and sort them according to there names
	 */
	public Site[] loadSites() {
		return siteService.getAll(true).stream()
		                  .map(site -> {
			                  Collections.sort(site.getJobs(), Comparator.comparing(Job::getDisplayName));
			                  return site;
		                  })
		                  .sorted(Comparator.comparing(Site::getName)).toArray(Site[]::new);
	}

	/**
	 * Returns the {@link JenkinsPluginUtil}
	 */
	public JenkinsPluginUtil getPluginUtil() {
		return pluginUtil;
	}

}
