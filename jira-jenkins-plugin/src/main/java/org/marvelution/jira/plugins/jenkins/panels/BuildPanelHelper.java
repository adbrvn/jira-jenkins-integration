/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.panels;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.Communicator;
import org.marvelution.jira.plugins.jenkins.services.JenkinsPluginConfiguration;
import org.marvelution.jira.plugins.jenkins.services.JobService;
import org.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import org.marvelution.jira.plugins.jenkins.utils.VelocityUtils;

import com.atlassian.jira.issue.Issue;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.Comparator.comparing;
import static java.util.Comparator.nullsFirst;
import static java.util.stream.Collectors.toList;

/**
 * CI Build Panel Helper
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Named
public class BuildPanelHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildPanelHelper.class);
	private final JenkinsPluginConfiguration pluginConfiguration;
	private final JobService jobService;
	private final BuildService buildService;
	private final VelocityUtils velocityUtils;
	private final JenkinsPluginUtil pluginUtil;
	private final TemplateRenderer templateRenderer;

	@Inject
	public BuildPanelHelper(JenkinsPluginConfiguration pluginConfiguration, JobService jobService, BuildService buildService,
	                        VelocityUtils velocityUtils, JenkinsPluginUtil pluginUtil, @ComponentImport TemplateRenderer templateRenderer) {
		this.pluginConfiguration = pluginConfiguration;
		this.jobService = jobService;
		this.buildService = buildService;
		this.velocityUtils = velocityUtils;
		this.pluginUtil = pluginUtil;
		this.templateRenderer = templateRenderer;
	}

	/**
	 * Getter for all the {@link Build}s related to the given {@link Issue}
	 *
	 * @param issue the {@link Issue} to get all the builds for
	 * @return the collection of {@link Build}s
	 */
	public Iterable<? extends Build> getBuildsByRelation(Issue issue) {
		LOGGER.debug("Looking for builds related to issue [{}]", issue.getKey());
		BuildIssueFilter filter = new BuildIssueFilter();
		filter.getInIssueKeys().add(issue.getKey());
		return buildService.getLatestBuildsByFilter(pluginConfiguration.getMaximumBuildsPerPage(), filter);
	}

	/**
	 * Get {@link BuildAction}s for all the {@link Build}s given
	 *
	 * @param builds the {@link Build}s to get actions for
	 * @param sorted flag to sort the {@link BuildAction}s in reverse order, if false the list is not sorted!
	 * @return the {@link BuildAction}s
	 */
	public List<BuildAction> getBuildActions(Iterable<? extends Build> builds, boolean sorted) {
		List<BuildAction> buildActions = new ArrayList<>();
		for (Build build : builds) {
			try {
				String html = getBuildActionHtml(build);
				if (StringUtils.isNotBlank(html)) {
					buildActions.add(new BuildAction(html, build.getBuildDate()));
				}
			} catch (Exception e) {
				LOGGER.debug("Failed to render HTML for Build [{}], {}", build, e.getMessage());
			}
		}
		if (buildActions.isEmpty()) {
			buildActions.add(BuildAction.NO_BUILDS_ACTION);
		}
		Comparator<BuildAction> comparator = nullsFirst(comparing(BuildAction::getTimePerformed));
		if (sorted) {
			comparator = comparator.reversed();
		}
		return buildActions.stream().sorted(comparator).collect(toList());
	}

	/**
	 * Generate a HTML View for the {@link Build} given
	 *
	 * @param build the {@link Build} to generate the HTML view for
	 * @return the HTML string, may be {@code empty} but not {@code null}
	 * @throws Exception in case of errors rendering the HTML
	 */
	public String getBuildActionHtml(Build build) throws Exception {
		Job job = jobService.get(build.getJobId());
		if (job == null) {
			return "";
		}
		Communicator communicator = jobService.getCommunicatorForJob(job);
		Map<String, Object> context = new HashMap<>();
		context.put("baseUrl", pluginConfiguration.getJIRABaseUrl().toASCIIString());
		context.put("velocityUtils", velocityUtils);
		context.put("job", job);
		context.put("jobUrl", communicator.getJobUrl(job));
		context.put("build", build);
		context.put("buildUrl", communicator.getBuildUrl(job, build));
		if (buildService.getRelatedIssueKeyCount(build) <= 5) {
			context.put("issueKeys", new ArrayList<>(buildService.getRelatedIssueKeys(build)));
		} else {
			context.put("projectKeys", new ArrayList<>(buildService.getRelatedProjectKeys(build)));
		}
		StringWriter writer = new StringWriter();
		try {
			// TODO Refactor to use SOY!
			templateRenderer.render("/templates/jenkins-build.vm", context, writer);
		} catch (IOException e) {
			LOGGER.warn("Failed to render html for build {} -> {}", build.getId(), e.getMessage());
		}
		return writer.toString();
	}

	/**
	 * Returns whether the panel should be shown for the given issue and user
	 */
	public boolean showPanel(Issue issue) {
		return pluginUtil.isFeatureEnabled(issue);
	}

}
