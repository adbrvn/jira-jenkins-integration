/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.jql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.annotation.Nonnull;

import org.marvelution.jira.plugins.jenkins.model.Build;
import org.marvelution.jira.plugins.jenkins.model.Job;
import org.marvelution.jira.plugins.jenkins.services.BuildService;
import org.marvelution.jira.plugins.jenkins.services.JobService;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.apache.commons.lang.math.IntRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.stream.Collectors.toCollection;
import static java.util.stream.Collectors.toList;

/**
 * JQL Function to located issues related to a Jenkins Build
 *
 * @author Mark Rekveld
 * @since 1.1.1
 */
public class IssuesRelatedToBuildFunction extends AbstractJqlFunction {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuesRelatedToBuildFunction.class);
	private final BuildService buildService;
	private final JobService jobService;

	public IssuesRelatedToBuildFunction(BuildService buildService, JobService jobService) {
		this.buildService = buildService;
		this.jobService = jobService;
	}

	@Override
	@Nonnull
	public MessageSet validate(ApplicationUser user, @Nonnull FunctionOperand functionOperand, @Nonnull TerminalClause terminalClause) {
		MessageSet messages = new MessageSetImpl();
		final List<String> arguments = functionOperand.getArgs();
		if (arguments.isEmpty()) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.number.of.arguments", 1));
		} else {
			switch (arguments.size()) {
				case 3:
					if (jobService.get(arguments.get(0)).isEmpty()) {
						messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
					}
					int start = 0, end = 0;
					try {
						start = Integer.parseInt(arguments.get(1));
					} catch (NumberFormatException e) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(1)));
					}
					try {
						end = Integer.parseInt(arguments.get(2));
					} catch (NumberFormatException e) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(2)));
					}
					if (start == 0 || end == 0 || end <= start) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.number.range"));
					}
					break;
				case 2:
					if (jobService.get(arguments.get(0)).isEmpty()) {
						messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
					}
					try {
						//noinspection ResultOfMethodCallIgnored
						Integer.parseInt(arguments.get(1));
					} catch (NumberFormatException e) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(1)));
					}
					break;
				case 1:
					// Only a single argument, assume its the Build Id and validate that
					try {
						final int buildId = Integer.parseInt(arguments.get(0));
						if (buildService.get(buildId) == null) {
							messages.addErrorMessage(getI18n().getText("jql.unknown.build", arguments.get(0)));
						}
					} catch (NumberFormatException e) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.id", arguments.get(0)));
					}
					break;
			}
		}
		return messages;
	}

	@Override
	@Nonnull
	public List<QueryLiteral> getValues(@Nonnull QueryCreationContext queryCreationContext, @Nonnull final FunctionOperand functionOperand,
	                                    @Nonnull TerminalClause terminalClause) {
		List<QueryLiteral> literals = new ArrayList<>();
		List<String> arguments = functionOperand.getArgs();
		List<Build> builds = new ArrayList<>();
		switch (arguments.size()) {
			case 3:
				try {
					final List<Job> jobs = jobService.get(arguments.get(0));
					IntRange buildRange = new IntRange(Integer.parseInt(arguments.get(1)), Integer.parseInt(arguments.get(2)));
					for (Job job : jobs) {
						builds.addAll(buildService.getAllInRange(job, buildRange));
					}
				} catch (Exception e) {
					LOGGER.debug("Failed to get builds {}-{} of job {}, {}", arguments.get(1), arguments.get(2), arguments.get(0),
					             e.getMessage());
				}
				break;
			case 2:
				try {
					final List<Job> jobs = jobService.get(arguments.get(0));
					final int buildNumber = Integer.parseInt(arguments.get(1));
					builds.addAll(jobs.stream().map(job -> buildService.get(job, buildNumber)).collect(toList()));
				} catch (Exception e) {
					LOGGER.debug("Failed to get build {} of job {}, {}", arguments.get(1), arguments.get(0), e.getMessage());
				}
				break;
			case 1:
				// Only a single argument, assume its the Build Id and validate that
				try {
					final int buildId = Integer.parseInt(arguments.get(0));
					builds.add(buildService.get(buildId));
				} catch (Exception e) {
					LOGGER.debug("Failed to get build {}, {}", arguments.get(0), e.getMessage());
				}
				break;
		}
		builds.stream()
		      .filter(build -> build != null)
		      .forEach(build -> buildService.getRelatedIssueKeys(build).stream()
		                                    .map(issueKey -> new QueryLiteral(functionOperand, issueKey))
		                                    .collect(toCollection(() -> literals)));
		return Collections.unmodifiableList(literals);
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	@Override
	@Nonnull
	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

}
