/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.soy.functions;

import java.net.URI;
import java.util.Set;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;

import static java.util.Collections.singleton;

/**
 * Soy Function to correctly format an {@link URI} to a {@link String}
 *
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class URIFunction implements SoyServerFunction<String>, SoyClientFunction {

	private static final Set<Integer> ARG_SIZES = singleton(1);

	@Override
	public JsExpression generate(JsExpression... args) {
		return new JsExpression("'' + " + args[0].getText());
	}

	@Override
	public String apply(Object... args) {
		Object arg = args[0];
		if (arg instanceof URI) {
			return ((URI) arg).toASCIIString();
		} else {
			return String.valueOf(arg);
		}
	}

	@Override
	public String getName() {
		return "uri";
	}

	@Override
	public Set<Integer> validArgSizes() {
		return ARG_SIZES;
	}

}
