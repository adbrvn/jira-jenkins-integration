/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.listeners;

import javax.inject.Inject;
import javax.inject.Named;

import org.marvelution.jira.plugins.jenkins.dao.IssueDAO;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ProjectDeletedEvent;
import com.atlassian.jira.event.issue.IssueEvent;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Event listener implementation that listens for Issue or Project  delete events
 *
 * @author Mark Rekveld
 * @since 1.4.9
 */
@Named
public class IssueOrProjectDeletedListener extends AbstractEventListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssueOrProjectDeletedListener.class);
	private final IssueDAO issueDAO;

	@Inject
	public IssueOrProjectDeletedListener(@ComponentImport EventPublisher eventPublisher, IssueDAO issueDAO) {
		super(eventPublisher);
		this.issueDAO = issueDAO;
	}

	/**
	 * {@link EventListener} for {@link IssueEvent}s
	 *
	 * @param event the new {@link IssueEvent} to handle
	 */
	@EventListener
	public void onIssueEvent(IssueEvent event) {
		if (EventType.ISSUE_DELETED_ID.equals(event.getEventTypeId())) {
			LOGGER.info("Updating build-to-issue index for deleted issue {}", event.getIssue().getKey());
			issueDAO.unlinkForIssueKey(event.getIssue().getKey());
		}
	}

	/**
	 * {@link EventListener} for {@link ProjectDeletedEvent}s
	 *
	 * @param event the new {@link ProjectDeletedEvent} to handle
	 */
	@EventListener
	public void onProjectDeleted(ProjectDeletedEvent event) {
		LOGGER.info("Updating build-to-project index for deleted project {}", event.getKey());
		issueDAO.unlinkForProjectKey(event.getKey());
	}

}
