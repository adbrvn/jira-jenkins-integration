/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jira.plugins.jenkins.rest.filter;

import javax.ws.rs.ext.Provider;

import org.marvelution.jira.plugins.jenkins.rest.security.AdminRequired;

import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AuthenticationRequiredException;
import com.atlassian.plugins.rest.common.security.AuthorisationException;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

import static java.util.Objects.requireNonNull;

/**
 * {@link Provider} implementation to check for the {@link AdminRequired} annotation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Provider
public class AdminRequiredResourceFilter implements ResourceFilter, ContainerRequestFilter {

	private final AbstractMethod abstractMethod;
	private final JiraAuthenticationContext authenticationContext;
	private final GlobalPermissionManager globalPermissionManager;

	public AdminRequiredResourceFilter(AbstractMethod abstractMethod,
	                                   JiraAuthenticationContext authenticationContext,
	                                   GlobalPermissionManager globalPermissionManager) {
		this.abstractMethod = requireNonNull(abstractMethod);
		this.authenticationContext = requireNonNull(authenticationContext);
		this.globalPermissionManager = requireNonNull(globalPermissionManager);
	}

	@Override
	public ContainerRequestFilter getRequestFilter() {
		return this;
	}

	@Override
	public ContainerResponseFilter getResponseFilter() {
		return null;
	}

	@Override
	public ContainerRequest filter(ContainerRequest request) {
		if (isAdminRequired()) {
			ApplicationUser loggedInUser = authenticationContext.getLoggedInUser();
			if (loggedInUser == null) {
				throw new AuthenticationRequiredException();
			} else if (!globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, loggedInUser)) {
				throw new AuthorisationException();
			}
		}
		return request;
	}

	/**
	 * Check if the {@link AdminRequired} annotation is present for this {@link AbstractMethod}
	 *
	 * @return {@code true} if the {@link AdminRequired} annotation is present on the method or resource
	 */
	private boolean isAdminRequired() {
		return ((abstractMethod != null) && (abstractMethod.isAnnotationPresent(AdminRequired.class) ||
				abstractMethod.getResource().isAnnotationPresent(AdminRequired.class)));
	}

}
