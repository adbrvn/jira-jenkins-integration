/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira;

import hudson.model.Job;
import hudson.model.Run;

import static org.apache.commons.codec.digest.DigestUtils.shaHex;
import static org.apache.commons.lang.StringUtils.stripEnd;
import static org.apache.commons.lang.StringUtils.stripStart;

/**
 * Utilities specific to the JIRA plugin
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
public class JIRAUtils {

	/**
	 * Returns the hash of the job that owns the specified {@link Run} that can be used to identify the Job on registered JIRA instances.
	 */
	public static String getJobHash(Run run) {
		return getJobHash(run.getParent());
	}

	/**
	 * Returns the hash of specified {@link Job job} that can be used to identify the Job on registered JIRA instances.
	 */
	public static String getJobHash(Job job) {
		String url = stripStart(stripEnd(job.getUrl(), "/"), "/");
		if (url.startsWith("job/")) {
			url = url.substring(4);
		}
		return shaHex(url);
	}

}
