/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.notifier;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Nullable;

import org.marvelution.jenkins.plugins.jira.JIRAPlugin;
import org.marvelution.jenkins.plugins.jira.JIRASite;
import org.marvelution.jenkins.plugins.jira.listener.JIRABuildNotificationListener;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import hudson.Extension;
import hudson.model.AbstractProject;
import hudson.model.Descriptor;
import hudson.model.Hudson;
import hudson.model.Item;
import hudson.model.ItemGroup;
import hudson.model.listeners.ItemListener;
import hudson.tasks.Publisher;
import hudson.util.DescribableList;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import static org.apache.commons.lang.StringUtils.isBlank;

/**
 * {@link ItemListener} to upgrade the {@link JIRABuildNotifier} to {@link JIRABuildNotificationListener}
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
@Extension
public class JIRABuildNotifierMigrator extends ItemListener {

	private static final Logger LOGGER = Logger.getLogger(JIRABuildNotifierMigrator.class.getName());

	@Override
	public void onLoaded() {
		for (Item item : Hudson.getInstance().getItems()) {
			processItem(item);
		}
	}

	private void processItem(Item item) {
		if (item instanceof ItemGroup) {
			LOGGER.log(Level.FINE, "Migrating JIRA Build Notifiers in group " + item.getName());
			for (Item subItem : ((ItemGroup<? extends Item>) item).getItems()) {
				processItem(subItem);
			}
		} else if (item instanceof AbstractProject) {
			AbstractProject<?, ?> project = (AbstractProject<?, ?>) item;
			DescribableList<Publisher, Descriptor<Publisher>> publishers = project.getPublishersList();
			JIRABuildNotifier buildNotifier = publishers.get(JIRABuildNotifier.class);
			if (buildNotifier != null) {
				LOGGER.log(Level.FINE, "Migrating JIRA Build Notifier of " + item.getName());
				JIRASite site = loadJIRASiteForPostUrl(buildNotifier.postUrl);
				if (site != null) {
					try {
						JIRAPlugin.getPlugin().registerJIRASite(site);
					} catch (IOException e) {
						LOGGER.log(Level.WARNING, "Failed to register JIRA site at " + site.getUri()
								+ ". Registration will be attempted again when the synchronization process in JIRA runs", e);
					}
				}
				publishers.remove(buildNotifier);
			}
		}
	}

	@Nullable
	private JIRASite loadJIRASiteForPostUrl(String postUrl) {
		if (isBlank(postUrl)) {
			return null;
		} else {
			URI jiraUri = URI.create(postUrl.substring(0, postUrl.indexOf("/rest/")));
			String jiraName = null;
			try {
				ClientResponse manifestResponse = new Client().resource(jiraUri)
				                                              .path("rest").path("applinks").path("2.0").path("manifest.json")
				                                              .get(ClientResponse.class);
				if (manifestResponse.getStatus() / 100 == 2) {
						JsonNode manifest = new ObjectMapper().readTree(manifestResponse.getEntityInputStream());
						if (manifest.has("url")) {
							jiraUri = URI.create(manifest.get("url").asText());
						}
						if (manifest.has("name")) {
							jiraName = manifest.get("name").getTextValue();
						}
				}
			} catch (Exception e) {
				LOGGER.log(Level.FINE, "Unable to get JIRA Site name and validate URL from the live instance at: " + jiraUri, e);
			}
			return new JIRASite(jiraUri, jiraName);
		}
	}

}
