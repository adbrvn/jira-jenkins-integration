/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira;

import java.io.IOException;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier;

import hudson.Plugin;
import hudson.PluginWrapper;
import hudson.model.Hudson;
import hudson.model.Items;
import org.kohsuke.stapler.QueryParameter;
import org.kohsuke.stapler.interceptor.RequirePOST;

import static java.util.Collections.unmodifiableSet;

/**
 * The {@link Plugin} implementation for the JIRA Jenkins Plugin.
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JIRAPlugin extends Plugin {

	private static final Logger LOGGER = Logger.getLogger(JIRAPlugin.class.getName());
	private final Set<JIRASite> jiraSites = new HashSet<>();

	/**
	 * Returns the plugin short name
	 */
	public static String getPluginShortName() {
		return getPluginWrapper().getShortName();
	}

	/**
	 * Returns the {@link PluginWrapper} for this plugin
	 */
	private static PluginWrapper getPluginWrapper() {
		return Hudson.getInstance().getPluginManager().getPlugin(JIRAPlugin.class);
	}

	/**
	 * Returns the runtime instance of the {@link JIRAPlugin} class
	 *
	 * @since 2.2.0
	 */
	public static JIRAPlugin getPlugin() {
		return (JIRAPlugin) getPluginWrapper().getPlugin();
	}

	@Override
	public void start() throws Exception {
		Items.XSTREAM.alias("com.marvelution.jenkins.plugins.jira.notifier.JIRABuildNotifier", JIRABuildNotifier.class);
		Items.XSTREAM.alias(JIRABuildNotifier.class.getName(), JIRABuildNotifier.class);
		super.start();
		load();
	}

	@Override
	public void stop() throws Exception {
		save();
		super.stop();
	}

	/**
	 * @since 2.2.0
	 */
	public static Set<JIRASite> getJIRASites() {
		return unmodifiableSet(getPlugin().jiraSites);
	}

	/**
	 * @since 2.2.0
	 */
	@RequirePOST
	public void doRegister(@QueryParameter(value = "uri", required = true) String jiraUri,
	                       @QueryParameter(value = "name", required = true) String jiraName) throws IOException {
		Hudson.getInstance().checkPermission(Hudson.ADMINISTER);
		URI uri = URI.create(jiraUri);
		for (JIRASite existing : jiraSites) {
			if (existing.getUri().equals(uri)) {
				LOGGER.log(Level.INFO, "Updating JIRA Site name to '" + jiraName + "' for site at: " + jiraUri);
				existing.setName(jiraName);
				save();
				return;
			}
		}
		LOGGER.log(Level.INFO, "Registering new JIRA Site '" + jiraName + "' at: " + jiraUri);
		registerJIRASite(new JIRASite(uri, jiraName));
	}

	/**
	 * @since 2.2.0
	 */
	public void registerJIRASite(JIRASite site) throws IOException {
		jiraSites.add(site);
		save();
	}

}
