/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.notifier;

import java.io.IOException;
import java.net.URI;

import org.marvelution.jenkins.plugins.jira.listener.JIRABuildNotificationListener;

import hudson.Extension;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.BuildListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;
import org.kohsuke.stapler.DataBoundConstructor;

/**
 * Custom {@link Notifier} implementation to notify build completions to JIRA
 *
 * @author Mark Rekveld
 * @since 1.0.0
 * @deprecated since 2.2.0, use the {@link JIRABuildNotificationListener} instead to also support pipeline jobs
 */
@Deprecated
public class JIRABuildNotifier extends Notifier {

	public final String postUrl;

	@DataBoundConstructor
	public JIRABuildNotifier(String postUrl) {
		this.postUrl = URI.create(postUrl).toASCIIString();
	}

	@Override
	public BuildStepMonitor getRequiredMonitorService() {
		return BuildStepMonitor.NONE;
	}

	@Override
	public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener) throws InterruptedException, IOException {
		return true;
	}

	/**
	 * {@link JIRABuildNotifier} descriptor class
	 */
	@Extension
	public static class JIRADBuildNotifierDescriptor extends BuildStepDescriptor<Publisher> {

		@Override
		public boolean isApplicable(Class<? extends AbstractProject> aClass) {
			return true;
		}

		@Override
		public String getDisplayName() {
			return "JIRA Build Notifier (Deprecated)";
		}

	}

}
