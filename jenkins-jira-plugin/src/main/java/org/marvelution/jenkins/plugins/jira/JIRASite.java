/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira;

import java.net.URI;
import java.util.Objects;
import javax.annotation.Nullable;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import static org.apache.commons.lang.StringUtils.defaultIfEmpty;

/**
 * Simple model object for storing registered JIRA instances.
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
public class JIRASite {

	private final URI uri;
	private String name;

	public JIRASite(URI uri) {
		this(uri, null);
	}

	public JIRASite(URI uri, @Nullable String name) {
		this.uri = uri;
		this.name = name;
	}

	public URI getUri() {
		return uri;
	}

	public String getName() {
		return defaultIfEmpty(name, "JIRA");
	}

	public void setName(String name) {
		this.name = name;
	}

	public WebResource getResource(Client client) {
		return client.resource(uri).path("rest").path("jenkins").path("1.0").path("build");
	}

	@Override
	public int hashCode() {
		return Objects.hash(uri);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null) {
			return false;
		} else if (o instanceof URI) {
			// Allow for equals checking against java.net.URI to allow for easier existence lookup
			return Objects.equals(uri, o);
		} else if (getClass() != o.getClass()) {
			return false;
		}
		JIRASite jiraSite = (JIRASite) o;
		return Objects.equals(uri, jiraSite.uri);
	}

	@Override
	public String toString() {
		return getName() + " at " + uri;
	}

}
