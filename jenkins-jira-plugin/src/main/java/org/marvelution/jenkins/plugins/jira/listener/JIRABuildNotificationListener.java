/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.listener;

import java.util.Set;
import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import org.marvelution.jenkins.plugins.jira.JIRAPlugin;
import org.marvelution.jenkins.plugins.jira.JIRASite;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import hudson.Extension;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.model.listeners.RunListener;

import static org.marvelution.jenkins.plugins.jira.JIRAUtils.getJobHash;

/**
 * {@link RunListener} implementation that will notify all registered JIRA instances that a build completed.
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
@Extension
public class JIRABuildNotificationListener extends RunListener<Run> {

	@Override
	public void onCompleted(Run run, TaskListener listener) {
		for (JIRASite jiraSite : getJIRASites()) {
			try {
				ClientResponse response = jiraSite.getResource(new Client())
				                                  .path(getJobHash(run))
				                                  .put(ClientResponse.class);
				Response.StatusType statusInfo = response.getStatusInfo();
				if (Response.Status.Family.SUCCESSFUL.equals(statusInfo.getFamily())) {
					listener.getLogger().printf("Notified %s that a build has completed.\n", jiraSite.getName());
				} else if (statusInfo.getStatusCode() != ClientResponse.Status.NOT_FOUND.getStatusCode()) {
					listener.error("Unable to notify %s: [%d] %s", jiraSite.getName(), statusInfo.getStatusCode(),
					               statusInfo.getReasonPhrase());
				}
			} catch (UniformInterfaceException | ClientHandlerException e) {
				listener.error("Failed to notify %s at %s on this builds completion -> %s", jiraSite.getName(), jiraSite.getUri(),
				               e.getMessage());
			}
		}
	}

	@Nonnull
	Set<JIRASite> getJIRASites() {
		return JIRAPlugin.getJIRASites();
	}

}
