/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.scm;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.Nonnull;

import org.marvelution.jenkins.plugins.jira.JIRAPlugin;
import org.marvelution.jenkins.plugins.jira.JIRASite;

import com.google.common.base.Joiner;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.GenericType;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import hudson.Extension;
import hudson.MarkupText;
import hudson.model.Run;
import hudson.scm.ChangeLogAnnotator;
import hudson.scm.ChangeLogSet;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.DeserializationConfig;

import static org.marvelution.jenkins.plugins.jira.JIRAUtils.getJobHash;

import static java.lang.String.valueOf;

/**
 * {@link ChangeLogAnnotator} implementation that annotated JIRA issues keys that of related Entity Links
 *
 * @author Mark Rekveld
 * @since 1.2.0
 */
@Extension
public class JIRAChangeLogAnnotator extends ChangeLogAnnotator {

	private static ThreadLocal<Client> client = new ThreadLocal<Client>() {
		@Override
		protected Client initialValue() {
			DefaultClientConfig config = new DefaultClientConfig();
			JacksonJaxbJsonProvider jacksonProvider = new JacksonJaxbJsonProvider();
			jacksonProvider.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			config.getSingletons().add(jacksonProvider);
			return Client.create(config);
		}
	};
	static final GenericType<Map<String, String>> LINKS_TYPE = new GenericType<Map<String, String>>() {};

	@Override
	public void annotate(Run<?, ?> build, ChangeLogSet.Entry entry, MarkupText markupText) {
		Map<String, String> issueLinks = new HashMap<>();
		for (JIRASite jiraSite : getJIRASites()) {
			Map<String, String> links = jiraSite.getResource(client.get())
			                                    .path(getJobHash(build))
			                                    .path(valueOf(build.getNumber()))
			                                    .path("links")
			                                    .get(LINKS_TYPE);
			if (links != null) {
				issueLinks.putAll(links);
			}
		}
		if (!issueLinks.isEmpty()) {
			Pattern pattern = Pattern.compile("(" + Joiner.on("|").join(issueLinks.keySet()) + ")");
			Matcher matcher = pattern.matcher(markupText.getText());
			while (matcher.find()) {
				if (issueLinks.containsKey(matcher.group())) {
					markupText.addMarkup(matcher.start(), matcher.end(), "<a href='" + issueLinks.get(matcher.group()) + "'>", "</a>");
				}
			}
		}
	}

	/**
	 * @since 1.2.0
	 */
	@Nonnull
	Set<JIRASite> getJIRASites() {
		return JIRAPlugin.getJIRASites();
	}

}
