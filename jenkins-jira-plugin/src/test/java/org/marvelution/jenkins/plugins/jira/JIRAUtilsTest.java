/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira;

import hudson.model.ItemGroup;
import hudson.model.Job;
import hudson.model.Run;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.apache.commons.codec.digest.DigestUtils.shaHex;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit test for {@link JIRAUtils}
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
public class JIRAUtilsTest {

	@Test
	public void testGetJobHash_ForRun() throws Exception {
		Run run = mock(Run.class);
		Job job = mock(Job.class);
		when(job.getParent()).thenAnswer(new Answer<ItemGroup>() {
			@Override
			public ItemGroup answer(InvocationOnMock invocation) throws Throwable {
				ItemGroup group = mock(ItemGroup.class);
				when(group.getUrl()).thenReturn("");
				when(group.getUrlChildPrefix()).thenReturn("job");
				return group;
			}
		});
		when(job.getName()).thenReturn("Test");
		when(job.getShortUrl()).thenCallRealMethod();
		when(run.getParent()).thenReturn(job);
		assertThat(JIRAUtils.getJobHash(run), is("640ab2bae07bedc4c163f679a746f7ab7fb5d1fa"));
		assertThat(shaHex("Test"), is("640ab2bae07bedc4c163f679a746f7ab7fb5d1fa"));
	}

	@Test
	public void testGetJobHash_ForJob() throws Exception {
		Job job = mock(Job.class);
		when(job.getParent()).thenAnswer(new Answer<ItemGroup>() {
			@Override
			public ItemGroup answer(InvocationOnMock invocation) throws Throwable {
				ItemGroup group = mock(ItemGroup.class);
				when(group.getUrl()).thenReturn("job/Folder/");
				when(group.getUrlChildPrefix()).thenReturn("job");
				return group;
			}
		});
		when(job.getName()).thenReturn("Test");
		when(job.getShortUrl()).thenCallRealMethod();
		assertThat(JIRAUtils.getJobHash(job), is("291e1d188dc7acf982260b1f775b2016d5fe6aea"));
		assertThat(shaHex("Folder/job/Test"), is("291e1d188dc7acf982260b1f775b2016d5fe6aea"));
	}

}
