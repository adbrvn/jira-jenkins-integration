/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.scm;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;

import org.marvelution.jenkins.plugins.jira.JIRASite;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import hudson.MarkupText;
import hudson.model.ItemGroup;
import hudson.model.Job;
import hudson.model.Run;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * Testcase for {@link JIRAChangeLogAnnotator}
 *
 * @author Mark Rekveld
 * @since 1.2.0
 */
@RunWith(MockitoJUnitRunner.class)
public class JIRAChangeLogAnnotatorTest {

	@Mock
	private Job job;
	@Mock
	private Run build;
	@Mock
	private WebResource webResource;
	private Set<JIRASite> jiraSites = new HashSet<>();
	private JIRAChangeLogAnnotator annotator;

	@Before
	public void setup() throws IOException {
		annotator = new JIRAChangeLogAnnotator() {
			@Nonnull
			@Override
			Set<JIRASite> getJIRASites() {
				return jiraSites;
			}
		};
		ItemGroup itemGroup = mock(ItemGroup.class);
		when(itemGroup.getUrl()).thenReturn("");
		when(job.getParent()).thenReturn(itemGroup);
		when(job.getUrl()).thenReturn("job/FreeStyleProject/");
		when(build.getParent()).thenReturn(job);
		when(build.getNumber()).thenReturn(1);
		when(webResource.path(anyString())).thenReturn(webResource);
	}

	@Test
	public void testAnnotate_NoJIRASites() throws IOException {
		MarkupText text = new MarkupText("DUMMY-1!");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("DUMMY-1!"));
		verifyZeroInteractions(webResource);
	}

	/**
	 * Test with a single mappable issue in the text
	 */
	@Test
	public void testAnnotate_SingleIssue() {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.get(JIRAChangeLogAnnotator.LINKS_TYPE)).thenAnswer(new Answer<Map<String, String>>() {
			@Override
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> links = new HashMap<>();
				links.put("DUMMY-1", "http://localhost:2990/jira/browse/DUMMY-1");
				return links;
			}
		});
		MarkupText text = new MarkupText("DUMMY-1!");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>!"));
		verify(webResource).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource).path("1");
		verify(webResource).path("links");
		verify(webResource).get(JIRAChangeLogAnnotator.LINKS_TYPE);
	}

	/**
	 * Test with multiple mappable issues and a single unmappable issue in the text
	 */
	@Test
	public void testAnnotate_MultipleIssues() {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.get(JIRAChangeLogAnnotator.LINKS_TYPE)).thenAnswer(new Answer<Map<String, String>>() {
			@Override
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> links = new HashMap<>();
				links.put("DUMMY-1", "http://localhost:2990/jira/browse/DUMMY-1");
				links.put("DUMMY-2", "http://localhost:2990/jira/browse/DUMMY-2");
				links.put("DUMMY-3", "http://localhost:2990/jira/browse/DUMMY-3");
				links.put("DUMMY-4", "http://localhost:2990/jira/browse/DUMMY-4");
				return links;
			}
		});
		MarkupText text = new MarkupText("DUMMY-1 Text DUMMY-2,DUMMY-3 DUMMY-4 NOTME-1!");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a> Text " +
				"<a href='http://localhost:2990/jira/browse/DUMMY-2'>DUMMY-2</a>," +
				"<a href='http://localhost:2990/jira/browse/DUMMY-3'>DUMMY-3</a> " +
				"<a href='http://localhost:2990/jira/browse/DUMMY-4'>DUMMY-4</a> NOTME-1!"));
		verify(webResource).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource).path("1");
		verify(webResource).path("links");
		verify(webResource).get(JIRAChangeLogAnnotator.LINKS_TYPE);
	}

	/**
	 * Test with different word boundaries
	 */
	@Test
	public void testAnnotate_WordBoundaries() {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.get(JIRAChangeLogAnnotator.LINKS_TYPE)).thenAnswer(new Answer<Map<String, String>>() {
			@Override
			public Map<String, String> answer(InvocationOnMock invocation) throws Throwable {
				Map<String, String> links = new HashMap<>();
				links.put("DUMMY-1", "http://localhost:2990/jira/browse/DUMMY-1");
				return links;
			}
		});
		MarkupText text = new MarkupText("DUMMY-1 Text ");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a> Text "));

		text = new MarkupText("DUMMY-1,comment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>,comment"));

		text = new MarkupText("DUMMY-1.comment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>.comment"));

		text = new MarkupText("DUMMY-1!comment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>!comment"));

		text = new MarkupText("DUMMY-1\tcomment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a>\tcomment"));

		text = new MarkupText("DUMMY-1\ncomment");
		annotator.annotate(build, null, text);
		assertThat(text.toString(false), is("<a href='http://localhost:2990/jira/browse/DUMMY-1'>DUMMY-1</a><br>comment"));
		verify(webResource, times(6)).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource, times(6)).path("1");
		verify(webResource, times(6)).path("links");
		verify(webResource, times(6)).get(JIRAChangeLogAnnotator.LINKS_TYPE);
	}

	private JIRASite createJIRASite(String uri, String name) {
		JIRASite site = spy(new JIRASite(URI.create(uri), name));
		doReturn(webResource).when(site).getResource(any(Client.class));
		return site;
	}

}
