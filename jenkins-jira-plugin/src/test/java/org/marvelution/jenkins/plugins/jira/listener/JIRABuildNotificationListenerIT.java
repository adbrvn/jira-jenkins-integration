/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.listener;

import java.util.concurrent.TimeUnit;

import org.marvelution.jenkins.plugins.jira.JIRAPlugin;
import org.marvelution.jenkins.plugins.jira.JIRASite;
import org.marvelution.jenkins.testkit.JenkinsRule;
import org.marvelution.jji.testkit.wiremock.WireMockRule;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.putRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;

/**
 * Integration test for the {@link JIRABuildNotificationListener}
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
public class JIRABuildNotificationListenerIT {

	@Rule
	public WireMockRule jira = new WireMockRule(false);
	@Rule
	public WireMockRule jira2 = new WireMockRule(false);
	@Rule
	public JenkinsRule jenkins = new JenkinsRule();

	@Before
	public void setup() throws Exception {
		JIRAPlugin.getPlugin().registerJIRASite(new JIRASite(jira.serverUri()));
	}

	@Test
	public void testOnComplete() throws Exception {
		String url = "/rest/jenkins/1.0/build/b08c555639b5b7a7f18b87e8d7830ef1d8102e52";
		FreeStyleProject project = jenkins.createProject(FreeStyleProject.class, "Trigger Test");
		jira.wireMock().register(new MappingBuilder(RequestMethod.PUT, urlEqualTo(url))
				                         .willReturn(new ResponseDefinitionBuilder().withStatus(200)));
		FreeStyleBuild build = project.scheduleBuild2(0).get(10, TimeUnit.SECONDS);
		// assert build results
		jenkins.assertBuildStatusSuccess(build);
		jenkins.assertLogContains("Notified JIRA that a build has completed.", build);
		// assert call to mocked JIRA
		jira.wireMock().verifyThat(putRequestedFor(urlEqualTo(url)));
	}

	@Test
	public void testOnComplete_MultipleJIRAInstances() throws Exception {
		String url = "/rest/jenkins/1.0/build/b08c555639b5b7a7f18b87e8d7830ef1d8102e52";
		JIRAPlugin.getPlugin().registerJIRASite(new JIRASite(jira2.serverUri(), "JIRA 2"));
		FreeStyleProject project = jenkins.createProject(FreeStyleProject.class, "Trigger Test");
		jira.wireMock().register(new MappingBuilder(RequestMethod.PUT, urlEqualTo(url))
				                         .willReturn(new ResponseDefinitionBuilder().withStatus(500)));
		jira2.wireMock().register(new MappingBuilder(RequestMethod.PUT, urlEqualTo(url))
				                          .willReturn(new ResponseDefinitionBuilder().withStatus(200)));
		FreeStyleBuild build = project.scheduleBuild2(0).get(10, TimeUnit.SECONDS);
		// assert build results
		jenkins.assertBuildStatusSuccess(build);
		jenkins.assertLogContains("Notified JIRA 2 that a build has completed.", build);
		jenkins.assertLogContains("ERROR: Unable to notify JIRA: [500] Internal Server Error", build);
		// assert call to mocked JIRA instances
		jira.wireMock().verifyThat(putRequestedFor(urlEqualTo(url)));
		jira2.wireMock().verifyThat(putRequestedFor(urlEqualTo(url)));
	}

}
