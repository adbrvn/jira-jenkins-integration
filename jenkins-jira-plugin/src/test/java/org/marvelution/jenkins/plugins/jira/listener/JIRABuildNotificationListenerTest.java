/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.listener;

import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nonnull;

import org.marvelution.jenkins.plugins.jira.JIRASite;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import hudson.model.ItemGroup;
import hudson.model.TaskListener;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

/**
 * Unit test for the {@link JIRABuildNotificationListener}
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
@RunWith(MockitoJUnitRunner.class)
public class JIRABuildNotificationListenerTest {

	@Mock
	private TaskListener taskListener;
	@Mock
	private PrintStream logger;
	@Mock
	private FreeStyleProject job;
	@Mock
	private FreeStyleBuild build;
	@Mock
	private WebResource webResource;
	@Mock
	private ClientResponse clientResponse;
	private Set<JIRASite> jiraSites = new HashSet<>();
	private JIRABuildNotificationListener listener;

	@Before
	public void setup() throws IOException {
		listener = new JIRABuildNotificationListener() {
			@Nonnull
			@Override
			Set<JIRASite> getJIRASites() {
				return jiraSites;
			}
		};
		ItemGroup itemGroup = mock(ItemGroup.class);
		when(itemGroup.getUrl()).thenReturn("");
		when(job.getParent()).thenReturn(itemGroup);
		when(job.getUrl()).thenReturn("job/FreeStyleProject/");
		when(build.getParent()).thenReturn(job);
		when(webResource.path(anyString())).thenReturn(webResource);
		when(taskListener.getLogger()).thenReturn(logger);
	}

	@Test
	public void testOnComplete_NoJIRASites() throws Exception {
		listener.onCompleted(build, taskListener);
		verifyZeroInteractions(webResource);
		verifyZeroInteractions(taskListener);
	}

	@Test
	public void testOnComplete_SuccessfulNotification() throws Exception {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.put(ClientResponse.class)).thenReturn(clientResponse);
		when(clientResponse.getStatusInfo()).thenReturn(ClientResponse.Status.ACCEPTED);
		listener.onCompleted(build, taskListener);
		verify(webResource).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource).put(ClientResponse.class);
		verify(taskListener).getLogger();
		verify(logger).printf("Notified %s that a build has completed.\n", "Local JIRA");
		verifyNoMoreInteractions(webResource);
		verifyNoMoreInteractions(taskListener);
	}

	@Test
	public void testOnComplete_NotFoundNotification() throws Exception {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.put(ClientResponse.class)).thenReturn(clientResponse);
		when(clientResponse.getStatusInfo()).thenReturn(ClientResponse.Status.NOT_FOUND);
		listener.onCompleted(build, taskListener);
		verify(webResource).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource).put(ClientResponse.class);
		verifyNoMoreInteractions(webResource);
		verifyZeroInteractions(taskListener);
	}

	@Test
	public void testOnComplete_NotificationError() throws Exception {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.put(ClientResponse.class)).thenReturn(clientResponse);
		when(clientResponse.getStatusInfo()).thenReturn(ClientResponse.Status.BAD_REQUEST);
		listener.onCompleted(build, taskListener);
		verify(webResource).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource).put(ClientResponse.class);
		verify(taskListener).error("Unable to notify %s: [%d] %s", "Local JIRA", 400, "Bad Request");
		verifyNoMoreInteractions(webResource);
		verifyNoMoreInteractions(taskListener);
	}

	@Test
	public void testOnComplete_NotificationException() throws Exception {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		when(webResource.put(ClientResponse.class)).thenThrow(new ClientHandlerException("exception"));
		listener.onCompleted(build, taskListener);
		verify(webResource).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource).put(ClientResponse.class);
		verify(taskListener).error("Failed to notify %s at %s on this builds completion -> %s", "Local JIRA",
		                           URI.create("http://localhost:2990/jira"), "exception");
		verifyNoMoreInteractions(webResource);
		verifyNoMoreInteractions(taskListener);
	}

	@Test
	public void testOnComplete_ExceptionsInOneWillNotBlockOtherJIRASiteNotifications() throws Exception {
		jiraSites.add(createJIRASite("http://localhost:2990/jira", "Local JIRA"));
		jiraSites.add(createJIRASite("http://localhost:8080/jira", "Local JIRA 2"));
		when(webResource.put(ClientResponse.class))
				.thenThrow(new ClientHandlerException("exception"))
				.thenReturn(clientResponse);
		when(clientResponse.getStatusInfo()).thenReturn(ClientResponse.Status.OK);
		listener.onCompleted(build, taskListener);
		verify(webResource, times(2)).path(DigestUtils.shaHex("FreeStyleProject"));
		verify(webResource, times(2)).put(ClientResponse.class);
		verify(taskListener).error(eq("Failed to notify %s at %s on this builds completion -> %s"), anyString(),
		                           any(URI.class), eq("exception"));
		verify(taskListener).getLogger();
		verify(logger).printf(eq("Notified %s that a build has completed.\n"), anyString());
		verifyNoMoreInteractions(webResource);
		verifyNoMoreInteractions(taskListener);
	}

	private JIRASite createJIRASite(String uri, String name) {
		JIRASite site = spy(new JIRASite(URI.create(uri), name));
		doReturn(webResource).when(site).getResource(any(Client.class));
		return site;
	}

}
