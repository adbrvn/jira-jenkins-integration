/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira.notifier;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.marvelution.jenkins.plugins.jira.JIRAPlugin;
import org.marvelution.jenkins.plugins.jira.JIRASite;
import org.marvelution.jenkins.testkit.JenkinsRule;

import hudson.model.FreeStyleProject;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Integration test for the {@link JIRABuildNotifierMigrator}
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
public class JIRABuildNotifierMigratorIT {

	@Rule
	public JenkinsRule jenkins = new JenkinsRule();

	@Test
	public void testOnCompleted() throws Exception {
		List<FreeStyleProject> projects = jenkins.jenkins.getAllItems(FreeStyleProject.class);
		assertThat(projects.size(), is(3));
		for (FreeStyleProject project : projects) {
			assertThat(project.getPublishersList().isEmpty(), is(true));
		}
		Map<URI, String> siteMap = new HashMap<>();
		for (JIRASite jiraSite : JIRAPlugin.getJIRASites()) {
			siteMap.put(jiraSite.getUri(), jiraSite.getName());
		}
		assertThat(siteMap.isEmpty(), is(false));
		assertThat(siteMap.size(), is(2));
		assertThat(siteMap.containsKey(URI.create("http://localhost:2990/jira")), is(true));
		assertThat(siteMap.get(URI.create("http://localhost:2990/jira")), is("JIRA"));
		assertThat(siteMap.containsKey(URI.create("https://issues.marvelution.org")), is(true));
		assertThat(siteMap.get(URI.create("https://issues.marvelution.org")), is("Marvelution Issues"));
	}

}
