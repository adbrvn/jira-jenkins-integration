/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.plugins.jira;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.Response;

import org.marvelution.jenkins.testkit.JenkinsRule;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.representation.Form;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.marvelution.jenkins.testkit.JenkinsRule.ALICE;
import static org.marvelution.jenkins.testkit.JenkinsRule.BOB;

import static hudson.security.Permission.READ;
import static javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED_TYPE;
import static jenkins.model.Jenkins.ADMINISTER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;

/**
 * Authz Integration Test for {@link JIRAPlugin}
 *
 * @author Mark Rekveld
 * @since 2.2.0
 */
public class JIRAPluginAuthzIT {

	@Rule
	public JenkinsRule jenkins = new JenkinsRule();

	@Before
	public void setUp() throws Exception {
		jenkins.useCrumbs(true);
	}

	@Test
	public void testRegisterJIRASite() throws Exception {
		jenkins.addAuthorization(ADMINISTER, ALICE);
		WebResource resource = new Client().resource(jenkins.getUriBuilder()
		                                                    .path("plugin")
		                                                    .path("jenkins-jira-plugin")
		                                                    .path("register")
		                                                    .build());
		resource.addFilter(new HTTPBasicAuthFilter(ALICE, ALICE));
		Form form = new Form();
		form.add("uri", "http://localhost:2990/jira");
		form.add("name", "Local JIRA");
		ClientResponse response = resource.entity(form, APPLICATION_FORM_URLENCODED_TYPE)
		                                  .header(jenkins.jenkins.getCrumbIssuer().getCrumbRequestField(),
		                                          jenkins.jenkins.getCrumbIssuer().getCrumb(null))
		                                  .post(ClientResponse.class);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		List<JIRASite> jiraSites = new ArrayList<>(JIRAPlugin.getJIRASites());
		assertThat(jiraSites.size(), is(1));
		assertThat(jiraSites.get(0).getUri(), is(URI.create("http://localhost:2990/jira")));
		assertThat(jiraSites.get(0).getName(), is("Local JIRA"));
	}

	@Test
	public void testRegisterJIRASite_WithoutCrumb() throws Exception {
		jenkins.useCrumbs(false);
		jenkins.addAuthorization(ADMINISTER, ALICE);
		WebResource resource = new Client().resource(jenkins.getUriBuilder()
		                                                    .path("plugin")
		                                                    .path("jenkins-jira-plugin")
		                                                    .path("register")
		                                                    .build());
		resource.addFilter(new HTTPBasicAuthFilter(ALICE, ALICE));
		Form form = new Form();
		form.add("uri", "http://localhost:2990/jira");
		form.add("name", "Local JIRA");
		ClientResponse response = resource.entity(form, APPLICATION_FORM_URLENCODED_TYPE)
		                                  .post(ClientResponse.class);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		List<JIRASite> jiraSites = new ArrayList<>(JIRAPlugin.getJIRASites());
		assertThat(jiraSites.size(), is(1));
		assertThat(jiraSites.get(0).getUri(), is(URI.create("http://localhost:2990/jira")));
		assertThat(jiraSites.get(0).getName(), is("Local JIRA"));
	}

	@Test
	public void testRegisterJIRASite_Update() throws Exception {
		JIRAPlugin.getPlugin().registerJIRASite(new JIRASite(URI.create("http://localhost:2990/jira"), "Local JIRA"));
		jenkins.addAuthorization(ADMINISTER, ALICE);
		WebResource resource = new Client().resource(jenkins.getUriBuilder()
		                                                    .path("plugin")
		                                                    .path("jenkins-jira-plugin")
		                                                    .path("register")
		                                                    .build());
		resource.addFilter(new HTTPBasicAuthFilter(ALICE, ALICE));
		Form form = new Form();
		form.add("uri", "http://localhost:2990/jira");
		form.add("name", "New JIRA");
		ClientResponse response = resource.entity(form, APPLICATION_FORM_URLENCODED_TYPE)
		                                  .header(jenkins.jenkins.getCrumbIssuer().getCrumbRequestField(),
		                                          jenkins.jenkins.getCrumbIssuer().getCrumb(null))
		                                  .post(ClientResponse.class);
		assertThat(response.getStatus(), is(Response.Status.OK.getStatusCode()));
		List<JIRASite> jiraSites = new ArrayList<>(JIRAPlugin.getJIRASites());
		assertThat(jiraSites.size(), is(1));
		assertThat(jiraSites.get(0).getUri(), is(URI.create("http://localhost:2990/jira")));
		assertThat(jiraSites.get(0).getName(), is("New JIRA"));
	}

	@Test
	public void testRegisterJIRASite_AnonymousNotAllowed() throws Exception {
		WebResource resource = new Client().resource(jenkins.getUriBuilder()
		                                                    .path("plugin")
		                                                    .path("jenkins-jira-plugin")
		                                                    .path("register")
		                                                    .build());
		Form form = new Form();
		form.add("uri", "http://localhost:2990/jira");
		form.add("name", "Local JIRA");
		ClientResponse response = resource.entity(form, APPLICATION_FORM_URLENCODED_TYPE)
		                                  .header(jenkins.jenkins.getCrumbIssuer().getCrumbRequestField(),
		                                          jenkins.jenkins.getCrumbIssuer().getCrumb(null))
		                                  .post(ClientResponse.class);
		assertThat(response.getStatus(), is(Response.Status.FORBIDDEN.getStatusCode()));
		List<JIRASite> jiraSites = new ArrayList<>(JIRAPlugin.getJIRASites());
		assertThat(jiraSites.isEmpty(), is(true));
	}

	@Test
	public void testRegisterJIRASite_AccessDenied() throws Exception {
		jenkins.addAuthorization(READ, BOB);
		WebResource resource = new Client().resource(jenkins.getUriBuilder()
		                                                    .path("plugin")
		                                                    .path("jenkins-jira-plugin")
		                                                    .path("register")
		                                                    .build());
		resource.addFilter(new HTTPBasicAuthFilter(BOB, BOB));
		Form form = new Form();
		form.add("uri", "http://localhost:2990/jira");
		form.add("name", "Local JIRA");
		ClientResponse response = resource.entity(form, APPLICATION_FORM_URLENCODED_TYPE)
		                                  .header(jenkins.jenkins.getCrumbIssuer().getCrumbRequestField(),
		                                          jenkins.jenkins.getCrumbIssuer().getCrumb(null))
		                                  .post(ClientResponse.class);
		// Test seem to have an issue with Guava so its not responding with 403 but instead return 500
		assertThat(response.getStatusInfo().getFamily(), not(Response.Status.Family.SUCCESSFUL));
		List<JIRASite> jiraSites = new ArrayList<>(JIRAPlugin.getJIRASites());
		assertThat(jiraSites.isEmpty(), is(true));
	}

}
