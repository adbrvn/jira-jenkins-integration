/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.testkit.scm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import hudson.model.AbstractBuild;
import hudson.model.User;
import hudson.scm.ChangeLogSet;
import org.kohsuke.stapler.export.Exported;
import org.kohsuke.stapler.export.ExportedBean;

/**
 * {@link ChangeLogSet} specific to the {@link SimpleChangelogParser}
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class SimpleChangeLog extends ChangeLogSet<SimpleChangeLog.Entry> {

	private final List<Entry> entries;

	public SimpleChangeLog(AbstractBuild<?, ?> build, List<Entry> entries) {
		super(build);
		for (Entry entry : entries) {
			entry.setParent(this);
		}
		this.entries = Collections.unmodifiableList(entries);
	}

	@Override
	public boolean isEmptySet() {
		return entries.isEmpty();
	}

	public List<Entry> getEntries() {
		return entries;
	}

	@Override
	public Iterator<Entry> iterator() {
		return entries.iterator();
	}

	@ExportedBean(defaultVisibility = 999)
	public static class Entry extends ChangeLogSet.Entry {

		private String message;
		private String author;

		@Override
		public void setParent(ChangeLogSet parent) {
			super.setParent(parent);
		}

		@Override
		@Exported
		public String getMsg() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		@Override
		@Exported
		public User getAuthor() {
			return (author == null) ? User.getUnknown() : User.get(author);
		}

		public void setAuthor(String author) {
			this.author = author;
		}

		@Override
		public Collection<String> getAffectedPaths() {
			return new ArrayList<>();
		}

	}

}
