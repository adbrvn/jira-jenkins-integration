/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.testkit.scm;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hudson.model.AbstractBuild;
import hudson.scm.ChangeLogParser;
import hudson.scm.ChangeLogSet;
import org.apache.commons.digester.Digester;
import org.xml.sax.SAXException;

/**
 * Simple {@link ChangeLogParser} implementation to parse the simple format
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class SimpleChangelogParser extends ChangeLogParser {

	@Override
	public ChangeLogSet<? extends ChangeLogSet.Entry> parse(AbstractBuild build, File changelogFile) throws IOException, SAXException {
		List<SimpleChangeLog.Entry> entries = new ArrayList<>();
		if (changelogFile.exists()) {
			try (FileInputStream fis = new FileInputStream(changelogFile)) {
				Digester digester = new Digester();
				digester.setClassLoader(SimpleChangeLog.class.getClassLoader());
				digester.push(entries);
				digester.addObjectCreate("*/simple-scm/entry", SimpleChangeLog.Entry.class);
				digester.addBeanPropertySetter("*/simple-scm/entry/message", "message");
				digester.addBeanPropertySetter("*/simple-scm/entry/author", "author");
				digester.addSetNext("*/simple-scm/entry", "add");
				digester.parse(fis);
			}
		}
		return new SimpleChangeLog(build, entries);
	}

}
