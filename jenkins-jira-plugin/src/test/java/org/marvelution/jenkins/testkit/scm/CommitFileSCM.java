/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jenkins.testkit.scm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import hudson.Extension;
import hudson.FilePath;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.BuildListener;
import hudson.scm.ChangeLogParser;
import hudson.scm.NullSCM;
import hudson.scm.SCM;
import hudson.scm.SCMDescriptor;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.StaplerRequest;

/**
 * Test {@link SCM} implementation that generates a changelog using a commit file
 *
 * @author Mark Rekveld
 * @since 1.6.0
 */
public class CommitFileSCM extends NullSCM {

	private final List<String> commits;
	private final int commitsPerBuild;

	public CommitFileSCM(URI commitFile) throws IOException {
		this(commitFile, 1);
	}

	public CommitFileSCM(URI commitFile, int commitsPerBuild) throws IOException {
		commits = Files.readAllLines(Paths.get(commitFile), Charset.forName("UTF-8"));
		Collections.reverse(commits);
		this.commitsPerBuild = commitsPerBuild;
	}

	@Override
	public boolean checkout(AbstractBuild<?, ?> build, Launcher launcher, FilePath workspace, BuildListener listener, File changeLogFile)
			throws IOException, InterruptedException {
		if (workspace.exists()) {
			listener.getLogger().println("Deleting existing workspace " + workspace.getRemote());
			workspace.deleteRecursive();
		}
		try (PrintStream stream = new PrintStream(new FileOutputStream(changeLogFile), false, "UTF-8")) {
			stream.println("<?xml version='1.0' encoding='UTF-8'?>");
			stream.println("<simple-scm>");
			int endIndex = build.getNumber() * commitsPerBuild;
			for (int i = endIndex - commitsPerBuild; i < endIndex && i < commits.size(); i++) {
				stream.println("<entry>");
				stream.println("<message>" + commits.get(i).substring(8) + "</message>");
				stream.println("<author>mark</author>");
				stream.println("</entry>");
			}
			stream.println("</simple-scm>");
			stream.close();
		}
		return true;
	}

	@Override
	public ChangeLogParser createChangeLogParser() {
		return new SimpleChangelogParser();
	}

	@Extension
	public static class CommitFileSCMDescriptor extends SCMDescriptor<CommitFileSCM> {

		public CommitFileSCMDescriptor() {
			super(null);
		}

		public String getDisplayName() {
			return "Simple SCM";
		}

		@Override
		public SCM newInstance(StaplerRequest req, JSONObject formData) throws FormException {
			try {
				return new CommitFileSCM(null);
			} catch (IOException e) {
				throw new FormException(e, "scm");
			}
		}
	}

}
