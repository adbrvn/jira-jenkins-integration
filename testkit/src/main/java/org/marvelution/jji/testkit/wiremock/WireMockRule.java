/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.wiremock;

import java.net.URI;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class WireMockRule implements TestRule {

	private final ThreadLocal<WireMockServer> server = new ThreadLocal<>();
	private final boolean isDefaultInstance;

	public WireMockRule() {
		this(true);
	}

	public WireMockRule(boolean isDefaultInstance) {
		this.isDefaultInstance = isDefaultInstance;
	}

	@Override
	public Statement apply(final Statement base, final Description description) {
		return new Statement() {
			@Override
			public void evaluate() throws Throwable {
				WireMockServer server = new WireMockServer(wireMockConfig().dynamicPort()
				                                                           .fileSource(new TestDescriptionFileSource(description)));
				WireMockRule.this.server.set(server);
				server.start();
				if (isDefaultInstance) {
					WireMock.configureFor("localhost", server.port());
				}
				try {
					base.evaluate();
				} finally {
					server.stop();
				}
			}
		};
	}

	public URI serverUri() {
		return URI.create("http://localhost:" + server().port() + "/");
	}

	public WireMock wireMock() {
		return new WireMock(server());
	}

	private WireMockServer server() {
		return server.get();
	}

}
