/*
 * Copyright (c) 2012-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.jji.testkit.wiremock;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.github.tomakehurst.wiremock.common.SingleRootFileSource;
import org.junit.runner.Description;

/**
 * @author Mark Rekveld
 * @since 2.0.0
 */
public class TestDescriptionFileSource extends SingleRootFileSource {

	public TestDescriptionFileSource(Description description) {
		super(locateRootDirectoryForDescription(description));
	}

	private static File locateRootDirectoryForDescription(Description description) {
		URL root = TestDescriptionFileSource.class.getClassLoader().getResource(description.getTestClass().getSimpleName());
		if (root != null) {
			try {
				Path path = Paths.get(root.toURI());
				if (description.isTest()) {
					Path resolve = path.resolve(description.getMethodName());
					if (Files.exists(resolve)) {
						return resolve.toFile();
					}
				}
			} catch (URISyntaxException e) {
				throw new IllegalStateException(e);
			}
		}
		return new File("src/test/resources");
	}

}
