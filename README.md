This projects features a Jenkins and a JIRA plugin to integrate the two, more details on <https://docs.marvelution.org/display/JJI>

Issue Tracker
=============
<https://issues.marvelution.org/browse/JJI>

Continuous Builder
==================
<https://builds.marvelution.org/browse/JJI>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
